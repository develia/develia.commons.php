<?php

namespace Develia;

class Reflector {

    private function __construct() { }


    /**
     * @param $classOrObject class-string | object
     * @param $methodName string
     * @return bool
     * @throws \ReflectionException
     */
    public static function isMethodRedefined($classOrObject, string $methodName): bool {
        $reflection = new \ReflectionMethod($classOrObject, $methodName);
        return $reflection->getDeclaringClass()->getName() !== $classOrObject;
    }

}