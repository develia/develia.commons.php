<?php

namespace Develia;


/**
 * @template T
 */
class ArrayManipulator implements \ArrayAccess, \Countable, \IteratorAggregate {


    /**
     * @var $array T[]
     */
    private array $array;


    /**
     * @param $array array<T>
     */
    public function __construct(array &$array) {
        $this->array = &$array;
    }

    /**
     * @param $value
     * @param bool $strict
     * @return false|int|string
     */
    public function search($value, bool $strict = false) {
        return array_search($value, $this->array, $strict);
    }

    /**
     * @return $this
     */
    public function clear(): ArrayManipulator {
        array_splice($this->array, 0);
        return $this;
    }

    /**
     * @param $mapper
     * @return $this
     * @throws \ReflectionException
     */
    public function map($mapper): ArrayManipulator {
        $mapper = Functions::getKeyValueIndexFn(Functions::createMapper($mapper));


        $i = 0;
        foreach ($this->array as $k => &$v) {
            $v = call_user_func($mapper, $k, $v, $i);
            $i++;
        }
        return $this;
    }

    /**
     * @param $size
     * @return ArrayManipulator
     */
    public function batch($size): ArrayManipulator {

        $this->values();

        for ($i = 0; $i < count($this->array); $i++) {
            $slice = array_slice($this->array, $i, $size);
            $this->splice($i, count($slice), [$slice]);
        }

        return $this;
    }

    /**
     * @throws \ReflectionException
     */
    public function mapMany($mapper): ArrayManipulator {
        $mapper = Functions::getKeyValueIndexFn(Functions::createMapper($mapper));

        $this->values();


        for ($i = 0; $i < count($this->array);) {
            $items = call_user_func($mapper, $i, $this->array[$i], $i);
            $this->splice($i, 1, $items);
            $i += count($items);
        }

        return $this;
    }

    /**
     * @return T[]
     */
    public function toArray(): array {
        return $this->getArray();
    }

    /**
     * @return ArrayManipulator
     */
    public function copy(): ArrayManipulator {
        $array = $this->toArray();
        return new ArrayManipulator($array);
    }


    /**
     * @return $this
     */
    public function reverse(): ArrayManipulator {
        $array = $this->getArray();
        $reverse = array_reverse($array);
        $this->replaceWith($reverse);
        return $this;
    }

    /**
     * @param $predicate
     * @return $this
     * @throws \ReflectionException
     */
    public function filter($predicate): ArrayManipulator {
        $predicate = Functions::getKeyValueIndexFn(Functions::createPredicate($predicate));


        $i = 0;
        foreach ($this->array as $k => $v) {
            if (!call_user_func($predicate, $k, $v, $i))
                unset($this->array[$k]);

            $i++;
        }
        return $this;
    }

    /**
     * @param $array
     * @return $this
     */
    public function replaceWith($array): ArrayManipulator {
        $this->clear();
        foreach ($array as $k => $v) {
            $this->array[$k] = $v;
        }
        return $this;
    }

    /**
     * @return $this
     */
    public function values(): ArrayManipulator {
        $this->replaceWith(array_values($this->array));
        return $this;
    }

    /**
     * @return $this
     */
    public function keys(): ArrayManipulator {

        $this->replaceWith(array_keys($this->array));
        return $this;
    }

    /**
     * Insert a value at a specific position.
     *
     * @param int $position
     * @param T[] ...$values
     * @return $this
     */
    public function insert(int $position, ...$values): ArrayManipulator {

        array_splice($this->array, $position, 0, $values);
        return $this;
    }

    /**
     * @param $offset
     * @return $this
     */
    public function removeKey($offset): ArrayManipulator {

        unset($this->array[$offset]);
        return $this;
    }

    /**
     * @param int $offset
     * @param int | null $length
     * @param T[] $replacement
     * @return T[]
     */
    public function splice(int $offset, ?int $length = null, array $replacement = array()): array {

        return array_splice($this->array, $offset, $length, $replacement);
    }


    /**
     * @param $array
     * @return $this
     */
    public function merge($array): ArrayManipulator {

        foreach ($array as $k => $v) {
            $this->array[$k] = $v;
        }
        return $this;
    }


    /**
     * @param bool $descending
     * @param bool $preserve_keys
     * @param int $flags
     * @return $this
     */
    public function sort(bool $descending = false, bool $preserve_keys = false, int $flags = SORT_REGULAR): ArrayManipulator {

        if ($descending) {
            if ($preserve_keys)
                arsort($this->array, $flags);
            else
                rsort($this->array, $flags);
        } else {
            if ($preserve_keys)
                asort($this->array, $flags);
            else
                sort($this->array, $flags);
        }

        return $this;
    }

    /**
     * @param callable $func
     * @param bool $descending
     * @return $this
     */
    public function sortBy(callable $func, bool $descending = false): ArrayManipulator {

        if ($descending) {
            usort($this->array, function ($a, $b) use ($func) {
                return call_user_func($func, $a) < call_user_func($func, $b);
            });
        } else {
            usort($this->array, function ($a, $b) use ($func) {
                return call_user_func($func, $a) > call_user_func($func, $b);
            });

        }
        return $this;
    }

    /**
     * @param int $offset
     * @param int|null $length
     * @param bool $preserve_keys
     * @return ArrayManipulator
     */
    public function slice(int $offset, int $length = null, bool $preserve_keys = false): ArrayManipulator {

        $slice = array_slice($this->array, $offset, $length, $preserve_keys);
        $this->replaceWith($slice);
        return $this;
    }

    /**
     * @param $value
     * @return $this
     */
    public function removeValue($value): ArrayManipulator {

        if (($key = array_search($value, $this->array)) !== false) {
            unset($this->array[$key]);
        }

        return $this;
    }


    /**
     * @return $this
     */
    public function shift(): ArrayManipulator {

        array_shift($this->array);
        return $this;
    }

    /**
     * @param ...$values
     * @return $this
     */
    public function unshift(...$values): ArrayManipulator {

        array_unshift($this->array, $values);
        return $this;
    }


    /**
     * @param ...$values
     * @return $this
     */
    public function push(...$values): ArrayManipulator {

        foreach ($values as $value) {
            $this->array[] = $value;
        }
        return $this;
    }

    /**
     * @return T
     */
    public function pop() {

        return array_pop($this->array);
    }


    // Implementación de \ArrayAccess

    /**
     * @param $value
     * @return bool
     */
    public function containsValue($value): bool {

        return in_array($value, $this->array);
    }

    /**
     * @param $value
     * @return bool
     */
    public function containsKey($value): bool {

        return array_key_exists($value, $this->array);
    }

    /**
     * @param $offset
     * @param $value
     * @return void
     */
    public function offsetSet($offset, $value) {

        $this->array[$offset] = $value;
    }

    /**
     * @param $offset
     * @return bool
     */
    public function offsetExists($offset): bool {

        return isset($this->array[$offset]);
    }

    /**
     * @param $offset
     * @return void
     */
    public function offsetUnset($offset) {
        $this->removeKey($offset);
    }

    // Implementación de \Countable

    /**
     * @param $offset
     * @return mixed
     */
    public function offsetGet($offset) {

        return $this->array[$offset];
    }

    // Implementación de \IteratorAggregate


    /**
     * @return T[]
     */
    public function & getArray(): array {
        return $this->array;
    }


    /**
     * @return \Generator
     */
    public function getIterator(): \Generator {

        foreach ($this->array as $k => $v)
            yield $k => $v;
    }

    /**
     * @return int
     */
    public function count(): int {
        $array = $this->getArray();
        return count($array);
    }

}
