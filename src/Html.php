<?php

namespace Develia;

class Html {

    public static function input($type, $value = null, $attrs = null) {
        $tmp = [
            "type"  => $type,
            "value" => $value
        ];

        Html::tag("input", null, array_merge($attrs, $tmp));
    }

    /**
     * @param string $tag
     * @param string | \Closure $content
     * @param array | null $attrs
     * @return void
     */
    public static function tag(string $tag, $content, ?array $attrs = null) {
        ?><<?php echo $tag; ?><?php Html::attributes($attrs); ?>><?php

        if (!is_null($content)) {
            if (is_callable($content) && !is_string($content))
                call_user_func($content);
            else
                echo $content;

            ?></<?php echo $tag; ?>><?php
        }


    }

    private static function attributes(array $attrs) {
        if ($attrs) {
            $i = 0;
            foreach ($attrs as $k => $v) {
                $v = str_replace('"', '\"', $v);
                if ($i > 0)
                    echo " ";
                echo("$k=\"$v\"");
                $i++;
            }

        }

    }

    public static function select(array $options, $selected_value = null, $attrs = null) {
        Html::tag("select", function () use ($selected_value, $options) {
            Html::options($options, $selected_value);
        }, $attrs);
    }

    /**
     * @param iterable $iterable
     * @param string|null $selected_value
     */
    public static function options(iterable $iterable, ?string $selected_value = null) {

        foreach ($iterable as $value => $label) {
            $selected = !is_null($selected_value) && $value == $selected_value;
            Html::tag("option", $label, $selected ? ["selected" => "selected"] : null);
        }

    }

    /**
     * @param $path
     * @param bool $use_cache
     * @return void
     */
    public static function inlineSvg($path, bool $use_cache = true) {
        $fileExtension = pathinfo($path, PATHINFO_EXTENSION);
        if (strtolower($fileExtension) !== 'svg') {
            return;
        }

        if (!file_exists($path)) {
            return;
        }

        $svgFolder = pathinfo($path, PATHINFO_DIRNAME);
        $svgFilename = pathinfo($path, PATHINFO_FILENAME);

        $cachedSvgPath = $svgFolder . '/' . $svgFilename . '.cached.svg';

        if ($use_cache && file_exists($cachedSvgPath)) {
            echo file_get_contents($cachedSvgPath);
            return;
        }

        $svgContent = file_get_contents($path);

        // Elimina la declaración XML si la tiene
        $svgContent = preg_replace('/<\?xml.*?\?>\s*/i', '', $svgContent);

        // Elimina doctypes
        $svgContent = preg_replace('/<!DOCTYPE.*?>\s*/i', '', $svgContent);

        // Elimina comentarios
        $svgContent = preg_replace('/<!--.*?-->\s*/s', '', $svgContent);

        // Elimina elementos <script> para prevenir XSS
        $svgContent = preg_replace('/<script.*?>.*?<\/script>\s*/si', '', $svgContent);

        // Elimina metadatos y descripciones
        $svgContent = preg_replace('/<(metadata|desc).*?>.*?<\/\1>\s*/si', '', $svgContent);

        // Elimina el atributo id
        $svgContent = preg_replace('/\sid="[^"]*"/i', '', $svgContent);

        // Elimina el atributo data-name
        $svgContent = preg_replace('/\sdata-name="[^"]*"/i', '', $svgContent);

        if (is_writable(pathinfo($cachedSvgPath, PATHINFO_DIRNAME))) {
            file_put_contents($cachedSvgPath, $svgContent);
        }

        echo $svgContent;
    }


}