<?php /** @noinspection PhpComposerExtensionStubsInspection */

namespace Develia {

    class Cryptor {

        private string $encryption_key;

        private $encryption_iv;

        private string $algorithm;

        /**
         *
         * @var int
         */
        private int $options;

        public function __construct(string $encryption_key, string $encryption_iv, string $algorithm = 'AES-256-CBC', int $options = 0) {
            $iv_length = openssl_cipher_iv_length($algorithm);

            if (strlen($encryption_iv) < $iv_length) {
                $this->encryption_iv = str_pad($encryption_iv, $iv_length, "0");
            } else {
                $this->encryption_iv = substr($encryption_iv, 0, $iv_length);
            }

            $this->encryption_key = $encryption_key;
            $this->algorithm = $algorithm;
            $this->options = $options;
        }

        public function decrypt($string) {

            return openssl_decrypt($string, $this->algorithm, $this->encryption_key, $this->options, $this->encryption_iv);

        }

        public function encrypt($string) {
            return openssl_encrypt($string, $this->algorithm, $this->encryption_key, $this->options, $this->encryption_iv);
        }

    }


}
