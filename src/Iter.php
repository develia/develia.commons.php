<?php

namespace Develia;

class Iter {

    private function __construct() { }


    /**
     * Applies a callback function to each element in the iterable and yields a generator with the results.
     *
     * @param iterable $iterable The collection to iterate over.
     * @param callable $callback A function to apply to each element, which takes two parameters: the value and the key.
     * @return \Generator A generator with the mapped values.
     */
    public static function map(iterable $iterable, callable $callback): \Generator {

        $callback = Functions::getKeyValueIndexFn($callback);
        $i = 0;
        foreach ($iterable as $key => $value) {
            yield $key => $callback($key, $value, $i++);
        }
    }

    /**
     * Filters elements based on a predicate and yields a generator with elements that match the predicate.
     *
     * @param iterable $iterable The collection to iterate over.
     * @param callable $callback A predicate function that returns true for elements to keep.
     * @return \Generator A generator with the filtered elements.
     */
    public static function filter(iterable $iterable, callable $callback): \Generator {
        $callback = Functions::getKeyValueIndexFn($callback);
        $i = 0;
        foreach ($iterable as $key => $value) {
            if ($callback($key, $value, $i++)) {
                yield $key => $value;
            }
        }
    }


    /**
     * Reduces the iterable to a single value by applying an accumulator function to each element.
     *
     * @param iterable $iterable The collection to iterate over.
     * @param callable $callback The accumulator function, takes the accumulator, value, and key as arguments.
     * @param mixed $initial The initial value for the accumulator.
     * @return mixed The reduced value after processing all elements.
     */
    public static function reduce(iterable $iterable, callable $callback, $initial = null) {
        $accumulator = $initial;
        foreach ($iterable as $key => $value) {
            $accumulator = $callback($accumulator, $value, $key);
        }
        return $accumulator;
    }

    /**
     * Returns the first element that satisfies the predicate or null if none do.
     *
     * @param iterable $iterable The collection to iterate over.
     * @param callable $callback The predicate function to apply, should return true for the desired element.
     * @param null $default
     * @return mixed|null The first element that matches the predicate, or null if no match is found.
     */
    public static function first(iterable $iterable, callable $callback, $default = null) {

        $callback = Functions::getKeyValueIndexFn($callback);
        $i = 0;

        foreach ($iterable as $key => $value) {
            if ($callback($key, $value, $i++)) {
                return $value;
            }
        }
        return $default;
    }

    /**
     * Checks if any element in the iterable satisfies the predicate.
     *
     * @param iterable $iterable The collection to iterate over.
     * @param callable $callback The predicate function, returns true if the element meets the condition.
     * @return bool True if at least one element satisfies the predicate, false otherwise.
     */
    public static function any(iterable $iterable, callable $callback): bool {

        $callback = Functions::getKeyValueIndexFn($callback);
        $i = 0;

        foreach ($iterable as $key => $value) {
            if ($callback($key, $value, $i++)) {
                return true;
            }
        }
        return false;
    }

    /**
     * Checks if all elements in the iterable satisfy the predicate.
     *
     * @param iterable $iterable The collection to iterate over.
     * @param callable $callback The predicate function, returns true if the element meets the condition.
     * @return bool True if all elements satisfy the predicate, false if any element fails.
     */
    public static function all(iterable $iterable, callable $callback): bool {

        $callback = Functions::getKeyValueIndexFn($callback);
        $i = 0;

        foreach ($iterable as $key => $value) {
            if (!$callback($key, $value, $i++)) {
                return false;
            }
        }
        return true;
    }

}