<?php

namespace Develia;

class Math {

    private function __construct() { }


    public static function constrain($input, $min, $max) {

        if ($input < $min)
            return $min;
        else return min($input, $max);

    }

    public static function almostEqual($a, $b, $epsilon = 0.00001): bool {
        return abs($a - $b) <= $epsilon;
    }

    public static function power($base, $exp) {
        return pow($base, $exp);
    }

    public static function sqrt($a): float {
        return sqrt($a);
    }

    public static function abs($a) {
        return abs($a);
    }

    public static function factorial($n): int {
        $result = 1;
        for ($i = 1; $i <= $n; $i++) {
            $result *= $i;
        }
        return $result;
    }

    public static function sin($angle): float {
        return sin($angle);
    }

    public static function cos($angle): float {
        return cos($angle);
    }

    public static function tan($angle): float {
        return tan($angle);
    }

    public static function log($number, $base = M_E): float {
        return log($number, $base);
    }

    public static function exp($number): float {
        return exp($number);
    }

    public static function hypotenuse($a, $b): float {
        return hypot($a, $b);
    }

    public static function round($a, $precision = 0): float {
        return round($a, $precision);
    }

    public static function floor($a) {
        return floor($a);
    }

    public static function ceil($a) {
        return ceil($a);
    }

    public static function radToDeg($rad): float {
        return rad2deg($rad);
    }

    public static function degToRad($deg): float {
        return deg2rad($deg);
    }

    public static function max($array) {
        return max($array);
    }

    public static function min($array) {
        return min($array);
    }

    public static function rand($min, $max): int {
        return rand($min, $max);
    }

    public static function avg($array) {
        return array_sum($array) / count($array);
    }

    public static function median($array) {
        sort($array);
        $count = count($array);
        $middle = floor($count / 2);

        if ($count % 2) {
            return $array[$middle];
        } else {
            return ($array[$middle - 1] + $array[$middle]) / 2;
        }
    }

    public static function mode($array) {
        $values = array_count_values($array);
        return array_search(max($values), $values);
    }

    public static function stdDev($array): float {
        $average = self::avg($array);
        $squareSum = 0;

        foreach ($array as $value) {
            $squareSum += pow($value - $average, 2);
        }

        return sqrt($squareSum / (count($array) - 1));
    }

    public static function sum($array) {
        return array_sum($array);
    }

    public static function sec($angle) {
        return 1 / cos($angle);
    }

    public static function csc($angle) {
        return 1 / sin($angle);
    }

    public static function cot($angle) {
        return 1 / tan($angle);
    }

    public static function asin($value): float {
        return asin($value);
    }

    public static function acos($value): float {
        return acos($value);
    }

    public static function atan($value): float {
        return atan($value);
    }

    public static function acosh($value): float {
        return acosh($value);
    }

    public static function asinh($value): float {
        return asinh($value);
    }

    public static function atanh($value): float {
        return atanh($value);
    }

}