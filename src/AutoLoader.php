<?php

namespace Develia;

use function strlen;
use function substr;

class AutoLoader {

    private bool $registered = false;

    private array $directories;


    private ?array $namespaces = null;

    private array $loaded_classes = [];

    private $file_name_resolver;

    /**
     * AutoLoader constructor.
     *
     * @param array|string $folders Directory or directories where to look for classes.
     * @param array|string $namespaces Namespaces or namespace that the autoloader should handle (optional).
     * @param callable | null $file_name_resolver
     */
    public function __construct($folders, $namespaces = null, ?callable $file_name_resolver = null) {
        if (is_string($folders))
            $folders = [$folders];

        $this->directories = array_map("realpath", $folders);

        if (is_string($namespaces))
            $this->namespaces = [$namespaces];

        if (is_array($namespaces)) {
            $namespaces = array_map(function ($x) {
                return str_replace("/", "\\", $x);
            }, $namespaces);
            $this->namespaces = $namespaces;
        }

        $this->file_name_resolver = $file_name_resolver;
    }

    /**
     * Convert a string to kebab-case.
     *
     * @return callable String in kebab-case.
     */
    public static function getKebabCaseFileNameResolver(): callable {
        return function ($string) {

            // Insert hyphens before uppercase letters (CamelCase)
            $string = preg_replace('/(?<=\\w)(?=[A-Z])/', '-$1', $string);

            $string = preg_replace('/[^a-zA-Z0-9]/', '-', $string);

            // Replace multiple hyphens with a single hyphen
            $string = preg_replace('/-+/', '-', $string);

            // Convert all uppercase letters to lowercase
            $string = strtolower($string);

            // Remove leading or trailing hyphens
            return trim($string, '-') . ".php";
        };

    }


    /**
     * Includes the file for the specified class.
     *
     * @param string $class Full name of the class.
     */
    public function load(string $class) {
        if ($this->isClassNamespaceAllowed($class)) {
            if ($this->tryLoadClass($class, $file)) {
                $this->loaded_classes[] = $class;
            }
        }

    }

    /**
     * Check if the class's namespace is in the list of allowed namespaces
     *
     * @param string $class Full name of the class.
     * @return bool Whether the namespace is allowed.
     */
    private function isClassNamespaceAllowed(string $class): bool {
        if ($this->namespaces === null)
            return true;


        foreach ($this->namespaces as $namespace) {

            $class_namespace = self::getNamespace($class) . "\\";
            if (substr($class_namespace, 0, strlen($namespace . "\\")) === $namespace . "\\") {
                return true;
            }

        }
        return false;
    }

    /**
     * @param $class
     * @return string
     */
    private static function getNamespace($class): string {
        $parts = explode("\\", $class);
        return implode("\\", array_slice($parts, 0, -1));
    }

    /**
     * Load the class file.
     *
     * @param $fqnClass
     * @param $full_file_path
     * @return bool
     */
    private function tryLoadClass($fqnClass, &$full_file_path): bool {
        $parts = explode("\\", $fqnClass);
        $class_name = $parts[count($parts) - 1];

        $parts[count($parts) - 1] = $this->file_name_resolver ? call_user_func($this->file_name_resolver, $class_name) : $class_name . '.php';
        $fqn_class_without_root = implode("\\", array_slice($parts, 1));

        foreach ($this->directories as $folder) {
            $full_file_path = $folder . DIRECTORY_SEPARATOR . $fqn_class_without_root;
            if (include_once $full_file_path)
                return true;
        }

        return false;

    }

    /**
     * @return array
     */
    public function getLoadedClasses(): array {
        return $this->loaded_classes;
    }

    public function scoped(callable $callable) {
        $wasRegistered = false;
        try {
            $wasRegistered = $this->isRegistered();
            if (!$wasRegistered)
                $this->register();

            call_user_func($callable);
        } finally {
            if (!$wasRegistered)
                $this->unregister();
        }
    }

    /**
     * @return bool
     */
    public function isRegistered(): bool {
        return $this->registered;
    }

    /**
     * Registers this AutoLoader in the AutoLoad stack.
     */
    public function register($prepend = false) {
        if (spl_autoload_register([$this, 'load'], true, $prepend))
            $this->registered = true;
    }

    /**
     * Unregisters this AutoLoader from the AutoLoad stack.
     */
    public function unregister() {
        if (spl_autoload_unregister([$this, 'load']))
            $this->registered = false;
    }

    public function addDirectory($directory) {
        $realpath = realpath($directory);
        if ($realpath !== false) {
            $this->directories[] = $realpath;
        }
    }

    public function removeDirectory($directory) {
        $realpath = realpath($directory);
        $key = array_search($realpath, $this->directories);
        if ($key !== false) {
            unset($this->directories[$key]);
        }
    }

    public function getDirectories(): array {
        return $this->directories;
    }

    public function addNamespace($namespace) {
        $this->namespaces[] = str_replace("/", "\\", $namespace);
    }

    public function removeNamespace($namespace) {
        $key = array_search(str_replace("/", "\\", $namespace), $this->namespaces);
        if ($key !== false) {
            unset($this->namespaces[$key]);
        }
    }

    public function getNamespaces(): array {
        return $this->namespaces;
    }


}