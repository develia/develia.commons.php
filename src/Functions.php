<?php /** @noinspection PhpFullyQualifiedNameUsageInspection */


namespace Develia;


/**
 *
 */
class Functions {


    private function __construct() {
    }

    /**
     * @param \Closure|string[]|string $selector
     * @return \Closure
     */
    public static function createMapper($selector) {

        if (is_null($selector))
            return null;

        if (is_callable($selector) && !is_array($selector))
            return $selector;

        if (is_array($selector))
            return function ($obj) use ($selector) {

                $output = [];
                foreach ($selector as $key => $v)
                    $output[$key] = Obj::getValue($obj, $v);

                return $output;
            };

        if (Obj::isString($selector))
            return function ($obj) use ($selector) {
                return Obj::getValue($obj, $selector);
            };


        throw new \InvalidArgumentException("Selector must be a closure, a list or a string");

    }

    /** @noinspection PhpUnusedParameterInspection */
    public static function keyValueIndexIdentity($k, $v, $i) {
        return $v;
    }

    /** @noinspection PhpUnusedParameterInspection */
    public static function keyValueIndexKey($k, $v, $i) {
        return $k;
    }

    /**
     * @param callable | null $func
     * @return \Closure|null
     * @noinspection PhpDocMissingThrowsInspection
     */
    public static function getKeyValueIndexFn(?callable $func): ?\Closure {

        if (is_null($func))
            return null;

        $output = null;

        try {

            /** @noinspection PhpUnhandledExceptionInspection */
            $reflector = new \ReflectionFunction($func);
            $num = $reflector->getNumberOfParameters();

            switch ($num) {
                case 0:
                    $output = function ($k, $v, $i) use ($func) {
                        return call_user_func($func);
                    };
                    break;
                case 1:
                    $output = function ($k, $v, $i) use ($func) {
                        return call_user_func($func, $v);
                    };
                    break;
                case 2:
                    $output = function ($k, $v, $i) use ($func) {
                        return call_user_func($func, $k, $v);
                    };
                    break;
                case 3:
                    $output = function ($k, $v, $i) use ($func) {
                        return call_user_func($func, $k, $v, $i);
                    };
                    break;
                default:
                    $output = function ($k, $v, $i) use ($num, $func) {
                        $args = [$k, $v, $i];
                        for ($j = 0; $j < $num; $j++)
                            $args[] = null;
                        return call_user_func_array($func, $args);
                    };
                    break;
            }
        } finally {
            return $output;
        }


    }

    /**
     * @param callable|array $predicate
     * @return callable
     */
    public static function createPredicate($predicate): ?callable {
        if (is_null($predicate))
            return null;

        if (is_callable($predicate) && !is_array($predicate))
            return $predicate;

        if (Obj::isAssociativeArray($predicate))
            return function ($obj) use ($predicate) {

                foreach ($predicate as $k => $v) {
                    if (Obj::getValue($obj, $k) != $v)
                        return false;
                }

                return true;
            };


        throw new \InvalidArgumentException("Predicate must be a callable or an associative array");

    }


    /**
     * @return true
     */
    public static function returnTrue(): bool {

        return true;

    }

    /**
     * @return false
     */
    public static function returnFalse(): bool {

        return false;

    }

    public static function identity($obj) {
        return $obj;
    }

    /**
     * @return void
     */
    public static function noop() {
    }


}