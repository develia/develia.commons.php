<?php

namespace Develia;

class Path {

    private function __construct() {
    }

    /**
     * Checks if the given path is a file.
     *
     * @param string $path The path to check.
     * @return bool True if the path is a file, false otherwise.
     */
    public static function isFile(string $path): bool {
        return is_file($path);
    }

    /**
     * Checks if the given path is a directory.
     *
     * @param string $path The path to check.
     * @return bool True if the path is a directory, false otherwise.
     */
    public static function isDirectory(string $path): bool {
        return is_dir($path);
    }

    /**
     * Checks if the given path is a link or device.
     *
     * @param string $path The path to check.
     * @return bool True if the path is a directory, false otherwise.
     */
    public static function isLink(string $path): bool {
        return is_link($path);
    }

    /**
     * Returns the extension of a file path.
     *
     * @param string $file The file path to get the extension from.
     *
     * @return string The extension of the file path.
     */
    public static function getExtension(string $file): string {
        return pathinfo($file, PATHINFO_EXTENSION);
    }

    public static function absolute($path): string {
        $root = null;
        $parts = [];

        if (OS::isWindows()) {
            if (strlen($path) > 2 && $path[1] == ':') {
                $root = substr($path, 0, 3);
                $path = substr($path, 3);
            }
        } else {
            if ($path[0] == "/") {
                $root = "/";
                $path = substr($path, 1);
            }
        }

        if (is_null($root))
            $root = getcwd();

        foreach (explode(DIRECTORY_SEPARATOR, $path) as $part) {
            if ('.' == $part || '' == $part) continue;
            if ('..' == $part) {
                array_pop($parts);
            } else {
                $parts[] = $part;
            }
        }

        return $root . implode(DIRECTORY_SEPARATOR, $parts);
    }

    /**
     * @param string[] $items
     * @return string
     */
    public static function combine(...$items): string {
        $items[0] = rtrim($items[0], '/\\');
        for ($i = 1; $i < count($items); $i++) {
            $items[$i] = trim($items[$i], '/\\');
        }
        return join(DIRECTORY_SEPARATOR, $items);
    }


}