<?php

namespace Develia;

class Algorithm {

    /**
     * Iteratively retrieves and yields items from paginated data.
     *
     * @param callable $pageFactory A callable that accepts page number and items per page,
     *                               and returns an array of items for the specified page.
     * @param int $itemsPerPage The number of items to request per page.
     * @param int $start The page to start retrieving items from.
     * @return \Generator A generator that yields items fetched from the paginated source.
     */
    public static function depaginate(callable $pageFactory, int $itemsPerPage, int $start, int $step): \Generator {

        $page = $start;
        do {

            $items = call_user_func_array($pageFactory, [$page, $itemsPerPage]);
            foreach ($items as $item) {
                yield $item;
            }
            $page += $step;

        } while (count($items) == $itemsPerPage);
    }

    /**
     * Performs a binary search within the specified range and returns the index
     * of the target element based on the comparison logic provided by the getter function.
     *
     * @param int $startIdx The starting index of the range to search within.
     * @param int $endIdx The ending index of the range to search within.
     * @param callable $getter A callback function to compare the mid index value
     *                         against the target. It should return 0 for a match,
     *                         a negative value if the target is smaller, and a positive
     *                         value if the target is larger.
     *
     * @return int The index of the target element, or the adjusted start index if the
     *             target element is not found.
     */
    public static function binarySearch(int $startIdx, int $endIdx, callable $getter): int {

        $start = $startIdx;
        $end = $endIdx;


        while ($start != $end) {

            $mid = intval(($start + $end) / 2);
            $comparison = call_user_func($getter, $mid);

            if ($comparison == 0)
                return $mid;

            if ($comparison < 0)
                $end = $mid - 1;
            else
                $start = $mid + 1;

        }

        return $start;

    }

}