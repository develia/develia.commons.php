<?php

namespace Develia;

use Develia\IO\FileStream;

class Request {

    protected function __construct() { }

    /**
     * @return Request
     */
    public static function current(): Request {
        return new Request();
    }

    /**
     * @return RequestPayload
     */


    public function getQuery(): RequestPayload {
        return new RequestPayload($_GET);
    }

    public function getInputStream($binary = null): FileStream {
        if (is_null($binary)) {
            $binary = MimeType::isBinary(Request::getContentType());
        }

        if ($binary) {
            return new FileStream(FileStream::INPUT, "rb", false);
        } else {
            return new FileStream(FileStream::INPUT, "r", false);
        }
    }

    public function getContentType(): ?string {
        $contentType = null;

        if (isset($_SERVER['CONTENT_TYPE'])) {
            $contentType = $_SERVER['CONTENT_TYPE'];
        } elseif (isset($_SERVER['HTTP_CONTENT_TYPE'])) {
            $contentType = $_SERVER['HTTP_CONTENT_TYPE'];
        }

        return $contentType;
    }


    /**
     * @return array|RequestPayload|false|\SimpleXMLElement|string
     */
    public function getPayload() {
        $contentType = $this->getContentType();

        if (!$contentType ||
            strpos($contentType, 'text/plain') !== false) {
            return $this->getBody();
        }

        if (strpos($contentType, 'application/json') !== false) {
            return $this->getJson();
        }

        if (strpos($contentType, 'multipart/form-data') !== false ||
            strpos($contentType, 'application/x-www-form-urlencoded') !== false) {
            return $this->getForm();
        }

        if (strpos($contentType, 'application/xml') !== false || strpos($contentType, 'text/xml') !== false) {
            return $this->getXml();
        }

        if (strpos($contentType, 'text/csv') !== false) {
            return $this->getCsv();
        }

        return $this->getBody();
    }

    /**
     * @return bool
     */
    public function isPut(): bool {
        return $this->getMethod() == "PUT";
    }

    /**
     * @return mixed
     */
    public function getMethod(): ?string {
        return $_SERVER["REQUEST_METHOD"] ?? null;
    }


    public function getJson(): RequestPayload {
        $body = $this->getBody();
        $data = null;
        if (is_string($body)) {
            $data = json_decode($body, false);
        }
        return new RequestPayload($data);
    }


    /**
     * @return false|string
     */
    public function getBody() {
        return file_get_contents('php://input');
    }

    public function getForm(): RequestPayload {

        $this->parse($form, $files);
        return new RequestPayload($form);
    }

    public function isPost(): bool {
        return $this->getMethod() == "POST";
    }

    public function isAjax(): bool {
        return isset($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) === 'xmlhttprequest';
    }

    public function isGet(): bool {
        return $this->getMethod() == "GET";
    }

    /**
     * @return array|false
     */
    public function getHeaders() {
        return getallheaders();
    }

    public function getHeader($key) {
        $headers = getallheaders();
        if ($headers && array_key_exists($key, $headers)) {
            return $headers[$key];
        }
        return false;
    }


    public function isDelete(): bool {
        return $this->getMethod() == "DELETE";
    }


    public function getServerAddress(): ?string {
        return $_SERVER["SERVER_ADDR"] ?? null;
    }

    /**
     * Parses the input data from the request and populates the provided form and files arrays.
     *
     * @param array|null $form A reference variable to store parsed form data.
     * @param array|null $files A reference variable to store parsed file upload data.
     * @return bool Returns true if the data was successfully parsed, otherwise false.
     */
    public function parse(?array &$form, ?array &$files): bool {

        $form = [];
        $files = [];


        $contentType = $this->getContentType();

        if ($this->isPost()) {
            $form = $_POST;
            $files = $_FILES;
            return true;
        }


        if ($this->isPut() || $this->isPatch()) {
            if (stripos($contentType, 'multipart/form-data') !== false) {

                $input = file_get_contents("php://input");

                preg_match('/boundary=(.*)$/', $contentType, $matches);
                if (!isset($matches[1])) return false;
                $boundary = $matches[1];

                $parts = explode("--" . $boundary, $input);
                array_pop($parts); // Eliminar la última parte vacía

                foreach ($parts as $part) {
                    if (empty(trim($part))) continue;

                    // Extraer nombre del campo y contenido
                    preg_match('/Content-Disposition:.*?name="(.*?)"(?:; filename="(.*?)")?\r\n/', $part, $matches);
                    if (!isset($matches[1])) continue;

                    $fieldName = $matches[1];
                    $fileName = $matches[2] ?? null;

                    // Obtener el contenido después de los encabezados
                    $value = preg_replace('/.*?\r\n\r\n/s', '', $part, 1);

                    if ($fileName) {
                        // Guardar archivo temporalmente
                        $tempPath = sys_get_temp_dir() . "/" . uniqid() . "-" . $fileName;
                        file_put_contents($tempPath, $value);

                        $files[$fieldName] = [
                            'name'     => $fileName,
                            'tmp_name' => $tempPath,
                            'size'     => strlen($value),
                        ];
                    } else {
                        $form[$fieldName] = trim($value);
                    }
                }
                parse_str(file_get_contents("php://input"), $form);
            } else {
                return false;
            }
            return true;
        }

        return false; // No es POST ni PUT

    }

    /**
     * @return int
     */
    public function getServerPort(): ?int {
        return isset($_SERVER["SERVER_PORT"]) ? intval($_SERVER["SERVER_PORT"]) : null;
    }

    /**
     * @return mixed
     */
    public function getClientAddress() {
        return $_SERVER["REMOTE_ADDR"] ?? null;
    }

    /**
     * @return mixed
     */
    public function getClientPort() {
        return $_SERVER["REMOTE_PORT"] ?? null;
    }

    /**
     * @return mixed
     */
    public function getUserAgent() {
        return $_SERVER["HTTP_USER_AGENT"] ?? null;
    }

    /**
     * @return mixed
     */
    public function getReferer() {
        return $_SERVER["HTTP_REFERER"] ?? null;
    }

    /**
     * @return string|null
     */
    public function getRemoteAddr(): ?string {
        return $_SERVER["REMOTE_ADDR"] ?? null;
    }

    /**
     * @return string|null
     */
    public function getScriptName(): ?string {
        return $_SERVER["SCRIPT_NAME"] ?? null;
    }

    /**
     * @return string|null
     */
    public function getServerName(): ?string {
        return $_SERVER["SERVER_NAME"] ?? null;
    }

    /**
     * @return string|null
     */
    public function getScriptFilename(): ?string {
        return $_SERVER["SCRIPT_FILENAME"] ?? null;
    }

    /**
     * @return string|null
     */
    public function getServerAdmin(): ?string {
        return $_SERVER["SERVER_ADMIN"] ?? null;
    }

    /**
     * @return string|null
     */
    public function getServerSignature(): ?string {
        return $_SERVER["SERVER_SIGNATURE"] ?? null;
    }

    /**
     * @return string|null
     */
    public function getAuthType(): ?string {
        return $_SERVER["AUTH_TYPE"] ?? null;
    }

    /**
     * @return string|null
     */
    public function getServerProtocol(): ?string {
        return $_SERVER["SERVER_PROTOCOL"] ?? null;
    }

    /**
     * @return string|null
     */
    public function getPathInfo(): ?string {
        return $_SERVER["PATH_INFO"] ?? null;
    }

    /**
     * @return mixed
     */
    public function getHost() {
        return $_SERVER["HTTP_HOST"] ?? null;
    }

    public function getUrl(): string {
        return (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
    }


    public function getUri() {
        return $_SERVER["REQUEST_URI"] ?? null;
    }


    private function getXml() {
        return simplexml_load_string($this->getBody());
    }


    private function getCsv(): array {
        return str_getcsv($this->getBody());
    }

    private function isPatch(): bool {
        return $this->getMethod() == "PATCH";
    }

}