<?php

namespace Develia;

abstract class Enumeration {

    protected function __construct($value) {
        $this->_value = $value;
    }

    /**
     * @var array
     */
    private static array $_cache = [];

    /**
     * @param $value
     * @return static
     */
    protected static function value($value): Enumeration {

        if (!isset(self::$_cache[static::class])) {
            self::$_cache[static::class] = [];
        }

        if (!isset(self::$_cache[static::class][$value])) {
            self::$_cache[static::class][$value] = new static($value);
        }

        return self::$_cache[static::class][$value];

    }

    /**
     * @var string
     */
    private string $_value;

    /**
     * @return string
     */
    public function getValue(): string {
        return $this->_value;
    }

    /**
     * @param string $value
     * @return static
     */
    public static function fromValue(string $value): Enumeration {
        return call_user_func([static::class, $value]);
    }

}
