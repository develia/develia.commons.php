<?php


namespace Develia;

/**
 * Provides methods to generate random values
 */
class Random {

    private function __construct() {

    }

    /**
     * Generate a random number within a range with specified precision.
     *
     * @param float|int $min The minimum value of the range.
     * @param float|int $max The maximum value of the range.
     * @param int $precision The number of decimal places to round the generated number (default: 2).
     * @return float The generated random number within the range, rounded to the specified precision.
     *                    If the minimum value is greater than the maximum value, returns false.
     */
    public static function real($min, $max, int $precision = 2): float {
        if ($min > $max) {
            Obj::swap($min, $max);
        }

        $numero = $min + mt_rand() / mt_getrandmax() * ($max - $min);

        return round($numero, $precision);
    }

    /**
     * Generates a random integer between the given minimum and maximum values.
     *
     * @param int $min The minimum value for the random integer.
     * @param int $max The maximum value for the random integer.
     *
     * @return int The randomly generated integer between the minimum and maximum values.
     */
    public static function integer(int $min, int $max): int {
        if ($min > $max) {
            Obj::swap($min, $max);
        }

        return mt_rand($min, $max);
    }

    /**
     * Returns a random element from the given array.
     *
     * @param array $array The array from which to select a random element.
     *
     * @return mixed The randomly selected element from the array.
     */
    public static function element(array $array) {
        return $array[array_rand($array)];
    }

    /**
     * Generates a random boolean value.
     *
     * @return bool The randomly generated boolean value.
     */
    public static function boolean(): bool {
        return (bool)mt_rand(0, 1);
    }

}