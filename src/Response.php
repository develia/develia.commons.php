<?php

namespace Develia;

use Develia\IO\FileStream;

class Response {

    protected function __construct() {
    }

    /**
     * @param $object
     * @param int $return_code
     * @return never-returns
     */
    public static function sendJson($object, int $return_code = 200) {
        Response::sendContent(json_encode($object), "application/json", $return_code);
    }

    /**
     * @param $data
     * @param string $content_type
     * @param int $return_code
     * @return never-returns
     */
    public static function sendContent($data, string $content_type = MimeType::TEXT, int $return_code = 200) {

        OB::cleanAll();

        http_response_code($return_code);
        header("Content-Type: " . $content_type);
        echo $data;
        exit();

    }

    /**
     * @param iterable $iterable
     * @param string $content_type
     * @param int $return_code
     * @return void
     * @throws \Exception
     */
    public static function iterable(iterable $iterable, string $content_type = MimeType::TEXT, int $return_code = 200) {
        Response::sendStream(function ($writer) use ($iterable) {
            /** @var FileStream $writer */
            foreach ($iterable as $item) {
                $writer->write($item);
            }
        }, $content_type, $return_code);
    }

    /**
     * @param callable(FileStream):void $callable
     * @param string $content_type
     * @param int $return_code
     * @return void
     * @throws \Exception
     */
    public static function sendStream(callable $callable, string $content_type = MimeType::TEXT, int $return_code = 200) {
        OB::cleanAll();

        http_response_code($return_code);
        header("Content-Type: " . $content_type);

        Run::scoped(new FileStream("php://output", MimeType::isBinary($content_type) ? "wb" : "w", false), function ($stream) use ($callable) {
            call_user_func($callable, $stream);
        });

        exit();
    }

    /**
     * @param string $html
     * @param int $return_code
     * @return never-returns
     */
    public static function sendHtml(string $html, int $return_code = 200) {
        Response::sendContent($html, "text/html", $return_code);
    }

    public static function sendCsv($data, $filaname, $return_code = 200) {

        OB::cleanAll();

        http_response_code($return_code);
        header("Content-type: text/csv");
        header("Cache-Control: no-store, no-cache");
        header('Content-Disposition: attachment; filename="' . $filaname . '"');

        $fp = null;
        try {
            $fp = fopen('php://memory', 'w');
            foreach ($data as $row) {
                fputcsv($fp, array_values($row));
            }

            fseek($fp, 0);
            fpassthru($fp);
        } finally {
            if ($fp)
                fclose($fp);
        }
        exit();

    }

    public static function sendStatusCode($code) {
        OB::cleanAll();

        http_response_code($code);
        exit();
    }

    public static function redirect($url, $statusCode = 302) {
        OB::cleanAll();

        header('Location: ' . $url, true, $statusCode);
        exit();
    }

    /**
     * @param string $filePath
     * @param null $contentType
     * @param string|null $downloadFileName
     * @return never-returns
     */
    public static function sendFile(string $filePath, $contentType = null, ?string $downloadFileName = null) {


        if (!file_exists($filePath)) {
            http_response_code(404);
            exit;
        }


        header('Content-Description: File Transfer');
        header('Content-Type: ' . $contentType ?: MimeType::fromExtension(pathinfo($filePath, PATHINFO_EXTENSION)));
        header('Content-Disposition: attachment; filename="' . ($downloadFileName ?: basename($filePath)) . '"');
        header('Expires: 0');
        header('Cache-Control: must-revalidate');
        header('Pragma: public');
        header('Content-Length: ' . filesize($filePath));

        while (ob_get_level())
            ob_end_clean();

        readfile($filePath);
        exit();
    }

}