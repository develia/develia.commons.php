<?php

namespace Develia\Collections;


/**
 * Class DefaultDictionary
 *
 * This class acts as a dictionary that provides a default value for keys that do not exist.
 * It implements the ArrayAccess, IteratorAggregate, and Countable interfaces to act like an array.
 */
class Dictionary implements \ArrayAccess, \IteratorAggregate, \Countable {


    protected array $keys = [];

    protected array $values = [];

    protected $equality_comparer;

    protected function default_equality_comparer($key1, $key2): bool {

        if (is_object($key1) && is_object($key2)) {
            return $key1 === $key2;
        }
        return $key1 == $key2;

    }

    /**
     * DefaultDictionary constructor.
     *
     * @param callable|null $equality_comparer Function that compares two keys for equality.
     */
    public function __construct(callable $equality_comparer = null) {
        $this->equality_comparer = $equality_comparer ?: [self::class, 'default_equality_comparer'];
    }

    public function offsetExists($offset): bool {
        return $this->getKeyIndex($offset) !== -1;
    }

    protected function getKeyIndex($key) {
        foreach ($this->keys as $index => $existingKey) {
            if (call_user_func($this->equality_comparer, $existingKey, $key)) {
                return $index;
            }
        }

        return -1;
    }

    public function offsetGet($offset) {
        return $this->values[$this->getKeyIndex($offset)];
    }

    public function offsetSet($offset, $value) {
        $index = $this->getKeyIndex($offset);

        if ($index === -1) {
            $this->keys[] = $offset;
            $this->values[] = $value;
        } else {
            $this->values[$index] = $value;
        }
    }

    public function offsetUnset($offset) {
        $index = $this->getKeyIndex($offset);

        if ($index !== -1) {
            unset($this->keys[$index]);
            unset($this->values[$index]);
        }
    }

    public function getIterator() {
        for ($i = 0; $i < count($this->keys); $i++) {
            yield $this->keys[$i] => $this->values[$i];
        }
    }

    public function count(): int {
        return count($this->keys);
    }

    /**
     * @return array
     */
    public function getValues(): array {
        return $this->values;
    }

    /**
     * @return array
     */
    public function getKeys(): array {
        return $this->keys;
    }

    /**
     * @return callable|\Closure
     */
    protected function getEqualityComparer() {
        return $this->equality_comparer;
    }

    public function tryGetValue($key, &$output, $fallback = null): bool {
        if (isset($this[$key])) {
            $output = $this[$key];
            return true;
        }
        $output = $fallback;
        return false;
    }


}