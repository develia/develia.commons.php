<?php

namespace Develia\Collections;


class ArraySlice implements \ArrayAccess, \Countable, \IteratorAggregate {

    private array $array;

    private int $offset;

    private $length;

    /**
     * @param array| (\ArrayAccess & \Countable & \IteratorAggregate) $array
     * @param int $offset
     * @param int | null $length
     */
    public function __construct(&$array, int $offset = 0, ?int $length = null) {
        $this->array = &$array;
        $this->offset = $offset;

        if ($length === null) {
            $this->length = count($this->array) - $this->offset;
        } else {
            $this->length = min($length, count($this->array) - $this->offset);
        }
    }

    public function offsetExists($offset): bool {
        return isset($this->array[$this->calculateRealOffset($offset)]);
    }

    public function offsetGet($offset) {
        return $this->array[$this->calculateRealOffset($offset)];
    }

    public function offsetSet($offset, $value) {
        $this->array[$this->calculateRealOffset($offset)] = $value;
    }

    public function offsetUnset($offset) {
        unset($this->array[$this->calculateRealOffset($offset)]);
    }

    public function count() {
        return $this->length;
    }

    public function getIterator() {
        $count = $this->count();
        for ($i = 0; $i < $count; $i++) {
            yield $this->array[$this->calculateRealOffset($i)];
        }
    }

    /**
     * @throws \Exception
     */
    public function toArray(): array {
        return iterator_to_array($this->getIterator());
    }

    /**
     * @return array
     */
    public function & getArray(): array {
        return $this->array;
    }

    /**
     * @return int
     */
    public function getOffset(): int {
        return $this->offset;
    }

    private function calculateRealOffset($offset) {
        return $this->offset + $offset;
    }

}