<?php

namespace Develia\Collections;

class DefaultDictionary extends Dictionary {
    /**
     * @var callable
     */
    private $fallback_fn;

    public function __construct(callable $fallback_fn, callable $eq_comparer = null) {
        parent::__construct($eq_comparer);
        $this->fallback_fn = $fallback_fn;
    }

    public function offsetGet($offset) {
        $index = $this->getKeyIndex($offset);

        if ($index === -1) {
            $value = call_user_func($this->fallback_fn, $offset);
            $this[$offset] = $value;
            return $value;
        }

        return $this->values[$index];
    }
}