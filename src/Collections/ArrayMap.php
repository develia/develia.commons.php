<?php

namespace Develia\Collections;


class ArrayMap implements \ArrayAccess, \Countable, \IteratorAggregate {


    /**
     * @var array|(\Countable & \ArrayAccess)
     */
    private $array;

    /**
     * @var array
     */
    private array $map;


    /**
     * @param array| (\ArrayAccess & \Countable) $array
     * @param array $map
     */
    public function __construct(&$array, array $map) {

        $this->array = &$array;
        $this->map = $map;
    }

    /**
     * @throws \Exception
     */
    public function toArray(): array {
        return iterator_to_array($this->getIterator());
    }

    /**
     * @return array
     */
    public function & getArray() {
        return $this->array;
    }

    public function getIterator() {
        foreach ($this->map as $k => $v) {
            yield $k => $this->array[$v];
        }
    }

    public function offsetExists($offset): bool {
        return isset($this->array[$this->map[$offset]]);
    }

    public function offsetGet($offset) {
        return $this->array[$this->map[$offset]];
    }

    public function offsetSet($offset, $value) {
        return $this->array[$this->map[$offset]] = $value;
    }

    public function offsetUnset($offset) {
        unset($this->array[$this->map[$offset]]);
    }

    public function count(): int {
        return count($this->map);
    }


}