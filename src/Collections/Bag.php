<?php

namespace Develia\Collections;


/**
 * Class Bag
 *
 * A Bag (or multiset) implementation in PHP that supports objects as keys and allows custom equality comparison.
 */
class Bag implements \ArrayAccess, \Countable, \IteratorAggregate {


    private array $keys = [];

    private array $values = [];

    private $equalityComparer;

    /**
     * Constructor
     *
     * @param callable|null $equalityComparer A callable that takes two parameters and returns a boolean indicating whether they are considered equal.
     */
    public function __construct(callable $equalityComparer = null) {
        $this->equalityComparer = $equalityComparer ?: function ($a, $b) {
            return $a == $b;
        };
    }

    /**
     * @param iterable $array
     * @param callable | null $eq_comparer
     * @return Bag
     */
    public static function fromIterable(iterable $array, ?callable $eq_comparer = null): Bag {
        $bag = new Bag($eq_comparer);
        foreach ($array as $k => $v) {
            $bag->add($k, $v);
        }
        return $bag;
    }

    /**
     * Inserts an item into the Bag, possibly multiple times.
     *
     * @param mixed $item The item to insert.
     * @param int $count The number of items to insert.
     */
    public function add($item, int $count = 1) {
        $index = $this->findKey($item);
        if ($index != -1) {
            $this->values[$index] += $count;
        } else {
            $this->keys[] = $item;
            $this->values[] = $count;
        }
    }

    /**
     * Finds the position of an item in the key array.
     *
     * @param mixed $item The item to find.
     * @return int The index of the item in the keys array, or -1 if not found.
     */
    private function findKey($item): int {
        foreach ($this->keys as $index => $key) {
            if (call_user_func($this->equalityComparer, $item, $key)) {
                return $index;
            }
        }
        return -1;
    }

    /**
     * Checks whether an offset exists.
     *
     * @param mixed $offset The offset to check.
     * @return bool Whether the offset exists.
     */
    public function offsetExists($offset): bool {
        return $this->find($offset);
    }

    /**
     * Checks if an item exists in the Bag.
     *
     * @param mixed $item The item to find.
     * @return bool Whether the item was found.
     */
    public function find($item): bool {
        return $this->findKey($item) != -1;
    }

    /**
     * Gets the value at an offset.
     *
     * @param mixed $offset The offset to retrieve.
     * @return int The value at the offset.
     */
    public function offsetGet($offset): int {
        return $this->count($offset);
    }

    /**
     * Counts the occurrences of an item or the numbers of keys in the Bag.
     *
     * @param mixed | null $item The item to count.
     * @return int The number of occurrences of the item if $item is null. Else the numbers of keys.
     */
    public function count($item = null): int {

        if (!is_null($item)) {
            $index = $this->findKey($item);
            return $index != -1 ? $this->values[$index] : 0;
        }
        return count($this->keys);
    }

    /**
     * Sets the value at an offset.
     *
     * @param mixed $offset The offset to assign the value to.
     * @param mixed $value The value to set.
     */
    public function offsetSet($offset, $value) {
        $count = $this->count($offset);
        if ($count > $value) {
            $this->subtract($offset, $count - $value);
        } else {
            $this->add($offset, $value - $count);
        }
    }

    /**
     * Deletes an item from the Bag, possibly multiple times.
     *
     * @param mixed $item The item to delete.
     * @param int $count The number of items to delete.
     */
    public function subtract($item, int $count = 1): int {
        $index = $this->findKey($item);
        if ($index === -1) return $count;

        $new_qty = $this->values[$index] -= $count;
        if ($new_qty <= 0) {
            array_splice($this->keys, $index, 1);
            array_splice($this->values, $index, 1);
            return -$new_qty;
        }

        return 0;
    }

    /**
     * Unsets an offset.
     *
     * @param mixed $offset The offset to unset.
     */
    public function offsetUnset($offset) {
        $this->subtract($offset, $this->count($offset));
    }

    public function getIterator() {
        for ($i = 0; $i < count($this->keys); $i++) {
            yield $this->keys[$i] => $this->values[$i];
        }
    }

    public function addBag($bag) {
        foreach ($bag as $k => $v) {
            $this->add($k, $v);
        }
    }

    /**
     * @param $bag
     * @return Bag
     */
    public function subtractBag($bag): Bag {
        $output = new Bag();

        foreach ($bag as $k => $v) {

            $diff = $this->subtract($k, $v);
            if ($diff > 0)
                $output->add($k, $diff);
        }

        return $output;
    }

    public function containsBag($bag): bool {
        foreach ($bag as $k => $v) {
            if (!$this->contains($k, $v))
                return false;
        }

        return true;
    }

    public function contains($k, $amount): bool {
        return $this[$k] >= $amount;
    }

}