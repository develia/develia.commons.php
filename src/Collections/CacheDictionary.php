<?php

namespace Develia\Collections;

use DateTimeInterface;

/**
 * Class Cache
 * Implements the ArrayAccess interface to simulate an associative array with caching functionality.
 */
class CacheDictionary implements \ArrayAccess, \Countable, \IteratorAggregate {

    /**
     * @var callable|null Backup function to generate the value of a key if it's not in the cache.
     */
    private $fallback;

    /**
     * @var array Stores the cached values.
     */
    private array $cache = [];

    /**
     * @var int|null Default duration in seconds for a key in the cache.
     */
    private ?int $defaultDuration;

    /**
     * @var DateTimeInterface[] Stores the expiration dates of elements in the cache.
     */
    private array $expiration = [];

    /**
     * Cache constructor.
     *
     * @param callable|null $fallback_fn Backup function to generate the value of a key if it's not in the cache.
     * @param int|null $defaultDuration Default duration in seconds for a key in the cache.
     */
    public function __construct(?callable $fallback_fn = null, ?int $defaultDuration = null) {
        $this->fallback = $fallback_fn;
        $this->defaultDuration = $defaultDuration;
    }

    /**
     * Checks if a key exists in the cache.
     *
     * @param mixed $offset The key to check.
     * @return bool Returns true if the key exists, false otherwise.
     */
    public function offsetExists($offset): bool {
        return isset($this->cache[$offset]);
    }


    public function offsetGet($offset) {
        if (!isset($this->cache[$offset])) {
            if ($this->fallback) {
                $this->cache[$offset] = call_user_func_array($this->fallback, [$offset]);
                $this->expiration[$offset] = $this->calculateExpiration($this->defaultDuration);
            } else {
                return null;
            }
        }

        if ($this->isExpired($offset)) {
            $this->remove($offset);
            return null;
        }

        return $this->cache[$offset];
    }

    /**
     * Gets the value of a key in the cache.
     *
     * @param mixed $key The key to retrieve.
     * @return mixed|null The value of the key if it exists and is not expired, null otherwise.
     */
    public function get($key) {
        return $this->offsetGet($key);
    }

    /**
     * Sets the value of a key in the cache.
     *
     * @param mixed $offset The key to set.
     * @param mixed $value The value to set.
     * @return void
     */
    public function offsetSet($offset, $value) {
        $this->cache[$offset] = $value;
        $this->expiration[$offset] = $this->calculateExpiration($this->defaultDuration);
    }

    /**
     * Removes a key from the cache.
     *
     * @param mixed $offset The key to remove.
     * @return void
     */
    public function offsetUnset($offset) {
        $this->remove($offset);
    }


    /**
     * Sets the value of a key in the cache with an optional duration.
     *
     * @param mixed $key The key to set.
     * @param mixed $value The value to set.
     * @param int|null $duration The duration in seconds for the key to remain in the cache.
     * @return void
     */
    public function set($key, $value, ?int $duration = null) {
        $this->cache[$key] = $value;
        $this->expiration[$key] = $this->calculateExpiration($duration);
    }

    /**
     * Clears a key from the cache.
     *
     * @param mixed $key The key to clear.
     * @return void
     */
    public function remove($key) {
        unset($this->cache[$key]);
        unset($this->expiration[$key]);
    }

    /**
     * Gets the expiration date of an element in the cache.
     *
     * @param mixed $key The key of the element.
     * @return DateTimeInterface|null The expiration date of the element if it exists, null otherwise.
     */
    public function getExpiration($key): ?DateTimeInterface {
        return $this->expiration[$key] ?? null;
    }

    /**
     * Sets the expiration date of an element in the cache.
     *
     * @param mixed $key The key of the element.
     * @param int|DateTimeInterface|null $expiration The expiration date as a timestamp, DateTimeInterface object, or null.
     * @return void
     */
    public function setExpiration($key, $expiration) {
        if ($expiration instanceof DateTimeInterface) {
            $this->expiration[$key] = $expiration;
        } else {
            unset($this->expiration[$key]);
        }
    }

    /**
     * Sets the duration for an element in the cache.
     *
     * @param mixed $key The key of the element.
     * @param int|null $duration The duration in seconds for the key to remain in the cache.
     * @return void
     */
    public function setDuration($key, ?int $duration) {
        if ($duration === null) {
            unset($this->expiration[$key]);
        } else {
            $expiration = (new \DateTimeImmutable())->setTimestamp(time() + $duration);
            $this->expiration[$key] = $expiration;
        }
    }

    public function count(): int {
        return count($this->cache);
    }

    /**
     * Checks if a key has expired.
     *
     * @param mixed $key The key to check.
     * @return bool Returns true if the key has expired, false otherwise.
     */
    private function isExpired($key): ?bool {
        if (isset($this->expiration[$key])) {
            $expiration = $this->expiration[$key];
            return $expiration instanceof DateTimeInterface && $expiration < new \DateTimeImmutable();
        }

        return null;
    }

    /**
     * Calculates the expiration date based on the default duration or a given duration.
     *
     * @param int|null $duration The duration in seconds.
     * @return DateTimeInterface|null The expiration date or null if no expiration is set.
     */
    private function calculateExpiration(?int $duration = null): ?DateTimeInterface {
        if ($duration === null) {
            return null;
        }

        return (new \DateTimeImmutable())->setTimestamp(time() + $duration);
    }

    public function getIterator(): \ArrayIterator {
        return new \ArrayIterator($this->cache);
    }

}