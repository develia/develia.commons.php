<?php


namespace Develia\Collections;

/**
 * Class BidirectionalDictionary
 *
 * This class implements a bidirectional dictionary allowing unique key-value
 * and value-key mappings. It supports adding, retrieving, and removing pairs
 * by both key and value.
 */
class BidirectionalDictionary {

    private array $keyValueMap = [];

    private array $valueKeyMap = [];


    public function __construct(array $initialData = []) {
        foreach ($initialData as $key => $value) {
            $this->add($key, $value);
        }
    }

    /**
     * Adds a key-value pair to the maps if it does not already exist.
     *
     * @param mixed $key The key to add.
     * @param mixed $value The value to add.
     * @return void
     * @throws \InvalidArgumentException If the key-value pair already exists.
     */
    public function add($key, $value) {
        if (isset($this->keyValueMap[$key]) || isset($this->valueKeyMap[$value])) {
            throw new \InvalidArgumentException("The key-value pair already exists.");
        }
        $this->keyValueMap[$key] = $value;
        $this->valueKeyMap[$value] = $key;
    }

    // Get value by key

    /**
     * Retrieves the value associated with the given key.
     *
     * @param mixed $key The key for which the value needs to be fetched.
     * @return mixed The value associated with the provided key.
     * @throws \OutOfBoundsException If the key is not found in the key-value map.
     */
    public function getValue($key) {
        if (!isset($this->keyValueMap[$key])) {
            throw new \OutOfBoundsException("Key not found.");
        }
        return $this->keyValueMap[$key];
    }

    // Get key by value

    /**
     * Retrieves the key associated with the given value.
     *
     * @param mixed $value The value for which to find the associated key.
     * @return mixed The key associated with the given value.
     * @throws \OutOfBoundsException If the value is not found in the map.
     */
    public function getKey($value) {
        if (!isset($this->valueKeyMap[$value])) {
            throw new \OutOfBoundsException("Value not found.");
        }
        return $this->valueKeyMap[$value];
    }

    // Remove a key-value pair by key

    /**
     * Removes the key-value pair from the maps for the given key.
     *
     * @param mixed $key The key to be removed.
     * @return void
     */
    public function removeByKey($key) {
        if (!isset($this->keyValueMap[$key])) {
            throw new \OutOfBoundsException("Key not found.");
        }
        $value = $this->keyValueMap[$key];
        unset($this->keyValueMap[$key]);
        unset($this->valueKeyMap[$value]);
    }

    // Remove a key-value pair by value

    /**
     * Removes a key-value pair from the map by its value.
     *
     * @param mixed $value The value to be removed.
     * @return void
     * @throws \OutOfBoundsException If the value does not exist in the map.
     */
    public function removeByValue($value) {
        if (!isset($this->valueKeyMap[$value])) {
            throw new \OutOfBoundsException("Value not found.");
        }
        $key = $this->valueKeyMap[$value];
        unset($this->valueKeyMap[$value]);
        unset($this->keyValueMap[$key]);
    }

    // Check if a key exists

    /**
     * Checks if the specified key exists in the key-value map.
     *
     * @param mixed $key The key to check for existence in the key-value map.
     * @return bool Returns true if the key exists, false otherwise.
     */
    public function hasKey($key): bool {
        return isset($this->keyValueMap[$key]);
    }

    // Check if a value exists

    /**
     * Checks if the specified value exists in the valueKeyMap array.
     *
     * @param mixed $value The value to check for existence in the valueKeyMap array.
     * @return bool True if the value exists, false otherwise.
     */
    public function hasValue($value): bool {
        return isset($this->valueKeyMap[$value]);
    }

    // Get all key-value pairs

    /**
     * Retrieves all elements from the key-value map.
     *
     * @return array All elements stored in the key-value map.
     */
    public function getAll(): array {
        return $this->keyValueMap;
    }

}