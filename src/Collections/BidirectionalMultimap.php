<?php

namespace Develia\Collections;

class BidirectionalMultimap {

    private array $keyToValues = [];

    private array $valueToKeys = [];

    // Add a relationship between a key and a value
    public function add($key, $value) {
        // Add value to the list of values for this key
        $this->keyToValues[$key][$value] = true;

        // Add key to the list of keys for this value
        $this->valueToKeys[$value][$key] = true;
    }

    // Remove a relationship between a key and a value
    public function remove($key, $value) {
        unset($this->keyToValues[$key][$value]);
        if (empty($this->keyToValues[$key])) {
            unset($this->keyToValues[$key]);
        }

        unset($this->valueToKeys[$value][$key]);
        if (empty($this->valueToKeys[$value])) {
            unset($this->valueToKeys[$value]);
        }
    }

    // Get all values associated with a given key
    public function getValues($key): array {
        return array_keys($this->keyToValues[$key] ?: []);
    }

    // Get all keys associated with a given value
    public function getKeys($value): array {
        return array_keys($this->valueToKeys[$value] ?: []);
    }

    // Check if a specific key-value pair exists
    public function hasPair($key, $value): bool {
        return isset($this->keyToValues[$key][$value]);
    }

    // Check if a key exists
    public function hasKey($key): bool {
        return isset($this->keyToValues[$key]);
    }

    // Check if a value exists
    public function hasValue($value): bool {
        return isset($this->valueToKeys[$value]);
    }

}