<?php

namespace Develia;

class MimeType {

    const HTML = "text/html";
    const CSS = "text/css";
    const JAVASCRIPT = "application/javascript";
    const JSON = "application/json";
    const XML = "application/xml";
    const SVG = "image/svg+xml";
    const JPEG = "image/jpeg";
    const PNG = "image/png";
    const GIF = "image/gif";
    const BMP = "image/bmp";
    const WEBP = "image/webp";
    const TIFF = "image/tiff";
    const MP4 = "video/mp4";
    const QUICKTIME = "video/quicktime";
    const AVI = "video/x-msvideo";
    const WMV = "video/x-ms-wmv";
    const MP3 = "audio/mpeg";
    const WAV = "audio/wav";
    const REAL_AUDIO = "audio/x-realaudio";
    const PDF = "application/pdf";
    const MSWORD = "application/msword";
    const MSEXCEL = "application/vnd.ms-excel";
    const MSPOWERPOINT = "application/vnd.ms-powerpoint";
    const OPENOFFICE = "application/vnd.openxmlformats-officedocument";
    const FLASH = "application/x-shockwave-flash";
    const ZIP = "application/zip";
    const RAR = "application/x-rar-compressed";
    const TAR = "application/x-tar";
    const GZIP = "application/gzip";
    const SEVEN_ZIP = "application/x-7z-compressed";
    const TEXT = "text/plain";
    const RTF = "application/rtf";
    const SQL = "application/sql";
    const CSV = "text/csv";
    const TSV = "text/tab-separated-values";
    const ICO = "image/vnd.microsoft.icon";
    const OGG_VIDEO = "video/ogg";
    const OGG_AUDIO = "audio/ogg";
    const WEBM_AUDIO = "audio/webm";
    const WEBM_VIDEO = "video/webm";
    const WEBM = "application/webm";
    const DART = "application/dart";
    const XHTML = "application/xhtml+xml";
    const ATOM = "application/atom+xml";
    const RSS = "application/rss+xml";
    const FORM_URLENCODED = "application/x-www-form-urlencoded";
    const MULTIPART_FORMDATA = "multipart/form-data";
    const CSHARP = "text/x-csharp";
    const MARKDOWN = "text/markdown";
    const YAML = "text/yaml";
    const MJPEG = "video/x-motion-jpeg";
    const FLV = "video/x-flv";
    const TTF = "font/ttf";
    const WOFF = "font/woff";
    const WOFF2 = "font/woff2";
    const EOT = "application/vnd.ms-fontobject";
    const SFNT = "application/font-sfnt";
    const BIN = "application/octet-stream";
    const DMG = "application/x-apple-diskimage";
    const ISO = "application/x-iso9660-image";
    const JAR = "application/java-archive";
    const PPTX = "application/vnd.openxmlformats-officedocument.presentationml.presentation";
    const DOCX = "application/vnd.openxmlformats-officedocument.wordprocessingml.document";
    const XLSX = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
    const EPUB = "application/epub+zip";
    const PS = "application/postscript";
    const OTF = "font/otf";
    const XHTML_MOBILE = "application/vnd.wap.xhtml+xml";
    const CRX = "application/x-chrome-extension";
    const XUL = "application/vnd.mozilla.xul+xml";
    const CBZ = "application/x-cbz";
    const CBR = "application/x-cdisplay";
    const AMR = "audio/amr";
    const EXE = "application/x-msdownload";
    const DLL = "application/x-msdownload";
    const DEB = "application/vnd.debian.binary-package";
    const RPM = "application/x-rpm";
    const ELF = "application/x-executable";
    const SWF = "application/vnd.adobe.flash.movie";
    const AIR = "application/vnd.adobe.air-application-installer-package+zip";
    const APK = "application/vnd.android.package-archive";
    const IPA = "application/octet-stream";
    const AU = "audio/basic";
    const MIDI = "audio/midi";
    const M4A = "audio/x-m4a";
    const H264 = "video/h264";
    const MKV = "video/x-matroska";
    const THREE_GP = "video/3gpp";
    const THREE_G2 = "video/3gpp2";
    const TS = "video/MP2T";
    const MPEG = "video/mpeg";
    const FLAC = "audio/flac";
    const X_FLAC = "audio/x-flac";
    const X_ICON = "image/x-icon";
    const TGA = "image/x-tga";
    const CMYK = "application/vnd.adobe.photoshop";
    const PSD = "image/vnd.adobe.photoshop";
    const AI = "application/postscript";
    const EPS = "application/postscript";
    const AZW = "application/vnd.amazon.ebook";
    const CBC = "application/vnd.oasis.opendocument.database";
    const ODT = "application/vnd.oasis.opendocument.text";
    const ODS = "application/vnd.oasis.opendocument.spreadsheet";

    private static array $extension_to_mime = [
        'html'  => self::HTML,
        'css'   => self::CSS,
        'js'    => self::JAVASCRIPT,
        'json'  => self::JSON,
        'xml'   => self::XML,
        'svg'   => self::SVG,
        'jpg'   => self::JPEG,
        'jpeg'  => self::JPEG,
        'png'   => self::PNG,
        'gif'   => self::GIF,
        'webp'  => self::WEBP,
        'tiff'  => self::TIFF,
        'tif'   => self::TIFF,
        'mp4'   => self::MP4,
        'm4v'   => self::MP4,
        'mpg'   => self::MPEG,
        'mov'   => self::QUICKTIME,
        'avi'   => self::AVI,
        'wmv'   => self::WMV,
        'mp3'   => self::MP3,
        'wav'   => self::WAV,
        'mid'   => self::MIDI,
        'ra'    => self::REAL_AUDIO,
        'pdf'   => self::PDF,
        'doc'   => self::MSWORD,
        'xls'   => self::MSEXCEL,
        'ppt'   => self::MSPOWERPOINT,
        'odf'   => self::OPENOFFICE,
        'zip'   => self::ZIP,
        'rar'   => self::RAR,
        'tar'   => self::TAR,
        'gz'    => self::GZIP,
        '7z'    => self::SEVEN_ZIP,
        'txt'   => self::TEXT,
        'rtf'   => self::RTF,
        'sql'   => self::SQL,
        'csv'   => self::CSV,
        'tsv'   => self::TSV,
        'ogv'   => self::OGG_VIDEO,
        'oga'   => self::OGG_AUDIO,
        'webma' => self::WEBM_AUDIO,
        'webmv' => self::WEBM_VIDEO,
        'webm'  => self::WEBM,
        'dart'  => self::DART,
        'xhtml' => self::XHTML,
        'atom'  => self::ATOM,
        'rss'   => self::RSS,
        'cs'    => self::CSHARP,
        'md'    => self::MARKDOWN,
        'yaml'  => self::YAML,
        'mjpeg' => self::MJPEG,
        'flv'   => self::FLV,
        // Adding more for the additional 50 extensions
        'ttf'   => self::TTF,
        'woff'  => self::WOFF,
        'woff2' => self::WOFF2,
        'eot'   => self::EOT,
        'bin'   => self::BIN,
        'dmg'   => self::DMG,
        'iso'   => self::ISO,
        'jar'   => self::JAR,
        'pptx'  => self::PPTX,
        'docx'  => self::DOCX,
        'xlsx'  => self::XLSX,
        'epub'  => self::EPUB,
        'ps'    => self::PS,
        'otf'   => self::OTF,
        'xht'   => self::XHTML_MOBILE,
        'crx'   => self::CRX,
        'xul'   => self::XUL,
        'cbz'   => self::CBZ,
        'cbr'   => self::CBR,
        'amr'   => self::AMR,
        'exe'   => self::EXE,
        'dll'   => self::DLL,
        'deb'   => self::DEB,
        'rpm'   => self::RPM,
        'elf'   => self::ELF,
        'swf'   => self::SWF,
        'apk'   => self::APK,
        'ipa'   => self::IPA,
        'au'    => self::AU,
        'midi'  => self::MIDI,
        'm4a'   => self::M4A,
        'h264'  => self::H264,
        'mkv'   => self::MKV,
        '3gp'   => self::THREE_GP,
        '3g2'   => self::THREE_G2,
        'ts'    => self::TS,
        'mpeg'  => self::MPEG,
        'flac'  => self::FLAC,
        'xflac' => self::X_FLAC,
        'bmp'   => self::BMP,
        'ico'   => self::X_ICON,
        'tga'   => self::TGA,
        'cmyk'  => self::CMYK,
        'psd'   => self::PSD,
        'ai'    => self::AI,
        'eps'   => self::EPS,
        'azw'   => self::AZW,
        'cbc'   => self::CBC,
        'odt'   => self::ODT,
        'ods'   => self::ODS
    ];

    private function __construct() { }

    public static function isBinary($mime_type): bool {
        return !in_array($mime_type, [
            MimeType::JAVASCRIPT,
            MimeType::XML,
            MimeType::TEXT,
            MimeType::HTML,
            MimeType::JSON,
            MimeType::CSV,
            MimeType::FORM_URLENCODED
        ]);
    }

    public static function fromFile($path): string {
        $info = pathinfo($path);

        if (isset($info['extension'])) {
            return self::fromExtension($info['extension']);
        } else {
            return self::BIN;
        }
    }

    public static function fromExtension($extension): string {
        if (array_key_exists($extension, self::$extension_to_mime)) {
            return self::$extension_to_mime[$extension];
        } else {
            return self::BIN;
        }
    }

}