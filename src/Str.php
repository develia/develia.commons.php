<?php

namespace Develia;

final class Str {

    private static ?bool $multibyte = null;

    private function __construct() { }

    /**
     * Verifica si una cadena puede ser un nombre de propiedad válido en PHP.
     *
     * Los nombres de las propiedades válidos deben comenzar con una letra o un guión bajo,
     * seguidos por cualquier número de letras, números o guiones bajos.
     *
     * @param string|null $property La cadena a verificar.
     *
     * @return bool Retorna verdadero si la cadena es un nombre de propiedad válido, falso en caso contrario.
     */
    public static function isValidPHPPropertyName(?string $property): bool {
        return is_string($property) && preg_match('/^[a-zA-Z_\x80-\xff][a-zA-Z0-9_\x80-\xff]*$/', $property);
    }


    /**
     * @param string $input
     * @param string $characters
     * @return string
     */
    public static function rightTrim(string $input, string $characters): string {
        return rtrim($input, $characters);
    }

    public static function trim(string $input, string $characters): string {
        return trim($input, $characters);
    }


    /**
     * @param string $input
     * @param string $characters
     * @return string
     */
    public static function leftTrim(string $input, string $characters): string {
        return ltrim($input, $characters);
    }

    /**
     * @param string $haystack
     * @param string $needle
     * @param int $offset
     * @return bool
     */
    public static function contains(string $haystack, string $needle, int $offset = 0): bool {
        return strpos($haystack, $needle, $offset) !== false;
    }

    /**
     * @param string $haystack
     * @param string $needle
     * @param bool|null $multibyte
     * @return bool
     */
    public static function endsWith(string $haystack, string $needle, ?bool $multibyte = null): bool {
        $length = Str::length($needle, $multibyte);
        if ($length == 0) {
            return true;
        }

        return (substr($haystack, -$length) === $needle);
    }

    /**
     * @param $str
     * @param $multibyte
     * @return int
     */
    public static function length($str, $multibyte = null): int {
        if (is_null($multibyte) ? Str::getUsingMultiByte() : $multibyte) {
            return mb_strlen($str);
        }
        return strlen($str);
    }

    /**
     * @return bool
     */
    public static function getUsingMultiByte(): bool {
        if (is_null(Str::$multibyte)) {
            Str::$multibyte = function_exists("mb_strtoupper");
        }

        return Str::$multibyte;
    }

    /**
     * @param string $str
     * @return string
     */
    public static function firstLetterToLower(string $str): string {
        return lcfirst($str);
    }

    /**
     * @param string $str
     * @return string
     */
    public static function firstLetterToUpper(string $str): string {
        return ucfirst($str);
    }


    /**
     * @param string $text
     * @param string $pattern
     * @param bool|null $multibyte
     * @return bool
     */
    public static function like(string $text, string $pattern, ?bool $multibyte = null): bool {

        $text = self::removeAccents(self::toLower(trim($text), $multibyte));
        $pattern = self::removeAccents(self::toLower(trim($pattern), $multibyte));


        return strpos($text, $pattern) !== false;
    }

    /**
     * @param string $string
     * @return mixed|string
     */
    public static function removeAccents(string $string): string {
        if (!preg_match('/[\x80-\xff]/', $string)) {
            return $string;
        }

        $chars = array(
            // Decompositions for Latin-1 Supplement
            chr(195) . chr(128) => 'A',
            chr(195) . chr(129) => 'A',
            chr(195) . chr(130) => 'A',
            chr(195) . chr(131) => 'A',
            chr(195) . chr(132) => 'A',
            chr(195) . chr(133) => 'A',
            chr(195) . chr(135) => 'C',
            chr(195) . chr(136) => 'E',
            chr(195) . chr(137) => 'E',
            chr(195) . chr(138) => 'E',
            chr(195) . chr(139) => 'E',
            chr(195) . chr(140) => 'I',
            chr(195) . chr(141) => 'I',
            chr(195) . chr(142) => 'I',
            chr(195) . chr(143) => 'I',
            chr(195) . chr(145) => 'N',
            chr(195) . chr(146) => 'O',
            chr(195) . chr(147) => 'O',
            chr(195) . chr(148) => 'O',
            chr(195) . chr(149) => 'O',
            chr(195) . chr(150) => 'O',
            chr(195) . chr(153) => 'U',
            chr(195) . chr(154) => 'U',
            chr(195) . chr(155) => 'U',
            chr(195) . chr(156) => 'U',
            chr(195) . chr(157) => 'Y',
            chr(195) . chr(159) => 's',
            chr(195) . chr(160) => 'a',
            chr(195) . chr(161) => 'a',
            chr(195) . chr(162) => 'a',
            chr(195) . chr(163) => 'a',
            chr(195) . chr(164) => 'a',
            chr(195) . chr(165) => 'a',
            chr(195) . chr(167) => 'c',
            chr(195) . chr(168) => 'e',
            chr(195) . chr(169) => 'e',
            chr(195) . chr(170) => 'e',
            chr(195) . chr(171) => 'e',
            chr(195) . chr(172) => 'i',
            chr(195) . chr(173) => 'i',
            chr(195) . chr(174) => 'i',
            chr(195) . chr(175) => 'i',
            chr(195) . chr(177) => 'n',
            chr(195) . chr(178) => 'o',
            chr(195) . chr(179) => 'o',
            chr(195) . chr(180) => 'o',
            chr(195) . chr(181) => 'o',
            chr(195) . chr(182) => 'o',
            chr(195) . chr(185) => 'u',
            chr(195) . chr(186) => 'u',
            chr(195) . chr(187) => 'u',
            chr(195) . chr(188) => 'u',
            chr(195) . chr(189) => 'y',
            chr(195) . chr(191) => 'y',
            // Decompositions for Latin Extended-A
            chr(196) . chr(128) => 'A',
            chr(196) . chr(129) => 'a',
            chr(196) . chr(130) => 'A',
            chr(196) . chr(131) => 'a',
            chr(196) . chr(132) => 'A',
            chr(196) . chr(133) => 'a',
            chr(196) . chr(134) => 'C',
            chr(196) . chr(135) => 'c',
            chr(196) . chr(136) => 'C',
            chr(196) . chr(137) => 'c',
            chr(196) . chr(138) => 'C',
            chr(196) . chr(139) => 'c',
            chr(196) . chr(140) => 'C',
            chr(196) . chr(141) => 'c',
            chr(196) . chr(142) => 'D',
            chr(196) . chr(143) => 'd',
            chr(196) . chr(144) => 'D',
            chr(196) . chr(145) => 'd',
            chr(196) . chr(146) => 'E',
            chr(196) . chr(147) => 'e',
            chr(196) . chr(148) => 'E',
            chr(196) . chr(149) => 'e',
            chr(196) . chr(150) => 'E',
            chr(196) . chr(151) => 'e',
            chr(196) . chr(152) => 'E',
            chr(196) . chr(153) => 'e',
            chr(196) . chr(154) => 'E',
            chr(196) . chr(155) => 'e',
            chr(196) . chr(156) => 'G',
            chr(196) . chr(157) => 'g',
            chr(196) . chr(158) => 'G',
            chr(196) . chr(159) => 'g',
            chr(196) . chr(160) => 'G',
            chr(196) . chr(161) => 'g',
            chr(196) . chr(162) => 'G',
            chr(196) . chr(163) => 'g',
            chr(196) . chr(164) => 'H',
            chr(196) . chr(165) => 'h',
            chr(196) . chr(166) => 'H',
            chr(196) . chr(167) => 'h',
            chr(196) . chr(168) => 'I',
            chr(196) . chr(169) => 'i',
            chr(196) . chr(170) => 'I',
            chr(196) . chr(171) => 'i',
            chr(196) . chr(172) => 'I',
            chr(196) . chr(173) => 'i',
            chr(196) . chr(174) => 'I',
            chr(196) . chr(175) => 'i',
            chr(196) . chr(176) => 'I',
            chr(196) . chr(177) => 'i',
            chr(196) . chr(178) => 'IJ',
            chr(196) . chr(179) => 'ij',
            chr(196) . chr(180) => 'J',
            chr(196) . chr(181) => 'j',
            chr(196) . chr(182) => 'K',
            chr(196) . chr(183) => 'k',
            chr(196) . chr(184) => 'k',
            chr(196) . chr(185) => 'L',
            chr(196) . chr(186) => 'l',
            chr(196) . chr(187) => 'L',
            chr(196) . chr(188) => 'l',
            chr(196) . chr(189) => 'L',
            chr(196) . chr(190) => 'l',
            chr(196) . chr(191) => 'L',
            chr(197) . chr(128) => 'l',
            chr(197) . chr(129) => 'L',
            chr(197) . chr(130) => 'l',
            chr(197) . chr(131) => 'N',
            chr(197) . chr(132) => 'n',
            chr(197) . chr(133) => 'N',
            chr(197) . chr(134) => 'n',
            chr(197) . chr(135) => 'N',
            chr(197) . chr(136) => 'n',
            chr(197) . chr(137) => 'N',
            chr(197) . chr(138) => 'n',
            chr(197) . chr(139) => 'N',
            chr(197) . chr(140) => 'O',
            chr(197) . chr(141) => 'o',
            chr(197) . chr(142) => 'O',
            chr(197) . chr(143) => 'o',
            chr(197) . chr(144) => 'O',
            chr(197) . chr(145) => 'o',
            chr(197) . chr(146) => 'OE',
            chr(197) . chr(147) => 'oe',
            chr(197) . chr(148) => 'R',
            chr(197) . chr(149) => 'r',
            chr(197) . chr(150) => 'R',
            chr(197) . chr(151) => 'r',
            chr(197) . chr(152) => 'R',
            chr(197) . chr(153) => 'r',
            chr(197) . chr(154) => 'S',
            chr(197) . chr(155) => 's',
            chr(197) . chr(156) => 'S',
            chr(197) . chr(157) => 's',
            chr(197) . chr(158) => 'S',
            chr(197) . chr(159) => 's',
            chr(197) . chr(160) => 'S',
            chr(197) . chr(161) => 's',
            chr(197) . chr(162) => 'T',
            chr(197) . chr(163) => 't',
            chr(197) . chr(164) => 'T',
            chr(197) . chr(165) => 't',
            chr(197) . chr(166) => 'T',
            chr(197) . chr(167) => 't',
            chr(197) . chr(168) => 'U',
            chr(197) . chr(169) => 'u',
            chr(197) . chr(170) => 'U',
            chr(197) . chr(171) => 'u',
            chr(197) . chr(172) => 'U',
            chr(197) . chr(173) => 'u',
            chr(197) . chr(174) => 'U',
            chr(197) . chr(175) => 'u',
            chr(197) . chr(176) => 'U',
            chr(197) . chr(177) => 'u',
            chr(197) . chr(178) => 'U',
            chr(197) . chr(179) => 'u',
            chr(197) . chr(180) => 'W',
            chr(197) . chr(181) => 'w',
            chr(197) . chr(182) => 'Y',
            chr(197) . chr(183) => 'y',
            chr(197) . chr(184) => 'Y',
            chr(197) . chr(185) => 'Z',
            chr(197) . chr(186) => 'z',
            chr(197) . chr(187) => 'Z',
            chr(197) . chr(188) => 'z',
            chr(197) . chr(189) => 'Z',
            chr(197) . chr(190) => 'z',
            chr(197) . chr(191) => 's'
        );

        return strtr($string, $chars);
    }

    /**
     * @param string $str
     * @param bool|null $multibyte
     * @return string
     */
    public static function toLower(string $str, ?bool $multibyte = null): string {
        if (is_null($multibyte) ? Str::getUsingMultiByte() : $multibyte) {
            return mb_strtolower($str);
        }
        return strtolower($str);
    }

    /**
     * @param string $string
     * @param string $old
     * @param string $new
     * @return array|string|string[]
     */
    public static function replace(string $string, string $old, string $new): string {
        return str_replace($old, $new, $string);
    }

    /**
     * @param $value
     * @return void
     */
    public static function setUsingMultiByte($value) {
        if (is_bool($value)) {
            Str::$multibyte = $value;
        }
    }

    /**
     * @param string $haystack
     * @param string $needle
     * @param bool|null $multibyte
     * @return bool
     */
    public static function startsWith(string $haystack, string $needle, ?bool $multibyte = null): bool {
        $length = Str::length($needle, $multibyte);
        return (substr($haystack, 0, $length) === $needle);
    }

    /**
     * @param string $string
     * @param int $offset
     * @param int|null $length
     * @return string
     */
    public static function substring(string $string, int $offset, ?int $length = null): string {
        return substr($string, $offset, $length);
    }

    /**
     * Convert a string to camelCase.
     *
     * @param string $string Input string.
     * @return string String in camelCase.
     */
    public static function toCamelCase(string $string): string {
        $words = str_word_count($string, 1);

        $result = '';
        foreach ($words as $index => $word) {
            $result .= ($index == 0 ? strtolower($word) : ucfirst(strtolower($word)));
        }

        return $result;
    }

    /**
     * Convert a string to PascalCase.
     *
     * @param string $string Input string.
     * @return string String in PascalCase.
     */
    public static function toPascalCase(string $string): string {
        $words = str_word_count($string, 1);

        $result = '';
        foreach ($words as $word) {
            $result .= ucfirst(strtolower($word));
        }

        return $result;
    }

    /**
     * Convert a string to kebab-case.
     *
     * @param string $string Input string.
     * @return string String in kebab-case.
     */
    public static function toKebabCase(string $string): string {
        // Insert hyphens before uppercase letters (CamelCase)
        $string = preg_replace('/(?<=\\w)(?=[A-Z])/', '-$1', $string);

        // Replace non-alphanumeric characters with a hyphen
        $string = preg_replace('/[^a-zA-Z0-9]/', '-', $string);

        // Replace multiple hyphens with a single hyphen
        $string = preg_replace('/-+/', '-', $string);

        // Convert all uppercase letters to lowercase
        $string = strtolower($string);

        // Remove leading or trailing hyphens
        return trim($string, '-');
    }

    /**
     * Convert a string to snake_case.
     *
     * @param string $string Input string.
     * @return string String in snake_case.
     */
    public static function toSnakeCase(string $string): string {
        $words = str_word_count($string, 1);

        $result = '';
        foreach ($words as $word) {
            $result .= (empty($result) ? '' : '_') . strtolower($word);
        }

        return $result;
    }

    /**
     * Remove a given prefix from a string if it exists.
     *
     * @param string $string Input string.
     * @param string $prefix Prefix to remove.
     * @return string String without the given prefix.
     */
    public static function removePrefix(string $string, string $prefix): string {
        if (strpos($string, $prefix) === 0) {
            return substr($string, strlen($prefix));
        }
        return $string;
    }

    /**
     * Remove a given suffix from a string if it exists.
     *
     * @param string $string Input string.
     * @param string $suffix Suffix to remove.
     * @return string String without the given suffix.
     */
    public static function removeSuffix(string $string, string $suffix): string {
        if (substr($string, -strlen($suffix)) === $suffix) {
            return substr($string, 0, -strlen($suffix));
        }
        return $string;
    }

    /**
     * @param string $string The original string.
     * @param string $substring The substring to be removed from the original string.
     * @return string The resulting string after the specified substring has been removed.
     */
    public static function remove(string $string, string $substring): string {
        return self::replace($string, $substring, "");
    }

    /**
     * Ensure a string has a certain prefix, adding it if it does not already exist.
     *
     * @param string $string Input string.
     * @param string $prefix Prefix to ensure.
     * @return string String with the given prefix.
     */
    public static function ensurePrefix(string $string, string $prefix): string {
        if (strpos($string, $prefix) !== 0) {
            return $prefix . $string;
        }
        return $string;
    }

    /**
     * Ensure a string has a certain suffix, adding it if it does not already exist.
     *
     * @param string $string Input string.
     * @param string $suffix Suffix to ensure.
     * @return string String with the given suffix.
     */
    public static function ensureSuffix(string $string, string $suffix): string {
        if (substr($string, -strlen($suffix)) !== $suffix) {
            return $string . $suffix;
        }
        return $string;
    }

    /**
     * @param string $str
     * @param bool|null $multibyte
     * @return string
     */
    public static function toUpper(string $str, ?bool $multibyte = null): string {
        if (is_null($multibyte) ? Str::getUsingMultiByte() : $multibyte) {
            return mb_strtoupper($str);
        }
        return strtoupper($str);
    }


    /**
     * @param string |null $string
     * @return string |null
     */
    public static function nullIfEmpty(?string $string): ?string {
        if ($string == "") {
            return null;
        }
        return $string;
    }

    /**
     * @param string|null $string $string
     * @return string
     */
    public static function emptyIfNull(?string $string): string {
        if (is_null($string)) {
            return "";
        }
        return $string;
    }

    /**
     * Removes emojis and other special control characters from a given string.
     *
     * @param string $string The input string potentially containing emojis or special characters.
     * @return string The string with emojis and special characters removed.
     */
    public static function removeEmojis(string $string): string {

        // Expresión regular mejorada para encontrar emojis y otros caracteres especiales
        $regex = '/\p{C}/u'; // Esto cubre todos los caracteres de control y de formato, incluidos los emojis

        // Eliminar emojis usando la expresión regular
        return preg_replace($regex, '', $string);

    }

    /**
     * @param mixed $string
     * @return bool
     */
    public static function isNullOrWhitespace($string): bool {
        if (is_string($string)) {
            $string = trim($string);
        }
        return Obj::isNullOrEmpty($string);
    }

    /**
     * @param int $count
     * @param string $one
     * @param string $more_than_one
     * @return string
     */
    public static function pluralize(int $count, string $one, string $more_than_one): string {
        if ($count > 1) {
            return $one;
        }
        return $more_than_one;
    }

    /**
     * @param mixed $value
     * @param string $thousands_separator
     * @return int|null
     */
    public static function parseInt($value, string $thousands_separator = ','): ?int {
        return Str::tryParseInt($value, $output, $thousands_separator) ? $output : null;
    }

    /**
     * @param mixed $value
     * @param int|null $output
     * @param string $thousands_separator
     * @return bool
     */
    public static function tryParseInt($value, ?int &$output, string $thousands_separator = ','): bool {

        if (is_integer($value)) {
            $output = $value;
            return true;
        }

        if (is_string($value)) {

            $value = str_replace($thousands_separator, "", $value);

            preg_match_all('/^\s*([+-]?\d+)\s*$/', $value, $output_array);
            if (count($output_array) > 1 && count($output_array[1]) > 0) {
                $output = intval($output_array[1][0]);
                return true;
            }
        }

        $output = null;
        return false;
    }

    /**
     * @param mixed $value
     * @param string $thousands_separator
     * @return float|null
     */
    public static function parseFlaot($value, string $thousands_separator = ','): ?float {
        return Str::tryParseFloat($value, $output, $thousands_separator) ? $output : null;
    }

    /**
     * @param mixed $value
     * @param float|null $output
     * @param string $decimal_separator
     * @param string $thousands_separator
     * @return bool
     */
    public static function tryParseFloat($value, ?float &$output, string $decimal_separator = '.', string $thousands_separator = ','): bool {

        if (is_float($value)) {
            $output = $value;
            return true;
        }

        if (is_string($value)) {

            $value = str_replace($thousands_separator, "", $value);
            $value = str_replace($decimal_separator, ".", $value);

            preg_match_all('/^\s*([+-]?\d+(\.\d+)?)\s*$/', $value, $output_array);
            if (count($output_array) > 1 && count($output_array[1]) > 0) {
                $output = floatval($output_array[1][0]);
                return true;
            }
        }

        $output = null;
        return false;
    }

}