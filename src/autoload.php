<?php


namespace Develia {


    /**
     * @return bool
     */
    function autoload(): bool {


        if (!defined(__NAMESPACE__ . "_AUTOLOADING")) {

            define(__NAMESPACE__ . "_AUTOLOADING", true);

            require_once "AutoLoader.php";
            $develia_autoloader = new AutoLoader(__DIR__, __NAMESPACE__);
            $develia_autoloader->register();
            return true;

        }
        return false;


    }
}

namespace {


    use Develia\From;
    use Develia\Run;

    if (!function_exists("scoped")) {

        /**
         * @template T
         * @template R
         * @param T $scoped_object
         * @param callable(T):R $callable
         * @return R
         * @throws ReflectionException
         */
        function scoped($scoped_object, $callable) {
            return Run::scoped($scoped_object, $callable);
        }

    }

    if (!function_exists("from")) {

        /**
         * @template TKey
         * @template TValue
         * @param array<TKey,TValue> | IteratorAggregate<TKey,TValue> $iterable
         * @return From<TKey,TValue>
         */
        function from($iterable): From {
            return new From($iterable);
        }
    }

    if (!spl_autoload_functions()) {
        Develia\autoload();
    }
}


