<?php

namespace Develia;

class Run
{


    private function __construct()
    {
    }

    /**
     * Ejecuta una función proporcionada con manejo de errores. En versiones de PHP inferiores a 7.0.0, los errores se capturan y se lanzan como excepciones ErrorException.
     *
     * @param callable $error_handler La función a ejecutar.
     *
     * @return mixed
     * @throws \ErrorException Se lanza si ocurre un error durante la ejecución de la función en versiones de PHP inferiores a 7.0.0.
     *
     */
    public static function handlingErrors(callable $error_handler)
    {

        if (version_compare(PHP_VERSION, '7.0.0', '<')) {
            set_error_handler(function ($errno, $errstr, $errfile, $errline) {
                throw new \ErrorException($errstr, $errno, 0, $errfile, $errline);
            });

            try {
                return call_user_func($error_handler);
            } finally {
                restore_error_handler();
            }
        } else {
            return call_user_func($error_handler);
        }
    }

    public static function buffered($fn, &$output)
    {
        try {
            ob_start();
            $ret = call_user_func($fn);
            $output = ob_get_clean();
            return $ret;

        } catch (\Exception $exception) {
            ob_end_clean();
            throw $exception;
        }
    }

    /**
     * @template T
     * @template R
     * @param T $scoped_object
     * @param callable(T):R $callable
     * @return R
     * @throws \ReflectionException
     */
    public static function scoped($scoped_object, callable $callable)
    {

        try {
            return call_user_func($callable, $scoped_object);
        } finally {
            if (is_resource($scoped_object)) {
                fclose($scoped_object);
            } else if (method_exists($scoped_object, "close")) {
                $scoped_object->close();
            } else if (method_exists($scoped_object, "dispose")) {
                $scoped_object->dispose();
            }

            unset($scoped_object);
            gc_collect_cycles();
            /*if (method_exists($scoped_object, "__destruct")) {
                $scoped_object->__destruct();
            }*/
        }

    }
}



