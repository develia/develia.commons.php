<?php


namespace Develia;

/**
 * @template T
 */
class Range {

    /**
     * @var T $start
     */
    public $start;

    /**
     * @var T $end
     */
    public $end;

    /**
     * Range constructor.
     * @param T $start
     * @param T $end
     */
    public function __construct($start, $end) {

        $this->start = $start;
        $this->end = $end;
    }


    /**
     * Finds the intersection between two ranges.
     *
     * @param Range<T> $other The Range object to intersect with.
     * @return Range<T>|null The intersected Range object, or null if there is no intersection.
     */
    public function intersect(Range $other): ?Range {
        $start = max($this->start, $other->start);
        $end = min($this->end, $other->end);

        if ($start < $end) {
            return new Range($start, $end);
        } else {
            return NULL;
        }
    }

    /**
     * Subtract the given range from this range.
     *
     * @param Range<T> $other The range to subtract from this range.
     * @return Range<T>[] An array of Range objects representing the result of the subtraction. If the result is empty, an empty array is returned.
     */
    public function subtract(Range $other): array {
        $result = [];

        if ($this->start >= $other->end || $this->end <= $other->start) {
            $result[] = new Range($this->start, $this->start);
        } elseif ($other->start <= $this->start && $other->end >= $this->end) {
            return $result;
        } else {
            if ($this->start < $other->start) {
                $result[] = new Range($this->start, $other->start);
            }

            if ($this->end > $other->end) {
                $result[] = new Range($other->end, $this->end);
            }
        }

        return $result;
    }

    /**
     * Determines if the given range overlaps with the current range.
     *
     * @param Range<T> $other The range to be checked for overlap.
     *
     * @return bool Returns true if there is an overlap between the ranges,
     *              false otherwise.
     */
    public function overlaps(Range $other): bool {


        return ($other->start > $this->start && $other->start < $this->end ||
                $other->end > $this->start && $other->end < $this->end) ||
               ($this->start > $other->start && $this->start < $other->end ||
                $this->end > $other->start && $this->end < $other->end) ||
               ($this->start == $other->start && $this->end == $other->end);


    }

    /**
     * Determines whether the specified value is within the range defined by the start and end values.
     *
     * @param mixed $obj The value to check if it is within the range.
     * @param bool $inclusive Optional. If set to true, the range boundaries are considered inclusive.
     *                        If set to false, the range boundaries are considered exclusive. Default is true.
     * @return bool Returns true if the specified value is within the range, false otherwise.
     */
    public function contains($obj, bool $inclusive = true): bool {
        return $inclusive ?
            $this->start <= $obj && $this->end >= $obj :
            $this->start < $obj && $this->end > $obj;
    }

}