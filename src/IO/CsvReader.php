<?php

namespace Develia\IO;

class CsvReader {

    /**
     * @var resource $stream
     */
    private $stream;

    /**
     * @param resource $stream
     */
    public function __construct($stream) {
        $this->stream = $stream;
    }

    public function read() {
        return fgetcsv($this->stream);
    }

    /**
     * @return resource
     */
    public function getStream() {
        return $this->stream;
    }


}