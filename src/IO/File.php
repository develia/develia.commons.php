<?php

namespace Develia\IO;


/**
 * Represents a file in the file system.
 */
interface File extends FileSystemNode
{

}