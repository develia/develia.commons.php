<?php

namespace Develia\IO;


/**
 * Class BinaryReader
 *
 * @package MyNamespace
 */
class BinaryReader {

    /**
     * @var resource $resource The stream to read from.
     */
    private $resource;

    /**
     * @var PackFormat
     */
    private PackFormat $format;

    /**
     * BinaryReader constructor.
     *
     * @param resource $resource The stream to read from.
     * @param string $endian The endianness to use for reading binary data.
     */
    public function __construct($resource, string $endian = EndianType::MACHINE) {
        $this->resource = $resource;
        $this->format = PackFormat::getFormat($endian);
    }

    /**
     * @return PackFormat
     */
    public function getFormat(): PackFormat {
        return $this->format;
    }

    /**
     * @param PackFormat $format
     */
    public function setFormat(PackFormat $format) {
        $this->format = $format;
    }

    /**
     * Reads a signed byte from the stream.
     *
     * @return int The signed byte read from the stream.
     */
    public function readSByte(): int {
        $data = $this->read(1);
        $result = unpack(PackFormat::SIGNED_CHAR, $data);
        return $result[1];
    }

    /**
     * Reads a specified number of bytes from the stream.
     *
     * @param int $length The number of bytes to read from the stream.
     * @return string The bytes read from the stream.
     */
    public function read(int $length = 1): string {
        return fread($this->resource, $length);
    }

    /**
     * Reads an unsigned byte from the stream.
     *
     * @return int The unsigned byte read from the stream.
     */
    public function readByte(): int {
        $data = $this->read(1);
        $result = unpack(PackFormat::UNSIGNED_CHAR, $data);
        return $result[1];
    }

    /**
     * Reads a signed 16-bit integer from the stream.
     *
     * @return int The signed 16-bit integer read from the stream.
     */
    public function readInt16(): int {
        $data = $this->read(2);
        $result = unpack(PackFormat::SIGNED_SHORT, $data);
        return $result[1];
    }

    /**
     * Reads a signed 32-bit integer from the stream.
     *
     * @return int The signed 32-bit integer read from the stream.
     */
    public function readInt32(): int {
        $data = $this->read(4);
        $result = unpack(PackFormat::SIGNED_LONG, $data);
        return $result[1];
    }

    /**
     * Reads a signed 64-bit integer from the stream.
     *
     * @return int The signed 64-bit integer read from the stream.
     */
    public function readInt64(): int {
        $data = $this->read(8);
        $result = unpack(PackFormat::SIGNED_LONG_LONG, $data);
        return $result[1];
    }

    /**
     * @return resource
     */
    public function getStream() {
        return $this->resource;
    }

    /**
     * Reads an unsigned 16-bit integer from the stream.
     *
     * @return int The unsigned 16-bit integer read from the stream.
     */
    public function readUInt16(): int {
        $data = $this->read(2);
        $result = unpack($this->format->getUnsignedShortFormat(), $data);
        return $result[1];
    }

    /**
     * Reads an unsigned 32-bit integer from the stream.
     *
     * @return int The unsigned 32-bit integer read from the stream.
     */
    public function readUInt32(): int {
        $data = $this->read(4);
        $result = unpack($this->format->getUnsignedLongFormat(), $data);
        return $result[1];
    }

    /**
     * Reads an unsigned 64-bit integer from the stream.
     *
     * @return int The unsigned 64-bit integer read from the stream.
     */
    public function readUInt64(): int {
        $data = $this->read(8);
        $result = unpack($this->format->getUnsignedLongLongFormat(), $data);
        return $result[1];
    }

    /**
     * Reads a signed integer of the appropriate size from the stream.
     *
     * @return int The signed integer read from the stream.
     */
    public function readInt(): int {
        $data = $this->read(PHP_INT_SIZE);
        $result = unpack(PackFormat::SIGNED_LONG, $data);
        return $result[1];
    }

}
