<?php

namespace Develia\IO;

class TextReader {

    /**
     * @var resource
     */
    private $resource;

    /**
     * @param resource $resource
     */
    public function __construct($resource) {
        $this->resource = $resource;
    }

    public function read($length = 1) {
        return fread($this->resource, $length);
    }

    public function readLine() {
        return fgets($this->resource);
    }


    public function readLines(): array {
        $output = [];
        while (($line = fgets($this->resource)) !== false) {
            $output[] = $line;
        }
        return $output;
    }

    public function readAll() {
        return stream_get_contents($this->resource);
    }

    /**
     * @return resource
     */
    public function getStream() {
        return $this->resource;
    }


}