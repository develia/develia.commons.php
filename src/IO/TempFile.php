<?php

namespace Develia\IO;

class TempFile extends LocalFile {

    public function __construct($path = null) {
        if (!$path)
            $path = tempnam(sys_get_temp_dir(), "tmp");

        parent::__construct($path);
    }

    public static function create($directory = null, $extension = null): TempFile {
        $directory = $directory ?: sys_get_temp_dir();

        $path = tempnam($directory, "tmp");

        if ($extension)
            $path .= "." . $extension;

        return new TempFile($path);
    }

    public function __destruct() {
        if ($this->exists())
            $this->delete();
    }

}