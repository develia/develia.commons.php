<?php

namespace Develia\IO;

interface FileSystemNode {

    /**
     * @return bool
     */
    public function delete(): bool;

    /**
     * Copies a file or directory to a new location.
     *
     * @param string $destination The path to the destination.
     */
    public function copy(string $destination);

    /**
     * Renames a file or directory to a new location.
     *
     * @param string $destination The path to the destination.
     */
    public function rename(string $destination);

    /**
     * Retrieves the path of the given file or directory.
     *
     * @return string The absolute path of the file or directory, or false if it does not exist.
     */
    public function getPath(): string;

    public function exists();

    /**
     * Gets the size of a file or directory.
     *
     * @return int The size of the file or the total size of the directory in bytes.
     */
    public function getSize(): int;

    /**
     * Gets the last access time of a file or directory.
     *
     * @return \DateTime The last access time.
     */
    public function getLastAccessTime(): \DateTime;

    /**
     * Gets the last modification time of a file or directory.
     *
     * @return \DateTime The last modification time.
     */
    public function getLastModificationTime(): \DateTime;


}