<?php

namespace Develia\IO;


/**
 * Represents a file in the file system.
 */
class LocalFile extends LocalFileSystemNode implements File {


    /**
     * Returns the size of the file associated with the current object.
     *
     * @return int The size of the file in bytes.
     */
    public function getSize(): int {
        return filesize($this->getPath());
    }


    /**
     * Deletes the file if it exists.
     *
     * @return bool
     */
    public function delete(): bool {
        return unlink($this->getPath());
    }

    /**
     * Checks if the file exists.
     *
     * @return bool Returns true if the file exists, false otherwise.
     */
    public function exists(): bool {
        return is_file($this->getPath());
    }

    /**
     * Opens a file with the specified mode.
     *
     * @param string $mode The mode to open the file in. Examples are 'r' for reading, 'w' for writing, and 'a' for appending.
     *                     For a complete list of modes, refer to the PHP fopen() function documentation.
     *
     * @return resource|false Returns a file pointer resource on success, or false on failure.
     *                        Note: The exact type of resource returned depends on the mode parameter passed to the function.
     *                        This return value can be passed to other file manipulation functions such as fread() and fwrite() for further processing.
     *                        Returns false if the file cannot be opened.
     */
    public function open(string $mode) {
        return fopen($this->getPath(), $mode);
    }

    /**
     * Copies the file to the specified destination.
     *
     * @param string $destination The path of the destination file including the file name.
     * @return LocalFile A new instance representing the copied file.
     */
    public function copy(string $destination): LocalFile {
        copy($this->getPath(), $destination);
        return new LocalFile($destination);
    }

}