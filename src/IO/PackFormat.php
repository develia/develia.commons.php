<?php

namespace Develia\IO;

/**
 *
 */
class PackFormat {

    /** @var string NUL-padded string */
    const NUL_PADDED_STRING = 'a';
    /** @var string SPACE-padded string */
    const SPACE_PADDED_STRING = 'A';
    /** @var string Hex string, low nibble first */
    const HEX_STRING_LOW_NIBBLE_FIRST = 'h';
    /** @var string Hex string, high nibble first */
    const HEX_STRING_HIGH_NIBBLE_FIRST = 'H';
    /** @var string signed char */
    const SIGNED_CHAR = 'c';
    /** @var string unsigned char */
    const UNSIGNED_CHAR = 'C';
    /** @var string signed short (always 16 bit, machine byte order) */
    const SIGNED_SHORT = 's';
    /** @var string unsigned short (always 16 bit, machine byte order) */
    const UNSIGNED_SHORT = 'S';
    /** @var string unsigned short (always 16 bit, big endian byte order) */
    const UNSIGNED_SHORT_BIG_ENDIAN = 'n';
    /** @var string unsigned short (always 16 bit, little endian byte order) */
    const UNSIGNED_SHORT_LITTLE_ENDIAN = 'v';
    /** @var string signed integer (machine dependent size and byte order) */
    const SIGNED_INT = 'i';
    /** @var string unsigned integer (machine dependent size and byte order) */
    const UNSIGNED_INT = 'I';
    /** @var string signed long (always 32 bit, machine byte order) */
    const SIGNED_LONG = 'l';
    /** @var string unsigned long (always 32 bit, machine byte order) */
    const UNSIGNED_LONG = 'L';
    /** @var string unsigned long (always 32 bit, big endian byte order) */
    const UNSIGNED_LONG_BIG_ENDIAN = 'N';
    /** @var string unsigned long (always 32 bit, little endian byte order) */
    const UNSIGNED_LONG_LITTLE_ENDIAN = 'V';
    /** @var string signed long long (always 64 bit, machine byte order) */
    const SIGNED_LONG_LONG = 'q';
    /** @var string unsigned long long (always 64 bit, machine byte order) */
    const UNSIGNED_LONG_LONG = 'Q';
    /** @var string unsigned long long (always 64 bit, big endian byte order) */
    const UNSIGNED_LONG_LONG_BIG_ENDIAN = 'J';
    /** @var string unsigned long long (always 64 bit, little endian byte order) */
    const UNSIGNED_LONG_LONG_LITTLE_ENDIAN = 'P';
    /** @var string float (machine dependent size and representation) */
    const FLOAT = 'f';
    /** @var string float (machine dependent size, little endian byte order) */
    const FLOAT_LITTLE_ENDIAN = 'g';
    /** @var string float (machine dependent size, big endian byte order) */
    const FLOAT_BIG_ENDIAN = 'G';
    /** @var string double (machine dependent size and representation) */
    const DOUBLE = 'd';
    /** @var string double (machine dependent size, little endian byte order) */
    const DOUBLE_LITTLE_ENDIAN = 'e';
    /** @var string double (machine dependent size, big endian byte order) */
    const DOUBLE_BIG_ENDIAN = 'E';
    /** @var string NUL byte */
    const NUL_BYTE = 'x';
    /** @var string Back up one byte */
    const BACK_UP_ONE_BYTE = 'X';
    /** @var string NUL-padded string */
    const NUL_PADDED_STRING_Z = 'Z';
    /** @var string NUL-fill to absolute position */
    const NUL_FILL_TO_ABSOLUTE_POSITION = '@';

    private string $unsignedShortFormat;

    private string $unsignedLongFormat;

    private string $unsignedLongLongFormat;

    private string $floatFormat;

    private string $doubleFormat;

    /**
     * @param string $endian
     * @return void
     */
    private function __construct(string $endian) {
        if ($endian === EndianType::BIG) {
            $this->unsignedShortFormat = PackFormat::UNSIGNED_SHORT_BIG_ENDIAN;
            $this->unsignedLongFormat = PackFormat::UNSIGNED_LONG_BIG_ENDIAN;
            $this->unsignedLongLongFormat = PackFormat::UNSIGNED_LONG_LONG_BIG_ENDIAN;
            $this->floatFormat = PackFormat::FLOAT_BIG_ENDIAN;
            $this->doubleFormat = PackFormat::DOUBLE_BIG_ENDIAN;
        } elseif ($endian === EndianType::LITTLE) {
            $this->unsignedShortFormat = PackFormat::UNSIGNED_SHORT_LITTLE_ENDIAN;
            $this->unsignedLongFormat = PackFormat::UNSIGNED_LONG_LITTLE_ENDIAN;
            $this->unsignedLongLongFormat = PackFormat::UNSIGNED_LONG_LONG_LITTLE_ENDIAN;
            $this->floatFormat = PackFormat::FLOAT_LITTLE_ENDIAN;
            $this->doubleFormat = PackFormat::DOUBLE_LITTLE_ENDIAN;
        } else {
            $this->unsignedShortFormat = PackFormat::UNSIGNED_SHORT;
            $this->unsignedLongFormat = PackFormat::UNSIGNED_LONG;
            $this->unsignedLongLongFormat = PackFormat::UNSIGNED_LONG_LONG;
            $this->floatFormat = PackFormat::FLOAT;
            $this->doubleFormat = PackFormat::DOUBLE;
        }
    }

    public static function getFormat($endian = EndianType::MACHINE): PackFormat {
        return new self($endian);
    }

    public function getUnsignedShortFormat(): string {
        return $this->unsignedShortFormat;
    }

    public function getUnsignedLongFormat(): string {
        return $this->unsignedLongFormat;
    }

    public function getUnsignedLongLongFormat(): string {
        return $this->unsignedLongLongFormat;
    }

    public function getFloatFormat(): string {
        return $this->floatFormat;
    }

    public function getDoubleFormat(): string {
        return $this->doubleFormat;
    }


}