<?php

namespace Develia\IO;

class TextWriter
{

    /**
     * @var resource
     */
    private $stream;

    /**
     * @param resource $resource
     */
    public function __construct($resource)
    {
        $this->stream = $resource;
    }

    public function writeLines($strings)
    {
        foreach ($strings as $string)
            $this->writeLine($string);
    }

    public function writeLine($string)
    {
        return fputs($this->stream, $string . PHP_EOL);
    }

    public function write($string)
    {
        return fputs($this->stream, $string);
    }


    /**
     * @return Stream|resource
     */
    public function getStream()
    {
        return $this->stream;
    }


}