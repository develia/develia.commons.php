<?php

namespace Develia\IO;


/**
 *
 */
class FileStream extends Stream {


    const STDIN = 'php://stdin';
    const STDOUT = 'php://stdout';
    const STDERR = 'php://stderr';
    const OUTPUT = 'php://output';
    const INPUT = 'php://input';
    const MEMORY = 'php://memory';
    const TEMP = 'php://temp';

    /**
     * @var string
     */
    private string $file;

    private string $mode;

    /**
     * Constructor.
     *
     * @param string $filePath Path to the file.
     * @throws \Exception if the file cannot be opened.
     */
    public function __construct($filePath, $mode, $autoclose = true) {
        $handle = fopen($filePath, $mode);
        if (!$handle) {
            throw new \Exception("Cannot open file: $filePath");
        }

        parent::__construct($handle, $autoclose);

        $this->file = $filePath;
        $this->mode = $mode;
    }

    /**
     * @return string
     */
    public function getFile(): string {
        return $this->file;
    }


    /**
     * @return string
     */
    public function getMode(): string {
        return $this->mode;
    }

    function isBinary(): bool {
        return strpos($this->mode, 'b') !== false;
    }

    function isWritable(): bool {
        $writableModes = ['w', 'w+', 'a', 'a+', 'x', 'x+', 'c', 'c+', 'rw', 'rw+'];

        $modeParts = explode('+', $this->mode);
        $baseMode = $modeParts[0];

        if (in_array($baseMode, $writableModes)) {
            return true;
        }

        foreach ($writableModes as $writableMode) {
            if (strpos($this->mode, $writableMode) !== false) {
                return true;
            }
        }

        return false;
    }

    function isReadable(): bool {
        $readableModes = ['r', 'r+', 'w+', 'a+', 'x+', 'c+', 'rw', 'rw+'];

        $modeParts = explode('+', $this->mode);
        $baseMode = $modeParts[0];

        if (in_array($baseMode, $readableModes)) {
            return true;
        }

        foreach ($readableModes as $readableMode) {
            if (strpos($this->mode, $readableMode) !== false) {
                return true;
            }
        }

        return false;
    }


}