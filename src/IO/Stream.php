<?php

namespace Develia\IO;

class Stream {


    /**
     * @var resource|null The stream resource handle.
     */
    private $handle;

    /**
     * @var bool Determines whether the stream should be closed automatically.
     */
    private bool $autoclose;

    /**
     * @var bool Indicates if the stream is closed.
     */
    private bool $is_closed;

    /**
     * Stream constructor.
     *
     * @param resource $resource The stream resource handle.
     * @param bool $autoclose Determines whether the stream should be closed automatically.
     */
    public function __construct($resource, bool $autoclose = true) {
        if (!is_resource($resource))
            throw new \InvalidArgumentException('$resource must be a resource');

        $this->handle = $resource;
        $this->autoclose = $autoclose;
        $this->is_closed = false;
    }

    /**
     * Checks if the stream is closed.
     *
     * @return bool Returns true if the stream is closed, false otherwise.
     */
    public function isClosed(): bool {
        return $this->is_closed;
    }

    /**
     * Stream destructor.
     *
     * Automatically closes the stream if autoclose is enabled.
     */
    public function __destruct() {
        if ($this->autoclose) {
            $this->close();
        }
    }

    /**
     * Closes the stream.
     *
     * @return void
     */
    public function close() {
        if (!$this->is_closed) {
            fclose($this->handle);
            $this->is_closed = true;
        }
    }

    /**
     * Checks if the end of the stream has been reached.
     *
     * @return bool Returns true if the end of the stream has been reached, false otherwise.
     */
    public function eof(): bool {
        return feof($this->handle);
    }

    /**
     * Returns the current position within the stream.
     *
     * @return int|false Returns the position within the stream, or false on failure.
     */
    public function getPosition() {
        return ftell($this->handle);
    }

    /**
     * Sets the position within the stream.
     *
     * @param int $position The new position within the stream.
     * @return int|bool Returns 0 on success, or -1 on failure.
     */
    public function setPosition(int $position) {
        return fseek($this->handle, $position, SEEK_SET);
    }

    /**
     * Reads data from the stream.
     *
     * @param int $length The number of bytes to read.
     * @return string|false Returns the data read from the stream, or false on failure.
     */
    public function read(int $length = 1) {
        return fread($this->handle, $length);
    }

    /**
     * Reads the remaining contents of the stream.
     *
     * @param int|null $length The maximum number of bytes to read. If null, reads until the end of the stream.
     * @param int $offset The offset where reading starts from.
     * @return string|false Returns the data read from the stream, or false on failure.
     */
    public function readAll(?int $length = null, int $offset = -1) {
        return stream_get_contents($this->handle, $length, $offset);
    }

    /**
     * Writes data to the stream.
     *
     * @param string $data The data to write to the stream.
     * @param int|null $length The number of bytes to write. If null, writes the entire string.
     * @return int|false Returns the number of bytes written, or false on failure.
     */
    public function write(string $data, ?int $length = null) {
        return fwrite($this->handle, $data, $length);
    }

    /**
     * Returns the stream resource handle.
     *
     * @return resource|null Returns the stream resource handle, or null if the stream is closed.
     */
    public function getHandle() {
        return $this->handle;
    }


}
