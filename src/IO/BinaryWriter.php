<?php

namespace Develia\IO;


/**
 * BinaryWriter class for writing binary data to a stream.
 */
class BinaryWriter {

    /**
     * @var resource A stream or resource where data will be written.
     */
    private $resource;

    /**
     * @var PackFormat
     */
    private PackFormat $format;


    /**
     * BinaryWriter constructor.
     *
     * @param mixed $resource A stream or resource where data will be written.
     * @param string $endian The endianness of the data to be written. Default is EndianType::MACHINE
     */
    public function __construct($resource, string $endian = EndianType::MACHINE) {
        $this->resource = $resource;
        $this->format = PackFormat::getFormat($endian);
    }

    /**
     * @return PackFormat
     */
    public function getFormat(): PackFormat {
        return $this->format;
    }

    /**
     * @param PackFormat $format
     */
    public function setFormat(PackFormat $format) {
        $this->format = $format;
    }

    /**
     * Writes a byte to the stream.
     *
     * @param int $value The byte to write.
     * @return int The number of bytes written.
     */
    public function writeByte(int $value): int {
        $data = pack(PackFormat::UNSIGNED_CHAR, $value);
        return $this->write($data);
    }

    /**
     * Writes data to the stream.
     *
     * @param string $data The data to write.
     * @param int|null $length The number of bytes to write. If null, all bytes are written.
     * @return int The number of bytes written.
     */
    public function write(string $data, ?int $length = null): int {
        return fwrite($this->resource, $data, $length);
    }

    /**
     * Writes a signed byte to the stream.
     *
     * @param int $value The signed byte to write.
     * @return int The number of bytes written.
     */
    public function writeSByte(int $value): int {
        $data = pack(PackFormat::SIGNED_CHAR, $value);
        return $this->write($data);
    }

    /**
     * Writes a 16-bit signed integer to the stream.
     *
     * @param int $value The 16-bit signed integer to write.
     * @return int The number of bytes written.
     */
    public function writeInt16(int $value): int {
        $data = pack(PackFormat::SIGNED_SHORT, $value);
        return $this->write($data);
    }

    /**
     * Writes a 32-bit signed integer to the stream.
     *
     * @param int $value The 32-bit signed integer to write.
     * @return int The number of bytes written.
     */
    public function writeInt32(int $value): int {
        $data = pack(PackFormat::SIGNED_LONG, $value);
        return $this->write($data);
    }

    /**
     * Writes a 64-bit signed integer to the stream.
     *
     * @param int $value The 64-bit signed integer to write.
     * @return int The number of bytes written.
     */
    public function writeInt64(int $value): int {
        $data = pack(PackFormat::SIGNED_LONG_LONG, $value);
        return $this->write($data);
    }

    /**
     * Writes a 16-bit unsigned integer to the stream.
     *
     * @param int $value The 16-bit unsigned integer to write.
     * @return int The number of bytes written.
     */
    public function writeUInt16(int $value): int {
        $data = pack($this->format->getUnsignedShortFormat(), $value);
        return $this->write($data);
    }

    /**
     * Writes a 32-bit unsigned integer to the stream.
     *
     * @param int $value The 32-bit unsigned integer to write.
     * @return int The number of bytes written.
     */
    public function writeUInt32(int $value): int {
        $data = pack($this->format->getUnsignedLongFormat(), $value);
        return $this->write($data);
    }

    /**
     * Writes a 64-bit unsigned integer to the stream.
     *
     * @param int $value The 64-bit unsigned integer to write.
     * @return int The number of bytes written.
     */
    public function writeUInt64(int $value): int {
        $data = pack($this->format->getUnsignedLongLongFormat(), $value);
        return $this->write($data);
    }

    /**
     * Writes a signed integer to the stream.
     *
     * @param int $value The signed integer to write.
     * @return int The number of bytes written.
     */
    public function writeInt(int $value): int {
        $data = pack(PackFormat::SIGNED_LONG, $value);
        return $this->write($data);
    }

    /**
     * Writes an unsigned integer to the stream.
     *
     * @param int $value The unsigned integer to write.
     * @return int The number of bytes written.
     */
    public function writeUInt(int $value): int {
        $data = pack($this->format->getUnsignedLongLongFormat(), $value);
        return $this->write($data);
    }

    /**
     * Writes a floating-point number to the stream.
     *
     * @param float $value The floating-point number to write.
     * @return int The number of bytes written.
     */
    public function writeFloat(float $value): int {
        $data = pack($this->format->getFloatFormat(), $value);
        return $this->write($data);
    }

    /**
     * Writes a double-precision floating-point number to the stream.
     *
     * @param double $value The double-precision floating-point number to write.
     * @return int The number of bytes written.
     */
    public function writeDouble(float $value): int {
        $data = pack($this->format->getDoubleFormat(), $value);
        return $this->write($data);
    }

    /**
     * @return resource
     */
    public function getStream() {
        return $this->resource;
    }

}
