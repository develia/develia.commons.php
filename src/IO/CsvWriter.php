<?php

namespace Develia\IO;

class CsvWriter {

    /**
     * @var resource
     */
    private $resource;

    /**
     * @param resource $resource
     */
    public function __construct($resource) {
        $this->resource = $resource;
    }

    /**
     * Writes a row to the CSV file.
     *
     * @param array $row An array of values.
     */
    public function write(array $row) {
        fputcsv($this->resource, $row);
    }

    /**
     * @return resource
     */
    public function getResource() {
        return $this->resource;
    }


}