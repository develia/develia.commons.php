<?php

namespace Develia\IO;

use Develia\Path;

abstract class LocalFileSystemNode implements FileSystemNode {

    private string $path;

    public function __construct($path) {
        $this->path = Path::absolute($path);
    }

    public static function fromPath($path) {
        if (is_dir($path))
            return new LocalDirectory($path);
        if (is_file($path))
            return new LocalFile($path);

        return null;

    }

    /**
     * @return bool
     */
    public abstract function delete(): bool;

    public function __toString() {
        return $this->path;
    }

    /**
     * Copies a file or directory to a new location.
     *
     * @param string $destination The path to the destination.
     */
    public abstract function copy(string $destination);

    /**
     * Renames a file or directory to a new location.
     *
     * @param string $destination The path to the destination.
     */
    public function rename(string $destination) {
        $tmp = realpath($destination);
        rename($this->getPath(), $tmp);
        $this->path = $tmp;
    }

    /**
     * Retrieves the path of the given file or directory.
     *
     * @return string The absolute path of the file or directory, or false if it does not exist.
     */
    public function getPath(): string {
        return $this->path;
    }

    public abstract function exists();

    /**
     * Gets the size of a file or directory.
     *
     * @return int The size of the file or the total size of the directory in bytes.
     */
    public abstract function getSize(): int;


    /**
     * Gets the last access time of a file or directory.
     *
     * @return \DateTime The last access time.
     */
    public function getLastAccessTime(): \DateTime {
        return (new \DateTime())->setTimestamp(fileatime($this->path));
    }

    /**
     * Gets the last modification time of a file or directory.
     *
     * @return \DateTime The last modification time.
     */
    public function getLastModificationTime(): \DateTime {
        return (new \DateTime())->setTimestamp(filemtime($this->path));
    }

    /**
     * Retrieves the directory of the current file system entry.
     *
     * @return LocalDirectory The directory path of the current file.
     */
    public function getParent(): LocalDirectory {
        return new LocalDirectory(dirname($this->path));
    }


    public function getName(): string {
        return basename($this->path);
    }

}