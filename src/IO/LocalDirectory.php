<?php


namespace Develia\IO;


use Develia\Path;

/**
 * Class Directory
 *
 * Represents a directory in the file system.
 */
class LocalDirectory extends LocalFileSystemNode implements Directory {

    /**
     * Deletes the directory and its contents recursively if specified.
     *
     * @return bool
     */
    public function delete(): bool {


        $entries = $this->getFiles();
        foreach ($entries as $file) {
            $file->delete();
        }

        $entries = $this->getDirectories();
        foreach ($entries as $dir) {
            $dir->delete();
        }


        return rmdir($this->getPath());
    }

    /**
     * Retrieve an array of File objects representing the files within the current directory
     *
     * @return array An array of File objects
     */
    public function getFiles(): array {
        $entries = array_diff(scandir($this->getPath()), array('.', '..'));

        $entries = array_map(function ($x) {
            return Path::combine($this->getPath(), $x);
        }, $entries);

        $entries = array_filter($entries, function ($x) {
            return is_file($x);
        });

        return array_values(array_map(function ($x) {
            return new LocalFile($x);
        }, $entries));
    }

    /**
     * Retrieves an array of Directory objects representing the directories within the current directory.
     *
     * @return LocalDirectory[] An array of Directory objects representing the directories within the current directory.
     */
    public function getDirectories(): array {
        $entries = array_diff(scandir($this->getPath()), array('.', '..'));

        $entries = array_map(function ($x) {
            return Path::combine($this->getPath(), $x);
        }, $entries);

        $entries = array_filter($entries, function ($x) {
            return is_dir($x);
        });

        return array_values(array_map(function ($x) {
            return new LocalDirectory($x);
        }, $entries));
    }

    /**
     * Get the total size of all files and directories within the current directory.
     *
     * @return int The total size in bytes.
     */
    public function getSize(): int {
        $output = 0;
        foreach ($this->getEntries() as $entry) {
            $output += $entry->getSize();
        }
        return $output;
    }

    /**
     * Retrieves the entries (directories and files) within the given directory.
     *
     * This method returns an array that contains the directories and files within the directory. The array
     * is obtained by merging the arrays of directories and files returned by the respective methods.
     *
     * @return LocalFileSystemNode[] An array containing the directories and files within the directory.
     */
    public function getEntries(): array {
        return array_values(array_map(function ($x) {
            return LocalFileSystemNode::fromPath($x);
        }, array_diff(scandir($this->getPath()), array('.', '..'))));
    }

    /**
     * Copies the current directory to the specified destination folder.
     *
     * @param string $destination The destination folder where the directory should be copied to.
     * @return LocalDirectory The newly created directory object representing the destination folder.
     */
    public function copy(string $destination): LocalDirectory {
        $destination = new LocalDirectory($destination);
        $destination->create();

        foreach ($this->getEntries() as $entry) {
            $entry->copy(Path::combine($destination->getPath(), $entry->getName()));
        }

        return $destination;
    }

    /**
     * Creates a new directory if it does not already exist.
     *
     * This method checks if the directory exists and creates a new directory
     * using the directory path specified by `getPath()` method if it does not exist.
     *
     * @return void
     */
    public function create() {
        if (!$this->exists())
            mkdir($this->getPath());
    }

    /**
     * Checks if the directory exists.
     *
     * @return bool Returns true if the directory exists, false otherwise.
     */
    public function exists(): bool {
        return is_dir($this->getPath());
    }

    /**
     * Ensures that the directory exists.
     *
     * If the directory does not exist, this method will create it, along with any parent directories as needed.
     *
     * @return void
     */
    public function ensure() {
        if (!$this->exists()) {
            $parent_dir = $this->getParent();
            if (!$parent_dir->exists()) {
                $parent_dir->ensure();
            }
            mkdir($this->getPath());
        }
    }

}