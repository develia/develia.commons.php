<?php

namespace Develia\IO;


/**
 * Class Directory
 *
 * Represents a directory in the file system.
 */
interface Directory extends FileSystemNode
{
   
}