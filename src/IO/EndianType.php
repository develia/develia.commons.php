<?php

namespace Develia\IO;

/**
 *
 */
final class EndianType
{
    const LITTLE = 'little';
    const BIG = 'big';
    const MACHINE = 'machine';

    private function __construct()
    {
        // Constructor privado para evitar la creación de instancias
    }

}