<?php

namespace Develia\IO;

class TempDirectory extends LocalDirectory {

    public function __construct($path = null) {
        if (!$path)
            $path = tempnam(sys_get_temp_dir(), "tmp");

        parent::__construct($path);
    }

    public function __destruct() {
        if ($this->exists())
            $this->delete();
    }

}