<?php

namespace Develia;

class TimeSpan {

    /**
     * Microseconds in this TimeSpan
     *
     * @var float
     */
    private $microseconds;

    /**
     * TimeSpan constructor.
     *
     * @param float $microseconds Number of microseconds
     */
    public function __construct(float $microseconds) {
        $this->microseconds = abs($microseconds);
    }

    /**
     * Create a new TimeSpan representing a number of days.
     *
     * @param float $days Number of days
     * @return TimeSpan New TimeSpan representing the given number of days
     */
    public static function fromDays(float $days): TimeSpan {
        $microseconds = $days * 24 * 60 * 60 * 1000000;
        return new TimeSpan($microseconds);
    }

    /**
     * Create a new TimeSpan representing a number of hours.
     *
     * @param float $hours Number of hours
     * @return TimeSpan New TimeSpan representing the given number of hours
     */
    public static function fromHours(float $hours): TimeSpan {
        $microseconds = $hours * 60 * 60 * 1000000;
        return new TimeSpan($microseconds);
    }

    /**
     * Create a new TimeSpan representing a number of minutes.
     *
     * @param float $minutes Number of minutes
     * @return TimeSpan New TimeSpan representing the given number of minutes
     */
    public static function fromMinutes(float $minutes): TimeSpan {
        $microseconds = $minutes * 60 * 1000000;
        return new TimeSpan($microseconds);
    }

    /**
     * Create a new TimeSpan representing a number of minutes.
     *
     * @param float $seconds Number of seconds
     * @return TimeSpan New TimeSpan representing the given number of seconds
     */
    public static function fromSeconds(float $seconds): TimeSpan {
        $microseconds = $seconds * 1000000;
        return new TimeSpan($microseconds);
    }


    /**
     * Create a new TimeSpan representing the difference between two DateTime objects.
     *
     * @param \DateTime $start The start date and time
     * @param \DateTime $end The end date and time
     * @return TimeSpan New TimeSpan representing the difference
     */
    public static function fromDateDifference(\DateTimeInterface $start, \DateTimeInterface $end): TimeSpan {
        $interval = $start->diff($end);
        return self::fromDateInterval($interval);
    }

    public static function fromDateInterval(\DateInterval $interval): TimeSpan {
        $days = $interval->d;
        $hours = $interval->h;
        $minutes = $interval->i;
        $seconds = $interval->s;

        if (version_compare(PHP_VERSION, '7.1.0', '>=')) {
            $microseconds = $interval->f * 1000000;
        } else {
            $microseconds = 0;
        }


        $totalMicroseconds = (($days * 24 * 60 * 60) + ($hours * 60 * 60) + ($minutes * 60) + $seconds) * 1000000 + $microseconds;

        return new TimeSpan($totalMicroseconds);
    }

    /**
     * Create a new TimeSpan representing a number of milliseconds.
     *
     * @param float $milliseconds Number of milliseconds
     * @return TimeSpan New TimeSpan representing the given number of milliseconds
     */
    public static function fromMilliseconds(float $milliseconds): TimeSpan {
        $microseconds = $milliseconds * 1000;
        return new TimeSpan($microseconds);
    }

    /**
     * Create a new TimeSpan representing a number of microseconds.
     *
     * @param float $microseconds Number of microseconds
     * @return TimeSpan New TimeSpan representing the given number of microseconds
     */
    public static function fromMicroseconds(float $microseconds): TimeSpan {
        return new TimeSpan($microseconds);
    }

    public function format($format) {
        $formatLower = strtolower($format);
        $hasHours = str_contains($formatLower, "h");
        $hasMinutes = str_contains($formatLower, "m");

        $hours = 0;
        $minutes = 0;
        $seconds = floor($this->getSeconds());

        if ($hasHours) {
            $hours = floor($seconds / 3600);
            $seconds -= $hours * 3600;
        }
        if ($hasMinutes) {
            $minutes = floor($seconds / 60);
            $seconds -= $minutes * 60;
        }

        $hoursPadded = str_pad($hours, 2, '0', STR_PAD_LEFT);
        $minutesPadded = str_pad($minutes, 2, '0', STR_PAD_LEFT);
        $secondsPadded = str_pad($seconds, 2, '0', STR_PAD_LEFT);

        $formatLower = str_replace('hh', $hoursPadded, $formatLower);
        $formatLower = str_replace('h', strval($hours), $formatLower);
        $formatLower = str_replace('mm', $minutesPadded, $formatLower);
        $formatLower = str_replace('m', strval($minutes), $formatLower);
        $formatLower = str_replace('ss', $secondsPadded, $formatLower);

        /** @noinspection PhpUnnecessaryLocalVariableInspection */
        $formatLower = str_replace('s', strval($seconds), $formatLower);

        return $formatLower;


    }

    /**
     * Get the number of seconds.
     *
     * @return float Number of seconds
     */
    public function getSeconds() {
        return $this->microseconds / 1000000;
    }

    /**
     * Convert this TimeSpan to a DateInterval.
     *
     * @return \DateInterval The DateInterval representing the same time span
     */
    public function toDateInterval(): \DateInterval {
        $seconds = (int)$this->getSeconds();
        $microseconds = $this->microseconds % 1000000;

        $interval = new \DateInterval("PT{$seconds}S");
        if (version_compare(PHP_VERSION, '7.1.0', '>=')) {
            $interval->f = $microseconds / 1000000;
        }

        return $interval;
    }

    /**
     * Add a TimeSpan to this TimeSpan.
     *
     * @param TimeSpan|\DateInterval $timespan TimeSpan to add
     * @return TimeSpan New TimeSpan representing the sum
     */
    public function add($timespan): TimeSpan {
        return new TimeSpan($this->microseconds + self::_getMicroseconds($timespan));
    }

    private static function _getMicroseconds($timespan) {
        if ($timespan instanceof TimeSpan) {
            return $timespan->getMicroseconds();
        } elseif ($timespan instanceof \DateInterval) {
            $seconds = $timespan->days * 24 * 60 * 60
                       + $timespan->h * 60 * 60
                       + $timespan->i * 60
                       + $timespan->s;

            // If PHP version is 7.1.0 or later, use fractional seconds
            if (version_compare(PHP_VERSION, '7.1.0', '>=')) {
                return $seconds * 1000000 + $timespan->f * 1000000;
            } else {
                // If PHP version is earlier than 7.1.0, ignore fractional seconds
                return $seconds * 1000000;
            }
        } else {
            throw new \InvalidArgumentException("Argument must be TimeSpan or DateInterval");
        }
    }

    /**
     * Get the number of microseconds.
     *
     * @return float|int Number of microseconds
     */
    public function getMicroseconds() {
        return $this->microseconds;
    }

    /**
     * Subtract a TimeSpan from this TimeSpan.
     *
     * @param TimeSpan|\DateInterval $timespan TimeSpan to subtract
     * @return TimeSpan New TimeSpan representing the difference
     */
    public function subtract($timespan): TimeSpan {
        return new TimeSpan($this->microseconds - self::_getMicroseconds($timespan));
    }

    /**
     * Get the number of milliseconds.
     *
     * @return float Number of milliseconds
     */
    public function getMilliseconds() {
        return $this->microseconds / 1000;
    }

    /**
     * Get the number of days.
     *
     * @return float Number of days
     */
    public function getDays() {
        return $this->getHours() / 24;
    }

    /**
     * Get the number of hours.
     *
     * @return float Number of hours
     */
    public function getHours() {
        return $this->getMinutes() / 60;
    }

    /**
     * Get the number of minutes.
     *
     * @return float Number of minutes
     */
    public function getMinutes() {
        return $this->getSeconds() / 60;
    }

    /**
     * Check if this TimeSpan is greater than another TimeSpan or DateInterval.
     *
     * @param TimeSpan|\DateInterval $timespan TimeSpan or DateInterval to compare
     * @return bool True if this TimeSpan is greater, false otherwise
     */
    public function gt($timespan): bool {
        return $this->microseconds > self::_getMicroseconds($timespan);
    }

    /**
     * Check if this TimeSpan is less than another TimeSpan or DateInterval.
     *
     * @param TimeSpan|\DateInterval $timespan TimeSpan or DateInterval to compare
     * @return bool True if this TimeSpan is less, false otherwise
     */
    public function lt($timespan): bool {
        return $this->microseconds < self::_getMicroseconds($timespan);
    }

    /**
     * Check if this TimeSpan is equal to another TimeSpan or DateInterval.
     *
     * @param TimeSpan|\DateInterval $timespan TimeSpan or DateInterval to compare
     * @return bool True if this TimeSpan is equal, false otherwise
     */
    public function eq($timespan): bool {
        return $this->microseconds == self::_getMicroseconds($timespan);
    }

    /**
     * Check if this TimeSpan is less than or equal to another TimeSpan or DateInterval.
     *
     * @param TimeSpan|\DateInterval $timespan TimeSpan or DateInterval to compare
     * @return bool True if this TimeSpan is less or equal, false otherwise
     */
    public function le($timespan): bool {
        return $this->microseconds <= self::_getMicroseconds($timespan);
    }

    /**
     * Check if this TimeSpan is greater than or equal to another TimeSpan or DateInterval.
     *
     * @param TimeSpan|\DateInterval $timespan TimeSpan or DateInterval to compare
     * @return bool True if this TimeSpan is greater or equal, false otherwise
     */
    public function ge($timespan): bool {
        return $this->microseconds >= self::_getMicroseconds($timespan);
    }

    /**
     * Check if this TimeSpan is not equal to another TimeSpan or DateInterval.
     *
     * @param TimeSpan|\DateInterval $timespan TimeSpan or DateInterval to compare
     * @return bool True if this TimeSpan is not equal, false otherwise
     */
    public function ne($timespan): bool {
        return $this->microseconds != self::_getMicroseconds($timespan);
    }

}