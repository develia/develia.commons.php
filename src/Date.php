<?php


namespace Develia;


use DateInterval;

use DateTime;
use DateTimeImmutable;
use DateTimeInterface;
use DateTimeZone;

class Date {

    private function __construct() { }

    /**
     * @param int $year
     * @param int $month
     * @param int $day
     * @param int|null $hour
     * @param int|null $minute
     * @param int|null $second
     * @param int|null $microsecond
     * @return DateTimeImmutable
     */
    public static function create(int $year, int $month, int $day, ?int $hour = 0, ?int $minute = 0, ?int $second = 0, ?int $microsecond = 0): DateTimeImmutable {
        $output = new DateTimeImmutable();
        $output = $output->setDate($year, $month, $day);
        if (version_compare(phpversion(), '7.1', ">=")) {
            $output->setTime($hour, $minute, $second, $microsecond);
        } else {
            $output->setTime($hour, $minute, $second);
        }
        return $output;

    }

    /**
     * @param DateTimeInterface $a
     * @param DateTimeInterface $b
     * @return TimeSpan
     */
    public static function difference(DateTimeInterface $a, DateTimeInterface $b): TimeSpan {
        $interval = $a->diff($b);
        return TimeSpan::fromDateInterval($interval);
    }


    /**
     * @param DateTimeInterface $datetime
     * @return DateTimeInterface returs the last moment of the day
     */
    public static function endOfDay(DateTimeInterface $datetime): DateTimeInterface {
        if (version_compare(phpversion(), '7.1', ">=")) {
            $datetime = $datetime->setTime(23, 59, 59, 59);
        } else {
            $datetime = $datetime->setTime(23, 59, 59);
        }

        return $datetime;
    }

    /**
     * @param DateTimeInterface $datetime
     * @return int
     */
    public static function getDay(DateTimeInterface $datetime): int {
        return intval($datetime->format("d"));
    }

    /**
     * @param int|DateTimeInterface $input
     * @param string|null $locale
     * @return false|string
     */
    public static function getMonthName($input, ?string $locale = null) {
        $locale = $locale ?: setlocale(LC_ALL, "0");

        if ($input instanceof DateTimeInterface) {
            $date = $input;
        } elseif (is_int($input)) {
            $date = new DateTime("2023-$input-01");
        } else {
            return false;
        }

        return self::localize($date, $locale, "%B");
    }

    /**
     * @param DateTimeInterface $date
     * @param string $locale
     * @param string $strftime_format
     * @return false|string
     */
    private static function localize(DateTimeInterface $date, string $locale, string $strftime_format) {
        // Guardar el valor original de la localización

        try {

            $originalLocale = setlocale(LC_TIME, "0");

            // Configurar la nueva localización
            setlocale(LC_TIME, $locale);
            $output = strftime($strftime_format, $date->getTimestamp());

        } finally {

            setlocale(LC_TIME, $originalLocale);

        }

        return $output;
    }

    /**
     * @param int|DateTimeInterface $input
     * @param string|null $locale
     * @return false|string
     */
    public static function getDayName($input, ?string $locale = null) {
        $locale = $locale ?: setlocale(LC_ALL, "0");

        if ($input instanceof DateTimeInterface) {
            $date = $input;
        } elseif (is_int($input)) {
            $date = new DateTime("2023-08-$input");
        } else {
            return false;
        }

        return self::localize($date, $locale, "%A");
    }

    /**
     * @param DateTimeInterface $datetime
     * @return int
     */
    public static function getHour(DateTimeInterface $datetime): int {
        return intval($datetime->format("H"));
    }

    /**
     * @param DateTimeInterface $datetime
     * @return int
     */
    public static function getMinute(DateTimeInterface $datetime): int {
        return intval($datetime->format("i"));
    }

    /**
     * @param DateTimeInterface $datetime
     * @return int
     */
    public static function getMonth(DateTimeInterface $datetime): int {
        return intval($datetime->format("m"));
    }

    /**
     * @param DateTimeInterface $datetime
     * @return int
     */
    public static function getSecond(DateTimeInterface $datetime): int {
        return intval($datetime->format("s"));
    }

    /**
     * @param DateTimeInterface $datetime
     * @return int
     */
    public static function getYear(DateTimeInterface $datetime): int {
        return intval($datetime->format("Y"));
    }

    /**
     * @param DateTimeInterface $datetime
     * @return DateTimeInterface returs the first moment of the day
     */
    public static function startOfDay(DateTimeInterface $datetime): DateTimeInterface {
        /** @noinspection PhpRedundantOptionalArgumentInspection */
        return $datetime->setTime(0, 0, 0);
    }

    /**
     * @param DateTimeZone | null $timezone
     * @param bool $immutable
     * @return DateTimeInterface
     * @throws \DateMalformedStringException
     */
    public static function now(?DateTimeZone $timezone = null, bool $immutable = true) {
        return $immutable ? new DateTimeImmutable('now', $timezone) : new DateTime('now', $timezone);
    }


    /**
     * @throws \DateInvalidTimeZoneException
     */
    public static function parse($string, $format, $timezone = null, $immutable = true): ?DateTimeImmutable {
        self::tryParse($string, $output, $format, $timezone, $immutable);
        return $output;
    }

    /**
     * @param string $string
     * @param string $format
     * @param DateTimeImmutable | null $output
     * @param DateTimeZone | string | null $timezone
     * @param bool $immutable
     * @return bool
     * @throws \DateInvalidTimeZoneException
     */
    public static function tryParse(string $string, ?DateTimeImmutable &$output, string $format, $timezone = null, bool $immutable = true): bool {
        if (is_string($timezone)) {
            $timezone = new DateTimeZone($timezone);
        }

        $date = $immutable ? DateTimeImmutable::createFromFormat($format, $string, $timezone) : DateTime::createFromFormat($format, $string, $timezone);
        if ($date !== false) {
            $output = $date;
            return true;
        }

        $output = null;
        return false;
    }

    /**
     * Changes the date format of a given string from one format to another.
     *
     * @param string $date The date string to be converted.
     * @param string $input_format The input format of the date string.
     * @param string $output_format The desired output format for the date string.
     *
     * @return string The date string in the desired output format.
     * @throws \InvalidArgumentException If the input format is invalid or does not match the date string.
     *
     */
    public static function changeFormat(string $date, string $input_format, string $output_format): string {
        $date_obj = DateTime::createFromFormat($input_format, $date);

        if ($date_obj === false) {
            throw new \InvalidArgumentException("Invalid input format.");
        }

        return $date_obj->format($output_format);
    }

    /**
     * @param DateTimeInterface $dateTime
     * @return TimeSpan
     */
    public static function getTimeOfDay(DateTimeInterface $dateTime): TimeSpan {
        $seconds = intval($dateTime->format('H')) * 3600 + intval($dateTime->format('i')) * 60 + intval($dateTime->format('s'));

        $microseconds = intval($dateTime->format('u'));

        $totalMicroseconds = $seconds * 1000000 + $microseconds;

        return new TimeSpan($totalMicroseconds);
    }

    /**
     * @param DateTimeInterface $dateTime
     * @param $timespan
     * @return mixed
     */
    public static function setTimeOfDay(DateTimeInterface $dateTime, $timespan) {
        if ($timespan instanceof DateInterval) {
            $timespan = TimeSpan::fromDateInterval($timespan);
        }

        $hours = (int)$timespan->getHours();
        $minutes = (int)$timespan->getMinutes();
        $seconds = (int)$timespan->getSeconds();
        $microseconds = (int)$timespan->getMicroseconds();
        $timeString = $hours . ':' . $minutes . ':' . $seconds . '.' . $microseconds;
        $modifiedDateTime = $dateTime->format('Y-m-d') . ' ' . $timeString;

        return $dateTime->createFromFormat('Y-m-d H:i:s.u', $modifiedDateTime);
    }

    public static function getMicroseconds($date) {
        return $date->format('u');
    }

    /**
     * @param DateTimeInterface $date
     * @param $milliseconds
     * @return DateTimeInterface
     */
    public static function setMilliseconds(DateTimeInterface $date, $milliseconds): DateTimeInterface {
        return self::setMicroseconds($date, $milliseconds * 1000);
    }

    /**
     * @param DateTimeInterface $date
     * @param $microseconds
     * @return DateTimeInterface
     */
    public static function setMicroseconds(DateTimeInterface $date, $microseconds): DateTimeInterface {
        return $date->modify($date->format('Y-m-d H:i:s.') . str_pad($microseconds, 6, '0', STR_PAD_LEFT));
    }

    public static function getMilliseconds(DateTimeInterface $date) {
        return floor(self::getMicroseconds($date) / 1000);
    }

    /**
     * @param DateTimeInterface $date
     * @param int $day
     * @return DateTimeInterface
     */
    public static function setDay(DateTimeInterface $date, int $day): DateTimeInterface {
        return $date->setDate($date->format('Y'), $date->format('m'), $day);
    }

    /**
     * @param DateTimeInterface $date
     * @param int $month
     * @return DateTimeInterface
     */
    public static function setMonth(DateTimeInterface $date, int $month): DateTimeInterface {
        return $date->setDate($date->format('Y'), $month, $date->format('d'));
    }

    /**
     * @param DateTimeInterface $date
     * @param int $year
     * @return DateTimeInterface
     */
    public static function setYear(DateTimeInterface $date, int $year): DateTimeInterface {
        return $date->setDate($year, $date->format('m'), $date->format('d'));
    }

    public static function setMinute(DateTimeInterface $date, int $minute) {
        return $date->setTime($date->format('H'), $minute, $date->format('s'));
    }

    /**
     * @param DateTimeInterface $date
     * @param int $second
     * @return DateTimeInterface
     */
    public static function setSecond(DateTimeInterface $date, int $second): DateTimeInterface {
        return $date->setTime($date->format('H'), $date->format('i'), $second);
    }

    /**
     * @param DateTimeImmutable|DateTime $datetime
     * @param TimeSpan|DateInterval $interval
     * @return DateTimeInterface
     */
    public static function add(DateTimeInterface $datetime, $interval): DateTimeInterface {
        if ($interval instanceof TimeSpan) {
            $interval = $interval->toDateInterval();
        }

        return $datetime->add($interval);
    }

    /**
     * @param DateTimeImmutable|DateTime $datetime
     * @param TimeSpan|DateInterval $interval
     * @return DateTimeInterface
     * @throws \DateInvalidOperationException
     */
    public static function subtract(DateTimeInterface $datetime, $interval): DateTimeInterface {
        if ($interval instanceof TimeSpan) {
            $interval = $interval->toDateInterval();
        }

        return $datetime->sub($interval);
    }

}

