<?php

/** @noinspection PhpRedundantOptionalArgumentInspection */

/** @noinspection PhpFullyQualifiedNameUsageInspection */

namespace Develia;


/**
 * @template TValue
 * @template TKey
 */
class From implements \IteratorAggregate, \Countable {


    private $iterable;

    /**
     * @param array<TKey,TValue> | \IteratorAggregate<TKey,TValue> $iterable
     */
    public function __construct($iterable) {
        $this->iterable = $iterable;
    }

    /** @noinspection PhpUnusedParameterInspection */
    private static function _keySelectorFn($k, $v, $i) {
        return $k;
    }

    /**
     * Splits the input data into smaller batches of the specified size.
     *
     * @param int $batchSize The size of each batch.
     * @param bool $dropRemainder If true, the remaining elements that do not fill a complete batch are discarded.
     */
    public function chunk(int $batchSize, bool $dropRemainder = false): From {

        return new From(new IteratorFactory(function () use ($batchSize, $dropRemainder) {
            $batch = [];
            foreach ($this->getIterator() as $key => $value) {
                $batch[$key] = $value;
                if (count($batch) === $batchSize) {
                    yield $batch;
                    $batch = [];
                }
            }
            if (!$dropRemainder && count($batch) > 0) {
                yield $batch;
            }

        }));
    }

    /**
     * Yields instances of a specific class from an iterable.
     *
     * Iterates over each element in the iterable. If an element is an instance of the specified class,
     * it is yielded.
     * @template class-string
     * @param class-string $class The name of the class to check instances against.
     * @return From<class-string> Yields instances of the specified class.
     */
    public function instancesOf($class): From {
        return new From(new IteratorFactory(function () use ($class) {
            foreach ($this as $item) {
                if ($item instanceof $class) {
                    yield $item;
                }
            }
        }));
    }

    /**
     * @param string $separator
     * @return string
     */
    function implode(string $separator): string {
        $output = "";


        foreach ($this as $item) {
            if ($output == "") {
                $output = strval($item);
            } else {
                $output .= $separator . $item;
            }
        }
        return $output;
    }

    /**
     * @param $key
     * @param $output
     * @param $fallback
     * @return bool
     * @deprecated Use tryResolve
     */
    public function tryGetElementByKey($key, &$output, $fallback = null): bool {
        return $this->tryResolve($key, $output, $fallback);
    }

    /**
     * @param mixed $key
     * @param mixed $output
     * @param mixed $fallback
     * @return bool
     */
    public function tryResolve($key, &$output, $fallback = null): bool {

        foreach ($this as $k => $v) {
            if ($k == $key) {
                $output = $v;
                return true;
            }
        }

        if (func_num_args() > 2) {
            $output = $fallback;
        }

        return false;
    }

    /**
     * @param $group_by_selector
     * @param $aggregation
     * @return From
     */
    public function groupBy($group_by_selector, $aggregation = null): From {
        return new From(new IteratorFactory(function () use ($aggregation, $group_by_selector) {


            $group_by_selector = Functions::getKeyValueIndexFn($group_by_selector);
            $aggregation = Functions::getKeyValueIndexFn($aggregation);

            $keys = [];
            $values = [];

            $i = 0;
            foreach ($this as $k => $v) {
                $key = call_user_func($group_by_selector, $k, $v, $i);
                if (!in_array($key, $keys)) {
                    $keys[] = $key;
                }

                $index = array_search($key, $keys);
                if (!isset($values[$index])) {
                    $values[$index] = [];
                }

                $values[$index][] = $v;
                $i++;
            }

            if ($aggregation) {
                for ($i = 0; $i < count($keys); $i++) {
                    yield $keys[$i] => call_user_func($aggregation, $keys[$i], $values[$i], $i);
                }
            } else {
                for ($i = 0; $i < count($keys); $i++) {
                    yield $keys[$i] => $values[$i];
                }
            }

        }));
    }

    /**
     */
    public function collect(): From {


        $keys = [];
        $values = [];

        foreach ($this as $k => $v) {
            $keys[] = $k;
            $values[] = $v;
        }

        return From::keysValues($keys, $values);

    }

    /**
     * @param array $keys
     * @param array $values
     * @return From
     */
    public static function keysValues(array $keys, array $values): From {
        return new From(new IteratorFactory(function () use ($values, $keys) {

            for ($i = 0; $i < count($keys); $i++)
                yield $keys[$i] => $values[$i];

        }));

    }

    /**
     * @param null $predicate
     * @return int
     */
    public function count($predicate = null): int {
        if ($predicate) {
            return $this->filter($predicate)->count();
        } else {
            $output = 0;
            foreach ($this->getIterator() as $ignored) {
                $output++;
            }
            return $output;
        }

    }

    /**
     * @param callable | null $predicate
     * @return From<TValue>
     */
    public function filter(?callable $predicate = null): From {
        return new From(new IteratorFactory(function () use ($predicate) {
            $i = 0;
            $predicate = $predicate ? Functions::getKeyValueIndexFn($predicate) : [From::class, "keyValueIndexIdentity"];
            foreach ($this->getIterator() as $k => $v) {
                if (call_user_func($predicate, $k, $v, $i++)) {
                    yield $k => $v;
                }

            }
        }));
    }


    /**
     * @return IterableIterator
     * @throws
     */
    public function getIterator(): IterableIterator {
        return new IterableIterator($this->iterable);
    }


    public function reduce($func) {
        $iterator = $this->getIterator();
        $iterator->rewind();

        $output = $iterator->current();
        $iterator->next();

        while ($iterator->valid()) {
            $output = call_user_func($func, $output, $iterator->current());
            $iterator->next();
        }

        return $output;
    }

    /**
     * Applies a function to each element of the iterator, accumulating the results.
     *
     * @param mixed $function The callback function to apply to each element.
     * @param mixed $initial (Optional) The initial value to start the accumulation. If not provided,
     *                       the first element of the iterator will be used as the initial value.
     * @return mixed The accumulated result.
     * @throws \Exception
     */
    public function aggregate($function, $initial = null) {

        $iterator = $this->getIterator();

        if (func_num_args() == 1 && $iterator->valid()) {
            $output = $iterator->current();
            $iterator->next();
        } else {
            $output = $initial;
        }

        while ($iterator->valid()) {
            $output = $function($output, $iterator->current());
            $iterator->next();
        }

        return $output;
    }

    /**
     * @param mixed $selector
     * @return float|int
     *
     */
    public function average($selector = null) {
        $selector = $selector ? Functions::getKeyValueIndexFn($selector) : [Functions::class, "keyValueIndexIdentity"];

        $output = 0;
        $i = 0;
        foreach ($this->getIterator() as $k => $v) {
            $output += call_user_func($selector, $k, $v, $i);
            $i++;
        }

        if ($i == 0) {
            return null;
        }

        return $output / $i;
    }

    /**
     * @param mixed $selector
     * @return float|int
     *
     */
    public function sum($selector = null) {
        $selector = $selector ? Functions::getKeyValueIndexFn($selector) : [Functions::class, "keyValueIndexIdentity"];

        $output = 0;
        $i = 0;
        foreach ($this->getIterator() as $k => $v) {
            $output += call_user_func($selector, $k, $v, $i);
            $i++;
        }
        return $output;
    }


    /**
     */
    public function union($transversable): From {
        return new From(new IteratorFactory(function () use ($transversable) {
            foreach ($this->getIterator() as $item)
                yield $item;
            foreach ($transversable as $item)
                yield $item;
        }));
    }

    /**
     * @param $predicate | null
     * @return bool
     *
     */
    public function any($predicate = null): bool {
        $predicate = Functions::getKeyValueIndexFn($predicate ?: function () {
            return true;
        });

        $i = 0;
        foreach ($this->getIterator() as $k => $v) {
            if (call_user_func($predicate, $k, $v, $i++)) {
                return true;
            }
        }

        return false;
    }

    /**
     * @param $predicate
     * @return bool
     *
     */
    public function all($predicate): bool {

        $predicate = Functions::getKeyValueIndexFn($predicate);

        $i = 0;
        foreach ($this->getIterator() as $k => $v) {
            if (!call_user_func($predicate, $k, $v, $i++)) {
                return false;
            }
        }

        return true;
    }

    /**
     * @template R
     * @param callable():R $value_selector
     * @param callable():R | null $key_selector
     * @return From<R>
     */
    public function map(callable $value_selector, ?callable $key_selector = null): From {
        return new From(new IteratorFactory(function () use ($key_selector, $value_selector) {

            $i = 0;


            $value_selector = Functions::createMapper(Functions::getKeyValueIndexFn($value_selector));
            $key_selector = $key_selector ? Functions::getKeyValueIndexFn($key_selector) : [self::class, "_keySelectorFn"];

            foreach ($this->getIterator() as $k => $v) {
                yield call_user_func($key_selector, $k, $v, $i) => call_user_func($value_selector, $k, $v, $i);
                $i++;
            }

        }));
    }


    /**
     * @param $selector
     * @return From
     */
    public function mapMany($selector): From {

        return new From(new IteratorFactory(function () use ($selector) {

            $i = 0;
            $selector = Functions::getKeyValueIndexFn($selector);

            foreach ($this->getIterator() as $k => $v) {
                foreach (call_user_func($selector, $k, $v, $i++) as $k2 => $v2)
                    yield $k2 => $v2; //No tocar
            }

        }));
    }

    /**
     * @param $obj
     * @param mixed|null $key
     * @return From
     */
    public function append($obj, $key = null): From {
        return new From(new IteratorFactory(function () use ($key, $obj) {
            foreach ($this->getIterator() as $k => $v)
                yield $k => $v;

            if (is_null($key)) {
                yield $obj;
            } else {
                yield $key => $obj;
            }

        }));
    }

    /**
     * @param $selector
     * @return mixed
     *
     */
    public function max($selector = null) {

        $selector = $selector ? Functions::getKeyValueIndexFn($selector) : [Functions::class, "keyValueIndexIdentity"];

        $iterator = $this->getIterator();
        $iterator->rewind();

        $i = 0;
        $key = $iterator->key();
        $current = $iterator->current();
        $output = call_user_func($selector, $key, $current, $i++);

        $iterator->next();

        while ($iterator->valid()) {

            $key = $iterator->key();
            $current = $iterator->current();
            $value = call_user_func($selector, $key, $current, $i++);

            if ($value > $output) {
                $output = $value;
            }

            $iterator->next();
        }

        return $output;

    }

    /**
     * @param $selector
     * @return mixed
     *
     */
    public function min($selector = null) {


        $selector = $selector ? Functions::getKeyValueIndexFn($selector) : [Functions::class, "keyValueIndexIdentity"];

        $iterator = $this->getIterator();
        $iterator->rewind();

        $i = 0;
        $key = $iterator->key();
        $current = $iterator->current();
        $output = call_user_func($selector, $key, $current, $i++);

        $iterator->next();

        while ($iterator->valid()) {

            $key = $iterator->key();
            $current = $iterator->current();
            $value = call_user_func($selector, $key, $current, $i++);

            if ($value < $output) {
                $output = $value;
            }

            $iterator->next();
        }

        return $output;

    }

    /**
     * @param $n
     * @return From
     */
    public function head($n): From {
        return new From(new IteratorFactory(function () use ($n) {

            $i = 0;
            foreach ($this->getIterator() as $k => $v) {
                if ($n > $i) {
                    yield $k => $v;
                    $i++;
                } else {
                    break;
                }
            }

        }));
    }

    public function last() {
        $output = null;
        foreach ($this as $item) {
            $output = $item;
        }
        return $output;
    }

    /**
     * @param $n
     * @return From
     */
    public function tail($n): From {
        return new From(new IteratorFactory(function () use ($n) {

            $output = [];

            foreach ($this as $item) {
                $output[] = $item;

                if (count($output) > 5) {
                    array_shift($output);
                }
            }

            return $output;

        }));
    }

    /**
     * @param $selector
     * @return mixed
     *
     */
    public function highest($selector) {

        $selector = Functions::getKeyValueIndexFn($selector);

        $iterator = $this->getIterator();
        $iterator->rewind();

        $i = 0;
        $key = $iterator->key();
        $output = $iterator->current();
        $max_value = call_user_func($selector, $key, $output, $i++);

        $iterator->next();

        while ($iterator->valid()) {

            $k = $iterator->key();
            $current = $iterator->current();
            $value = call_user_func($selector, $k, $current, $i++);

            if ($value > $max_value) {
                $output = $current;
                $max_value = $value;
            }


            $iterator->next();
        }

        return $output;

    }

    /**
     * @param $iterable
     * @return From
     */
    public function subtract($iterable): From {

        return new From(new IteratorFactory(function () use ($iterable) {

            $iterable = from($iterable);
            foreach ($this as $k => $v) {
                if (!$iterable->contains($v)) {
                    yield $k => $v;
                }

            }

        }));
    }

    /**
     * @param $obj
     * @return bool
     */
    public function contains($obj): bool {
        foreach ($this as $item) {
            if ($obj === $item) {
                return true;
            }
        }
        return false;
    }

    /**
     * @param array|null $cache
     * @return From
     */
    public function cache(?array &$cache): From {
        if (is_null($cache)) {
            $cache = $this->toArray(true);
        }

        return new From($cache);
    }

    /**
     * @param bool $associative
     * @return array<TKey,TValue>
     */
    public function toArray(bool $associative = true): array {
        return iterator_to_array($this->getIterator(), $associative);
    }

    /**
     * @param $selector
     * @param string $order
     * @return From
     */
    public function orderBy($selector, string $order = "asc"): From {


        return new From(new IteratorFactory(function () use ($selector, $order) {

            $copy = $this->toArray(true);

            if (strtolower($order) == "desc") {
                usort($copy, function ($a, $b) use ($selector) {
                    return Obj::compare(call_user_func($selector, $b), call_user_func($selector, $a));
                });

            } else if (strtolower($order) == "asc") {
                usort($copy, function ($a, $b) use ($selector) {
                    return Obj::compare(call_user_func($selector, $a), call_user_func($selector, $b));
                });
            }

            foreach ($copy as $item)
                yield $item;

        }));
    }

    public function orderKeysBy($selector, $order = "asc"): From {
        return new From(new IteratorFactory(function () use ($selector, $order) {


            $copy = $this->toArray(true);

            if (strtolower($order) == "desc") {
                uksort($copy, function ($a, $b) use ($selector) {
                    return Obj::compare(call_user_func($selector, $b), call_user_func($selector, $a));
                });

            } else if (strtolower($order) == "asc") {
                uksort($copy, function ($a, $b) use ($selector) {
                    return Obj::compare(call_user_func($selector, $a), call_user_func($selector, $b));
                });
            }

            foreach ($copy as $k => $v)
                yield $k => $v;

        }));
    }

    /**
     * @param $selector
     * @return mixed
     *
     */
    public function argmin($selector = null) {


        $selector = $selector ? Functions::getKeyValueIndexFn($selector) : [Functions::class, "keyValueIndexIdentity"];

        $iterator = $this->getIterator();
        $iterator->rewind();

        $i = 0;
        $key = $iterator->key();
        $output = $key;
        $current = $iterator->current();
        $min_value = call_user_func($selector, $key, $current, $i++);

        $iterator->next();

        while ($iterator->valid()) {

            $key = $iterator->key();
            $current = $iterator->current();
            $value = call_user_func($selector, $key, $current, $i++);

            if ($value < $min_value) {
                $min_value = $value;
                $output = $key;
            }


            $iterator->next();
        }

        return $output;

    }

    /**
     * @param $selector
     * @return mixed
     *
     */
    public function argmax($selector = null) {

        $selector = $selector ? Functions::getKeyValueIndexFn($selector) : [Functions::class, "keyValueIndexIdentity"];

        $iterator = $this->getIterator();
        $iterator->rewind();

        $i = 0;
        $key = $iterator->key();
        $output = $key;
        $current = $iterator->current();
        $max_value = call_user_func($selector, $key, $current, $i++);

        $iterator->next();

        while ($iterator->valid()) {

            $key = $iterator->key();
            $current = $iterator->current();
            $value = call_user_func($selector, $key, $current, $i++);

            if ($value > $max_value) {
                $max_value = $value;
                $output = $key;
            }


            $iterator->next();
        }

        return $output;

    }

    /**
     * @param $selector
     * @return mixed
     *
     */
    public function lowest($selector) {

        $function = Functions::getKeyValueIndexFn($selector);

        $iterator = $this->getIterator();
        $iterator->rewind();

        $i = 0;
        $output = $iterator->current();
        $min_value = call_user_func($function, $iterator->current(), $output, $i++);

        $iterator->next();

        while ($iterator->valid()) {

            $k = $iterator->key();
            $obj = $iterator->current();
            $value = call_user_func($function, $k, $obj, $i++);

            if ($value < $min_value) {
                $output = $obj;
                $min_value = $value;
            }


            $iterator->next();
        }

        return $output;
    }

    /**
     * @param callable|null $keyGetter
     * @param callable|null $valueGetter
     * @return \SplObjectStorage
     *
     */
    public function toObjectStorage(callable $keyGetter = null, callable $valueGetter = null): \SplObjectStorage {
        $keyGetter = Functions::getKeyValueIndexFn($keyGetter) ?: [Functions::class, "keyValueIndexIdentity"];
        $valueGetter = Functions::getKeyValueIndexFn($valueGetter) ?: [Functions::class, "keyValueIndexKey"];

        $output = new \SplObjectStorage();
        $i = 0;
        foreach ($this as $k => $v) {
            $key = call_user_func($keyGetter, $k, $v, $i);
            $value = call_user_func($valueGetter, $k, $v, $i);
            $output->attach($key, $value);
            $i++;
        }
        return $output;
    }

    /**
     * @param $callable
     * @return void
     *
     */
    public function each($callable) {
        $callable = Functions::getKeyValueIndexFn($callable);
        $i = 0;
        foreach ($this->getIterator() as $k => $v)
            call_user_func($callable, $k, $v, $i++);
    }

    /**
     * @return From
     */
    function transpose(): From {
        return new From(new IteratorFactory(function () {
            $transposed = [];
            foreach ($this as $key => $subarray) {
                if (!Obj::isIterable($subarray)) {
                    throw new \InvalidArgumentException();
                }

                foreach ($subarray as $subkey => $value) {
                    $transposed[$subkey][$key] = $value;
                }

            }
            yield from $transposed;
        }));


    }


    /**
     * @param bool $preserveKeys
     * @return From
     */
    public function flatten(bool $preserveKeys = false): From {
        return new From(new IteratorFactory(function () use ($preserveKeys) {
            if ($preserveKeys) {
                foreach ($this->getIterator() as $v) {
                    foreach ($v as $k => $subitem)
                        yield $k => $subitem;
                }
            } else {
                foreach ($this->getIterator() as $v) {
                    /** @noinspection PhpUnusedLocalVariableInspection */
                    foreach ($v as $k => $subitem)
                        yield $subitem;
                }
            }
        }));
    }

    /**
     * @return From
     */
    public function keys(): From {
        return new From(new IteratorFactory(function () {
            foreach ($this as $k => $v) {
                yield $k;
            }
        }));

    }

    /**
     * @return From
     */
    public function values(): From {
        return new From(new IteratorFactory(function () {
            foreach ($this as $v) {
                yield $v;
            }
        }));
    }

    /**
     * @param $key
     * @return bool
     */
    public function containsKey($key): bool {
        foreach ($this as $k => $v) {
            if ($key === $k) {
                return true;
            }
        }
        return false;
    }

    /**
     * @param callable $predicate
     * @param From<TValue> $ifTrue
     * @param From<TValue> $ifFalse
     * @return void
     *
     */
    public function split(callable $predicate, From &$ifTrue, From &$ifFalse) {
        $predicate = Functions::getKeyValueIndexFn($predicate);

        $ifTrueTmp = [];
        $ifFalseTmp = [];

        $i = 0;
        foreach ($this as $k => $v) {
            if (call_user_func($predicate, $k, $v, $i)) {
                $ifTrueTmp[$k] = $v;
            } else {
                $ifFalseTmp[$k] = $v;
            }

            $i++;
        }

        $ifTrue = new From($ifTrueTmp);
        $ifFalse = new From($ifFalseTmp);
    }


    /**
     * @param mixed|null $fallback
     * @return TValue
     */
    public function first($predicate = null, $fallback = null) {
        $predicate = Functions::getKeyValueIndexFn($predicate);

        if ($predicate) {
            $i = 0;
            foreach ($this->getIterator() as $k => $v) {
                if (call_user_func($predicate, $k, $v, $i)) {
                    return $v;
                }
                $i++;
            }

        } else {
            foreach ($this->getIterator() as $item)
                return $item;
        }
        return $fallback;
    }

    /**
     * @param mixed|null $fallback
     * @return TValue
     * @throws \Exception
     */
    public function single($predicate = null, $fallback = null) {
        $predicate = Functions::getKeyValueIndexFn($predicate);
        $output = null;
        $found = false;

        if ($predicate) {
            $i = 0;
            foreach ($this->getIterator() as $k => $v) {
                if (call_user_func($predicate, $k, $v, $i)) {
                    if (!$found) {
                        $output = $v;
                        $found = true;
                    } else {
                        throw new \Exception("More than one elment found.");
                    }
                }
                $i++;
            }


        } else {
            foreach ($this->getIterator() as $item) {
                if (!$found) {
                    $output = $item;
                    $found = true;
                } else {
                    throw new \Exception("More than one elment found.");
                }
            }
        }

        return $found ? $output : $fallback;
    }

    /**
     * @param $callable
     * @return From
     */
    function tap($callable): From {
        return new From(new IteratorFactory(function () use ($callable) {
            $callable = Functions::getKeyValueIndexFn($callable);
            $i = 0;
            foreach ($this as $k => $v) {
                call_user_func($callable, $k, $v, $i++);
                yield $k => $v;
            }
        }));

    }

    /**
     * @param $i
     * @return mixed
     */
    public function valueAt($i) {
        $j = 0;
        foreach ($this->getIterator() as $item) {
            if ($i == $j) {
                return $item;
            }

            $j++;
        }
        throw new \OutOfRangeException();

    }

    /**
     * @param $i
     * @return mixed
     */
    public function keyAt($i) {
        $j = 0;
        foreach ($this->getIterator() as $k => $v) {
            if ($i == $j) {
                return $k;
            }

            $j++;
        }
        throw new \OutOfRangeException();

    }

    /**
     * @deprecated
     */
    function unique(): From {
        return $this->distinct();
    }

    /**
     * Returns a new collection that contains only distinct elements from the current collection.
     * @return From - A new collection that contains only distinct elements.
     */
    function distinct(): From {
        return new From(new IteratorFactory(function () {
            foreach (array_unique($this->toArray(false)) as $item)
                yield $item;
        }));

    }

    /**
     * @return array<int,TValue>
     */
    public function toVector(): array {
        return $this->toArray(false);
    }

    /**
     * @param array|null $weights An array of weights for each element in the array. If empty, a random element will be selected with equal probability.
     * @return mixed|null Returns a random element from the array based on the weights provided. If the weights array is empty, a random element is selected with equal probability. Returns
     * null if the weights array is empty and the input array is also empty.
     */
    public function random(?array $weights = null) {
        $array = iterator_to_array($this);

        if (count($array) == 0) {
            return null;
        }

        if (is_null($weights)) {
            return $array[array_rand($array)];
        }


        $totalWeight = array_sum($weights);
        $cumulativeWeight = 0;
        $rand = mt_rand(0, $totalWeight - 1);

        foreach ($array as $k => $v) {

            $weight = $weights[$k] ?? 1;
            $cumulativeWeight += $weight;

            if ($rand <= $cumulativeWeight) {
                return $v;
            }
        }

        return null;


    }

    /**
     * @param $objs
     * @return bool
     */
    public function containsAny($objs): bool {
        foreach ($this->getIterator() as $item) {
            if (in_array($item, $objs)) {
                return true;
            }
        }
        return false;
    }

    /**
     * @param $objs
     * @return bool
     */
    public function containsAll($objs): bool {
        foreach ($this->getIterator() as $item) {
            if (!in_array($item, $objs)) {
                return false;
            }
        }
        return true;
    }

    /**
     * @param $iterable
     * @return From
     */
    public function intersect($iterable): From {

        return new From(new IteratorFactory(function () use ($iterable) {

            $array1 = iterator_to_array($this);
            $array2 = is_array($iterable) ? $iterable : iterator_to_array($iterable);

            foreach (array_intersect($array1, $array2) as $item)
                yield $item;

        }));
    }

    /**
     * @param $obj
     * @param mixed|null $key
     * @return From
     */
    public function prepend($obj, $key = null): From {
        return new From(new IteratorFactory(function () use ($key, $obj) {
            if (is_null($key)) {
                yield $obj;
            } else {
                yield $key => $obj;
            }
            foreach ($this->getIterator() as $k => $v)
                yield $k => $v;

        }));
    }

    /**
     * @param $n
     * @return From
     */
    public function skip($n): From {
        return new From(new IteratorFactory(function () use ($n) {

            $skipped = 0;
            foreach ($this->getIterator() as $k => $v) {
                if ($skipped < $n) {
                    $skipped++;
                } else {
                    yield $k => $v;
                }
            }


        }));

    }

    /**
     * @param iterable $iterable
     * @param callable $on
     * @param callable|null $selector
     * @return From
     */
    public function join(iterable $iterable, callable $on, callable $selector = null): From {
        $selector = $selector ?: function ($a, $b) {
            return [$a, $b];
        };

        return new From(new IteratorFactory(function () use ($selector, $on, $iterable) {

            foreach ($this->getIterator() as $item1) {
                foreach ($iterable as $item2) {
                    if (call_user_func($on, $item1, $item2)) {
                        yield call_user_func($selector, $item1, $item2);
                    }
                }
            }

        }));
    }

    /**
     * @param $iterable
     * @param callable $on
     * @param callable|null $selector
     * @return From
     */
    public function rightJoin($iterable, callable $on, callable $selector = null): From {
        return (new From($iterable))->leftJoin($this, $on, $selector);
    }

    /**
     * @param $iterable
     * @param callable $on
     * @param callable|null $selector
     * @return From
     */
    public function leftJoin($iterable, callable $on, callable $selector = null): From {
        $selector = $selector ?: function ($a, $b) {
            return [$a, $b];
        };

        return new From(new IteratorFactory(function () use ($selector, $on, $iterable) {

            foreach ($this->getIterator() as $item1) {
                $matched = false;
                foreach ($iterable as $item2) {
                    if (call_user_func($on, $item1, $item2)) {
                        yield call_user_func($selector, $item1, $item2);
                        $matched = true;
                    }
                }
                if (!$matched) {
                    yield call_user_func($selector, $item1, null);
                }
            }

        }));
    }

}

