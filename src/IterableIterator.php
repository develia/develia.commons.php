<?php

namespace Develia;

class IterableIterator implements \Iterator {

    /**
     * @var \ArrayIterator|\IteratorIterator
     */
    private $iterator;

    function __construct($iterable) {
        $this->iterator = is_array($iterable) ? new \ArrayIterator($iterable) : new \IteratorIterator($iterable);
    }

    /** @noinspection PhpLanguageLevelInspection */
    #[\ReturnTypeWillChange]
    public function current() {
        return $this->iterator->current();
    }

    public function next(): void {
        $this->iterator->next();
    }

    /** @noinspection PhpLanguageLevelInspection */
    #[\ReturnTypeWillChange]
    public function key() {
        return $this->iterator->key();
    }

    public function valid(): bool {
        return $this->iterator->valid();
    }

    public function rewind(): void {
        $this->iterator->rewind();
    }

}