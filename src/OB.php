<?php

namespace Develia;

class OB {

    private function __construct() {
    }

    public static function cleanAll() {
        while (self::getLevel()) {
            self::endClean();
        }
    }

    /**
     * @return int
     */
    public static function getLevel(): int {
        return ob_get_level();
    }

    /**
     * @return void
     */
    public static function endClean() {
        ob_end_clean();
    }

    public static function flushAll() {
        while (self::getLevel()) {
            self::endFlush();
        }
    }

    /**
     * @return void
     */
    public static function endFlush() {
        ob_end_flush();
    }

    /**
     * @return false|string
     */
    public static function getFlush() {
        return ob_get_flush();
    }

    /**
     * @return false|string
     */
    public static function getClean() {
        return ob_get_clean();
    }

    /**
     * @param $callback
     * @param int $chunk_size
     * @param int $flags
     * @return bool
     */
    public function start($callback = null, int $chunk_size = 0, int $flags = PHP_OUTPUT_HANDLER_STDFLAGS): bool {
        return ob_start($callback, $chunk_size, $flags);
    }

}