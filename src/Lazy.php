<?php

namespace Develia;

/**
 * @template T
 */
class Lazy {

    /**
     * @var callable(): T
     */
    private $getter;

    /**
     * @var boolean
     */
    private bool $_hasValue;

    /**
     * @var T
     */
    private $value;

    /**
     * @param callable(): T $getter
     */
    public function __construct(callable $getter) {

        $this->getter = $getter;
        $this->_hasValue = false;
    }

    public function hasValue(): bool {
        return $this->_hasValue;
    }

    /**
     * @return T
     */
    public function getValue() {

        if (!$this->_hasValue) {
            $this->value = call_user_func($this->getter);
            $this->_hasValue = true;
        }

        return $this->value;

    }

}