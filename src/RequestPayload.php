<?php

namespace Develia;

class RequestPayload {

    /** @noinspection PhpMissingFieldTypeInspection */
    private $params;

    /**
     * @param mixed $params
     */
    public function __construct(&$params) {

        $this->params = &$params;
    }


    public function __debugInfo() {
        return $this->params->__debugInfo();
    }

    /**
     * @param int | string $key
     * @param int|null $size
     * @param mixed $fallback
     * @return bool
     */
    public function tryGetSize($key, ?int &$size, $fallback = null): bool {

        if ($key) {
            if (Obj::tryGetValue($this->params, $key, $tmp, $fallback)) {
                $size = count($tmp);
                return true;
            }
        } else {
            $size = count($this->params);
        }


        $size = null;
        return false;
    }

    /**
     * @param int | string $key
     * @param mixed $output
     * @param mixed $fallback
     * @return bool
     */
    public function tryGet($key, &$output, $fallback = null): bool {
        return Obj::tryGetValue($this->params, $key, $output, $fallback);
    }

    /**
     * @param int | string $key
     * @param mixed $output
     * @param mixed $fallback
     * @return bool
     */
    public function tryGetString($key, &$output, $fallback = null): bool {

        if (Obj::tryGetValue($this->params, $key, $output)) {
            $output = strval($output);
            return true;
        }

        $output = $fallback;
        return false;
    }

    /**
     * @param int | string $key
     * @param mixed $output
     * @param string $thousands_separator
     * @param mixed $fallback
     * @return bool
     */
    public function tryGetInt($key, &$output, string $thousands_separator = ",", $fallback = null): bool {

        if (Obj::tryGetValue($this->params, $key, $tmp) &&
            Str::tryParseInt($tmp, $output, $thousands_separator)) {
            return true;
        }

        $output = $fallback;
        return false;

    }

    /**
     * @param int | string $key
     * @param mixed $output
     * @param string $decimals_separator
     * @param string $thousands_separator
     * @param mixed $fallback
     * @return bool
     */
    public function tryGetFloat($key, &$output, string $decimals_separator = ".", string $thousands_separator = ",", $fallback = null): bool {

        if (Obj::tryGetValue($this->params, $key, $tmp) &&
            Str::tryParseFloat($tmp, $output, $decimals_separator, $thousands_separator)) {
            return true;
        }

        $output = $fallback;
        return false;

    }

    /**
     * @param int | string $key
     * @param mixed $output
     * @param string $format
     * @param \DateTimeZone | string | null $timezone
     * @param bool $immutable
     * @param mixed $fallback
     * @return bool
     * @throws \DateInvalidTimeZoneException
     */
    public function tryGetDate($key, &$output, string $format = DateTimeFormat::MYSQL_DATE, $timezone = null, bool $immutable = true, $fallback = null): bool {

        if (Obj::tryGetValue($this->params, $key, $tmp) &&
            Date::tryParse($tmp, $output, $format, $timezone, $immutable)) {
            return true;
        }

        $output = $fallback;
        return false;
    }

    /**
     * @param int | string $key
     * @param mixed $output
     * @param string $format
     * @param \DateTimeZone | string | null $timezone
     * @param bool $immutable
     * @param mixed $fallback
     * @return bool
     * @throws \DateInvalidTimeZoneException
     */
    public function tryGetDateTime($key, &$output, string $format = DateTimeFormat::ATOM, $timezone = null, bool $immutable = true, $fallback = null): bool {

        if (Obj::tryGetValue($this->params, $key, $tmp) &&
            Date::tryParse($tmp, $output, $format, $timezone, $immutable)) {
            return true;
        }

        $output = $fallback;
        return false;
    }

}