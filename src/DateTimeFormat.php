<?php

namespace Develia;

use DateTimeInterface;

final class DateTimeFormat {

    const ATOM = DateTimeInterface::ATOM;
    const COOKIE = DateTimeInterface::COOKIE; // Ejemplo: 2005-08-15T15:52:01+00:00
    const ISO8601 = 'Y-m-d\TH:i:sO'; // Ejemplo: Monday, 15-Aug-05 15:52:01 UTC
    const RFC822 = DateTimeInterface::RFC822; // Ejemplo: 2005-08-15T15:52:01+0000
    const RFC850 = DateTimeInterface::RFC850; // Ejemplo: Mon, 15 Aug 05 15:52:01 +0000
    const RFC1036 = DateTimeInterface::RFC1036; // Ejemplo: Monday, 15-Aug-05 15:52:01 UTC
    const RFC1123 = DateTimeInterface::RFC1123; // Ejemplo: Mon, 15 Aug 05 15:52:01 +0000
    const RFC2822 = DateTimeInterface::RFC2822; // Ejemplo: Mon, 15 Aug 2005 15:52:01 +0000
    const RFC3339 = DateTimeInterface::RFC3339; // Ejemplo: Mon, 15 Aug 2005 15:52:01 +0000
    const RSS = DateTimeInterface::RSS; // Ejemplo: 2005-08-15T15:52:01+00:00
    const W3C = DateTimeInterface::W3C; // Ejemplo: Mon, 15 Aug 2005 15:52:01 +0000
    const RFC5322 = 'D, d M Y H:i:s O'; // Ejemplo: 2005-08-15T15:52:01+00:00
    const RFC7231 = DateTimeInterface::RFC7231; // Ejemplo: Mon, 15 Aug 2005 15:52:01 +0000
    const RFC3339_EXTENDED = DateTimeInterface::RFC3339_EXTENDED; // Ejemplo: Mon, 15 Aug 2005 15:52:01 GMT
    const RFC3339_FRACTION = 'Y-m-d\TH:i:s.v'; // Ejemplo: 2005-08-15T15:52:01.000+00:00

    const MYSQL_DATE = 'Y-m-d'; // Formato de fecha para MySQL. Ejemplo: 2005-08-15
    const MYSQL_TIME = 'H:i:s'; // Formato de tiempo para MySQL. Ejemplo: 15:52:01
    const MYSQL_DATETIME = 'Y-m-d H:i:s'; // Formato de fecha y hora para MySQL. Ejemplo: 2005-08-15 15:52:01
    const UNIX_TIMESTAMP = 'U'; // Timestamp UNIX. Ejemplo: 1124212321

    private function __construct() { }

}