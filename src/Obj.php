<?php


namespace Develia;

final class Obj {

    /**
     * Retrieves object class
     * @param object | class-string $obj_or_class
     * @param bool $recursive
     * @return array|false|string[]
     */
    public static function getTraits($obj_or_class, bool $recursive = false) {
        $traits = class_uses($obj_or_class);

        if ($recursive) {
            // Get the parent class
            $parent = get_parent_class($obj_or_class);

            while ($parent) {
                // Get traits of the parent class
                $parentTraits = class_uses($parent);

                // Merge parent traits
                $traits = array_merge($traits, $parentTraits);

                // Move up in the hierarchy
                $parent = get_parent_class($parent);
            }
        }

        return $traits;
    }

    /**
     * Compares two values.
     *
     * @param mixed $a The first value to compare.
     * @param mixed $b The second value to compare.
     * @return int Returns -1 if $a is less than $b, 1 if $a is greater than $b, or 0 if they are equal.
     */
    public static function compare($a, $b): int {

        if ($a < $b)
            return -1;

        if ($a > $b)
            return 1;

        return 0;
    }

    /**
     * @param object | array | class-string $assignee
     * @param mixed $assigned
     * @return void
     * @throws \Exception
     */
    public static function setValues(&$assignee, $assigned) {
        if (is_object($assigned))
            $assigned = Obj::getValues($assigned);

        foreach ($assigned as $k => $v) {
            Obj::setValue($assignee, $k, $v);
        }
    }

    /**
     * @param object | array | class-string $obj
     */
    public static function getValues($obj): array {
        if (is_array($obj))
            return $obj;

        if (is_object($obj)) {
            return get_object_vars($obj);
        }


        if (is_string($obj) && class_exists($obj)) {

            $reflector = new \ReflectionClass($obj);
            $properties = $reflector->getProperties(\ReflectionProperty::IS_PUBLIC);

            $vars = [];
            foreach ($properties as $property) {
                $vars[$property->getName()] = $property->getValue();
            }
            return $vars;
        }

        throw new \InvalidArgumentException();
    }

    /**
     * @param array | class-string | object $obj
     * @param scalar $attr
     * @param mixed $default
     * @return mixed
     * @throws \Exception
     */
    public static function getValue($obj, $attr, $default = null) {
        Obj::tryGetValue($obj, $attr, $output, $default);
        return $output;
    }

    /**
     * @param array | class-string | object $obj
     * @param scalar $attr
     * @param mixed $output
     * @param mixed $default
     * @return bool
     */
    public static function tryGetValue($obj, $attr, &$output, $default = null): bool {

        if (is_array($obj)) {
            if (array_key_exists($attr, $obj)) {
                $output = $obj[$attr];
                return true;
            }
            $output = $default;
            return false;
        }

        $isObject = is_object($obj);
        if (is_string($obj) && class_exists($obj) || $isObject) {

            if (method_exists($obj, $attr)) {
                $output = call_user_func([$obj, $attr]);
                return true;
            }

            $method = "get" . ucfirst($attr);
            if (method_exists($obj, $method)) {
                $output = call_user_func([$obj, $method]);
                return true;
            }

            $method = "get_" . $attr;
            if (method_exists($obj, $method)) {
                $output = call_user_func([$obj, $method]);
                return true;
            }

            $method = "is" . ucfirst($attr);
            if (method_exists($obj, $method)) {
                $output = call_user_func([$obj, $method]);
                return true;
            }

            $method = "is_" . $attr;
            if (method_exists($obj, $method)) {
                $output = call_user_func([$obj, $method]);
                return true;
            }

            if (property_exists($obj, $attr)) {
                $output = $isObject ? $obj->$attr : $obj::$attr;
                return true;
            }

        }


        $output = $default;
        return false;

    }

    /**
     * @param array | class-string | object $obj
     * @param scalar $attr
     * @param mixed $value
     * @return void
     * @throws \Exception
     */
    public static function setValue(&$obj, $attr, $value) {
        Obj::trySetValue($obj, $attr, $value);
    }

    /**
     * @param array | class-string | object $obj
     * @param scalar $attr
     * @param mixed $value
     * @return bool
     */

    public static function trySetValue(&$obj, $attr, $value): bool {

        if (is_array($obj)) {
            $obj[$attr] = $value;
            return true;
        }

        $isObject = is_object($obj);
        if (is_string($obj) && class_exists($obj) || $isObject) {

            if (method_exists($obj, $attr)) {
                call_user_func([$obj, $attr], $value);
                return true;
            }

            $method = "set" . ucfirst($attr);
            if (method_exists($obj, $method)) {
                call_user_func([$obj, $method], $value);
                return true;
            }

            $method = "set_" . $attr;
            if (method_exists($obj, $method)) {
                call_user_func([$obj, $method], $value);
                return true;
            }

            $method = "is" . ucfirst($attr);
            if (method_exists($obj, $method)) {
                call_user_func([$obj, $method], $value);
                return true;
            }

            $method = "is_" . $attr;
            if (method_exists($obj, $method)) {
                call_user_func([$obj, $method], $value);
                return true;
            }

            if (property_exists($obj, $attr)) {
                if ($isObject) {
                    $obj->$attr = $value;
                } else {
                    $obj::$attr = $value;
                }
                return true;
            }
        }

        return false;

    }

    /**
     * Checks if the given value is null, an empty array, or an empty string.
     *
     * - Returns true if the given value is null.
     * - Returns true if the given value is an array with no elements.
     * - Returns true if the given value is an empty string.
     * - Returns false for all other values.
     *
     * @param mixed $obj The value to check.
     *
     * @return bool True if the value is null, an empty array, or an empty string, otherwise false.
     */
    public static function isNullOrEmpty($obj): bool {

        if (is_null($obj))
            return true;

        if (is_array($obj))
            return count($obj) == 0;

        if (is_string($obj))
            return $obj == '';


        return false;
    }

    /**
     * @param array $iterable
     * @return \stdClass
     */
    public static function fromArray(array $iterable): \stdClass {
        $output = new \stdClass();

        foreach ($iterable as $k => $v) {
            if (Str::isValidPHPPropertyName($k))
                $output->$k = $v;
        }

        return $output;
    }

    /**
     * @throws \ReflectionException
     */
    public static function isGenerator($function): bool {
        if (!is_callable($function))
            return false;

        $reflection = new \ReflectionFunction($function);
        return $reflection->isGenerator();

    }

    public static function merge(array $objects): \stdClass {
        $output = new \stdClass();
        foreach ($objects as $object) {
            foreach ($object as $property => $value) {
                $output->$property = $value;
            }
        }
        return $output;
    }

    /**
     * @param $obj
     * @return bool
     */
    public static function isArray($obj): bool {
        return is_array($obj);
    }

    /**
     * @param string|mixed $objOrClass
     * @param string $class
     * @return bool
     */
    public static function isSubclassOf($objOrClass, string $class): bool {
        return is_subclass_of($objOrClass, $class);
    }

    /**
     * @param $obj
     * @return bool
     */
    public static function isScalar($obj): bool {
        return is_scalar($obj);
    }

    /**
     * @param $obj
     * @return bool
     */
    public static function toBoolean($obj): bool {
        return boolval($obj);
    }

    /**
     * @param $obj
     * @return string
     */
    public static function toString($obj): string {
        return strval($obj);
    }

    /**
     * @param $obj
     * @return int
     */
    public static function toInt($obj): int {
        return intval($obj);
    }

    /**
     * @param $obj
     * @return float
     */
    public static function toFloat($obj): float {
        return floatval($obj);
    }

    /**
     * Determines if the given array is an indexed array.
     *
     * @param mixed $obj The value to be checked if it is an indexed array.
     * @return bool|null Returns true if the array is indexed, false if it is not indexed,
     *                   and null for an empty array. If the input is not an array, returns false.
     */
    public static function isIndexedArray($obj): ?bool {
        if (!is_array($obj))
            return false;

        if (count($obj) == 0)
            return null;

        $i = 0;
        foreach ($obj as $k => $v) {
            if ($k !== $i)
                return false;

            $i++;
        }
        return true;
    }


    /**
     * Determines whether the given array is an associative array.
     *
     * An associative array is defined as an array where at least one key
     * does not correspond to a sequential integer index starting from 0.
     *
     * @param mixed $obj The variable to be checked, expected to be an array.
     * @return bool|null Returns true if the array is associative, false if
     *                   it is a sequential array, or null if the array is empty.
     */
    public static function isAssociativeArray($obj): ?bool {
        if (!is_array($obj))
            return false;

        if (count($obj) == 0)
            return null;

        $i = 0;
        foreach ($obj as $k => $v) {
            if ($k !== $i)
                return true;
            $i++;
        }
        return false;
    }

    /**
     * @param $obj
     * @return bool
     */
    public static function isString($obj): bool {
        return is_string($obj);
    }

    /**
     * @param $obj
     * @return bool
     */
    public static function isBoolean($obj): bool {
        return is_bool($obj);
    }

    /**
     * @template T
     * @param T $obj
     * @return T[]
     */
    public static function repeat($obj, $times): array {
        $output = [];
        for ($i = 0; $i < $times; $i++)
            $output[] = $obj;
        return $output;
    }

    /**
     * Checks if the given value is an integer.
     *
     * @param mixed $obj The value to be checked.
     * @return bool Returns true if the value is an integer, otherwise false.
     */
    public static function isInteger($obj): bool {
        return is_integer($obj);
    }

    /**
     * Checks if a given object is iterable.
     *
     * @param mixed $obj The object to check.
     * @return bool Returns true if the object is an array or implements the Traversable interface, otherwise false.
     */
    public static function isIterable($obj): bool {
        return is_array($obj) || ($obj instanceof \Traversable);
    }

    /**
     * Swaps the values of the two provided variables.
     *
     * @param mixed $a The first variable to swap.
     * @param mixed $b The second variable to swap.
     * @return void
     */
    public static function swap(&$a, &$b) {

        $c = $a;
        $a = $b;
        $b = $c;
    }

    /**
     * @param $obj
     * @return bool
     */
    public static function isFloat($obj): bool {
        return is_float($obj);
    }

    /**
     * @param $obj
     * @return bool
     */
    public static function isLong($obj): bool {
        return is_long($obj);
    }

    /**
     * @param $obj
     * @return bool
     */
    public static function isDouble($obj): bool {
        return is_double($obj);
    }

    /**
     * @param $obj
     * @return bool
     */
    public static function isNumeric($obj): bool {
        return is_numeric($obj);
    }


    /**
     * @param $obj
     * @return bool
     */
    public static function isResource($obj): bool {
        return is_resource($obj);
    }

    /**
     * @param $obj
     * @return bool
     */
    public static function isNaN($obj): bool {
        return is_nan($obj);
    }

    /**
     * @param $obj
     * @return string
     */
    public static function getClass($obj): string {
        return get_class($obj);
    }

    /**
     * @param $obj
     * @return bool
     */
    public static function isEmpty($obj): bool {
        return empty($obj);
    }

    /**
     * @param $obj
     * @return bool
     */
    public static function isNotEmpty($obj): bool {
        return !empty($obj);
    }

    /**
     * @param $obj
     * @return bool
     */
    public static function isNull($obj): bool {
        return is_null($obj);
    }

    /**
     * @param $obj
     * @return bool
     */
    public static function isNotNull($obj): bool {
        return !is_null($obj);
    }

    /**
     * @param $obj
     * @return bool
     */
    public static function isCallable($obj): bool {
        return is_callable($obj);

    }

    /**
     * @param $obj
     * @return bool
     */
    public static function isClosure($obj): bool {
        return $obj instanceof \Closure;
    }

    /**
     * @param $objectOrClass
     * @param $type
     * @return bool
     */
    public static function isA($objectOrClass, $type): bool {
        return is_a($objectOrClass, $type);
    }

    /**
     * @param $obj_or_class
     * @return false|string
     */
    public static function getParentClass($obj_or_class) {
        return get_parent_class($obj_or_class);
    }

    /**
     * @param $obj
     * @return bool
     */
    public static function isObject($obj): bool {
        return is_object($obj);
    }

    /**
     * @param object | object[] $obj
     * @param boolean $recursive
     * @return array
     */
    public static function toArray($obj, bool $recursive = true): array {
        return $recursive ? Obj::_toArrayRecursive($obj) : Obj::_toArrayNonRecursive($obj);
    }

    /**
     * @param mixed $obj
     * @return mixed
     */
    private static function _toArrayRecursive($obj) {
        if (is_null($obj) || is_scalar($obj))
            return $obj;

        return is_object($obj) ? get_object_vars($obj) : array_map([self::class, "_toArrayRecursive"], $obj);
    }

    /**
     * @param object|object[] $obj
     * @return array
     */
    private static function _toArrayNonRecursive($obj): array {
        return is_object($obj) ? get_object_vars($obj) : array_map("get_object_vars", $obj);
    }

    /**
     * @param object|class-string $object_or_class
     * @param bool $recursive
     * @return array|false|string[]
     */
    function getInterfaces($object_or_class, bool $recursive = false) {
        $interfaces = class_implements($object_or_class);

        if ($recursive) {
            // Get the parent class
            $parent = get_parent_class($object_or_class);

            while ($parent) {
                // Get interfaces of the parent class
                $parentInterfaces = class_implements($parent);

                // Merge parent interfaces
                $interfaces = array_merge($interfaces, $parentInterfaces);

                // Move up in the hierarchy
                $parent = get_parent_class($parent);
            }
        }

        return $interfaces;
    }


}