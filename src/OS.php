<?php

namespace Develia;

use Develia\IO\LocalDirectory;

class OS {

    private function __construct() {
    }

    /**
     * Executes a callable using a temporary file.
     *
     * @param callable $callable The callable function to be executed.
     * @param string|null $file (optional) The path to the temporary file. If null, a temp file will be created.
     * @return mixed The result of the callable function execution.
     */
    public static function usingTempFile(callable $callable, ?string $file = null) {
        if (is_null($file)) {
            $file = tempnam(sys_get_temp_dir(), "tmp");
        }

        try {
            return call_user_func($callable, $file);
        } finally {

            if (is_file($file))
                unlink($file);
        }
    }

    /**
     * Executes a callable function using a temporary directory.
     *
     * @param callable $callable The function to be executed.
     * @param string|null $dir Optional. The path of the temporary directory. If not provided, a random temporary directory will be created.
     * @return mixed The result returned by the callable function.
     *
     * @throws \RuntimeException if the temporary directory cannot be created.
     */
    public static function usingTempDirectory(callable $callable, ?string $dir = null) {
        if (is_null($dir)) {
            $dir = tempnam(sys_get_temp_dir(), "tmp");
        }

        mkdir($dir);

        try {
            return call_user_func($callable, $dir);
        } finally {

            if (is_dir($dir))
                (new LocalDirectory($dir))->delete();
        }
    }

    /**
     * Gets the system's temporary directory.
     *
     * @return string The path to the temporary directory.
     */
    public static function getTempDirectory(): string {
        return sys_get_temp_dir();
    }

    /**
     * Returns an array of system fonts based on the current OS.
     *
     * @return array An array of strings, each representing the path to a font file.
     */
    public static function getSystemFonts(): array {
        $output = [];
        foreach (OS::getSystemFontsDirectories() as $dir) {
            $dir = new LocalDirectory($dir);
            foreach ($dir->getFiles() as $file)
                $output[] = $file;
        }
        return $output;
    }

    /**
     * Returns the system fonts directory or directories based on the current OS.
     *
     * @return array An array of strings, each representing a path to a system font directory.
     */
    public static function getSystemFontsDirectories(): array {

        if (self::isWindows()) {
            // Utilizar la variable de entorno SystemRoot para encontrar la ruta del sistema
            $systemRoot = getenv('SystemRoot');
            if ($systemRoot) {
                return [$systemRoot . '\\Fonts'];
            } else {
                // Si por alguna razón la variable de entorno no está disponible
                return ['C:\\Windows\\Fonts'];
            }
        } elseif (self::isLinux()) {
            // Rutas típicas de fuentes en Linux
            return ['/usr/share/fonts', '/usr/local/share/fonts', '~/.fonts'];
        } elseif (self::isMacOS()) {
            // Rutas típicas de fuentes en macOS
            return ['/System/Library/Fonts', '/Library/Fonts', '~/Library/Fonts'];
        } else {
            return []; // Sistema operativo desconocido
        }
    }

    /**
     * Checks if the operating system is Windows.
     *
     * @return bool True if the OS is Windows, False otherwise.
     */
    public static function isWindows(): bool {
        return strtoupper(substr(PHP_OS, 0, 3)) === 'WIN';
    }

    /**
     * Checks if the operating system is Linux.
     *
     * @return bool True if the OS is Linux, False otherwise.
     */
    public static function isLinux(): bool {
        return strtoupper(substr(PHP_OS, 0, 5)) === 'LINUX';
    }

    /**
     * Checks if the operating system is macOS.
     *
     * @return bool True if the OS is macOS, False otherwise.
     */
    public static function isMacOS(): bool {
        return strtoupper(substr(PHP_OS, 0, 6)) === 'DARWIN';
    }

}