<?php

namespace Develia;

/**
 * Class IteratorFactory
 *
 * IteratorFactory is a class for creating reusable iterables from a generator function.
 * It implements the IteratorAggregate interface, allowing the generator function to be invoked
 * each time the object needs to be iterated, therefore providing a fresh iterator for each iteration.
 *
 */
class IteratorFactory implements \IteratorAggregate {

    /**
     * @var callable The generator function that will be used to create a new iterator.
     */
    private $fn;

    /**
     * IteratorFactory constructor.
     *
     * @param callable():\Iterator $generator_fn A generator function that returns an iterator when called.
     */
    public function __construct(callable $generator_fn) {
        $this->fn = $generator_fn;
    }

    /**
     * Get an iterator from the generator function.
     * This method is part of the IteratorAggregate interface and is called whenever an iterator is needed.
     *
     * @return \Traversable A fresh iterator from the generator function.
     */
    public function getIterator(): \Traversable {
        return call_user_func($this->fn);
    }

}
