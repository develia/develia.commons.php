<?php

namespace Develia;

class UrlBuilder {

    /**
     * @var string
     */
    protected $host;

    /**
     * @var null|string
     */
    protected $user;

    /**
     * @var null|string
     */
    protected $path;

    /**
     * @var null|int
     */
    protected $port;

    /**
     * @var string
     */
    private $scheme;

    /**
     * @var string
     */
    private $fragment;

    /**
     * @var null|array
     */
    private ?array $query;

    /**
     * @var null|string
     */
    private $pass;

    /**
     * @param string|null $url
     */
    public function __construct(?string $url = null) {
        if ($url) {
            $parts = parse_url($url);

            if (isset($parts["scheme"]))
                $this->scheme = $parts["scheme"];

            if (isset($parts["host"]))
                $this->host = $parts["host"];

            if (isset($parts["port"]))
                $this->port = $parts["port"];

            if (isset($parts["user"]))
                $this->user = $parts["user"];

            if (isset($parts["pass"]))
                $this->pass = $parts["pass"];

            if (isset($parts["path"]))
                $this->path = $parts["path"];

            if (isset($parts["query"]))
                parse_str($parts["query"], $this->query);

            if (isset($parts["fragment"]))
                $this->fragment = $parts["fragment"];


        }
    }


    public function build(): string {

        $url = '';

        if ($this->scheme)
            $url .= "$this->scheme://";

        if ($this->user) {
            $url .= $this->user;

            if ($this->pass)
                $url .= ":$this->pass";

            $url .= "@";

        } else if ($this->pass)
            $url .= ":$this->pass@";

        if ($this->host)
            $url .= $this->host;

        if ($this->port)
            $url .= ":$this->port";

        if ($this->path)
            $url .= "/" . ltrim($this->path, "/");

        if ($this->query) {
            $url .= "?" . http_build_query($this->query);
        }

        if ($this->fragment)
            $url .= "#$this->fragment";

        return $url;
    }

    /**
     * @return string|null
     */
    public function getPath(): ?string {
        return $this->path;
    }

    /**
     * @param string|null $path
     * @return UrlBuilder
     */
    public function setPath(?string $path): UrlBuilder {
        $this->path = $path;
        return $this;
    }

    /**
     * @return null|array
     */
    public function getQuery(): ?array {
        return $this->query;
    }

    /**
     * @param array|null $query
     * @return UrlBuilder
     */
    public function setQuery(?array $query): UrlBuilder {
        $this->query = $query;
        return $this;
    }

    /**
     * @return null|string
     */
    public function getFragment(): ?string {
        return $this->fragment;
    }

    /**
     * @param string $fragment
     * @return UrlBuilder
     */
    public function setFragment(string $fragment): UrlBuilder {
        $this->fragment = $fragment;
        return $this;
    }

    /**
     * @return string
     */
    public function getScheme(): string {
        return $this->scheme;
    }

    /**
     * @param string $scheme
     * @return UrlBuilder
     */
    public function setScheme(string $scheme): UrlBuilder {
        $this->scheme = $scheme;
        return $this;
    }

    /**
     * @return null|string
     */
    public function getUser(): ?string {
        return $this->user;
    }

    /**
     * @param string|null $user
     * @return UrlBuilder
     */
    public function setUser(?string $user): UrlBuilder {
        $this->user = $user;
        return $this;
    }

    /**
     * @return null|int
     */
    public function getPort(): ?int {
        return $this->port;
    }

    /**
     * @param int|null $port
     * @return UrlBuilder
     */
    public function setPort(?int $port): UrlBuilder {
        $this->port = $port;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getPass(): ?string {
        return $this->pass;
    }

    /**
     * @param string|null $pass
     * @return UrlBuilder
     */
    public function setPass(?string $pass): UrlBuilder {
        $this->pass = $pass;
        return $this;
    }

    /**
     * @return string
     */
    public function getHost(): string {
        return $this->host;
    }

    /**
     * @param string $host
     * @return UrlBuilder
     */
    public function setHost(string $host): UrlBuilder {
        $this->host = $host;
        return $this;
    }

    /**
     * @param string $key
     * @param string $value
     * @return void
     */
    public function setQueryParameter(string $key, string $value) {
        if (!$this->query)
            $this->query = [];

        $this->query[$key] = $value;
    }

    /**
     * @param string $key
     * @return string|null
     */
    public function getQueryParameter(string $key): ?string {
        if (!$this->query || !isset($this->query[$key]))
            return null;

        return $this->query[$key];
    }

    /**
     * @param string $key
     * @param $output
     * @return boolean
     */
    public function tryGetQueryParameter(string $key, &$output): bool {
        if (!$this->query || !isset($this->query[$key]))
            return false;

        $output = $this->query[$key];
        return true;
    }

    /**
     * @param string $key
     * @return bool
     */
    public function removeQueryParameter(string $key): bool {
        $removed = $this->query && isset($this->query[$key]);
        if ($removed)
            unset($this->query[$key]);

        return $removed;
    }

}