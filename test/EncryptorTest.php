<?php

use PHPUnit\Framework\TestCase;
use function Develia\autoload;

require_once "../develia.php";
autoload("Develia", __DIR__ . "/../src");

class EncryptorTest extends TestCase
{
    public function test1()
    {

        $encryptor = new \Develia\Encryptor("xDD", "xDDDD", 'bf-cbc', OPENSSL_RAW_DATA);

        $pack = pack("N", 644);

        $a = $encryptor->encrypt($pack);
        $b = $encryptor->decrypt($a);


        $value = 5663;
        $packed = pack("N", 5663);
        $encoded = \Develia\base32_encode($packed);
        $decoded = \Develia\base32_decode($encoded);
        $unpacked = unpack("N", $decoded)[1];
    }
}