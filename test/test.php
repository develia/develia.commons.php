<?php


require_once "../src/autoload.php";


class X
{
    function __destruct()
    {
        echo "Destruido";
    }
}


{
    $temp = new X();
    $temp = null;
}
echo "Sali del ambito";

exit;