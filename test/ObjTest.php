<?php

use Develia\Obj;
use PHPUnit\Framework\TestCase;
use function Develia\autoload;

require_once "../develia.php";
autoload("Develia", __DIR__ . "/../src");

class ObjTest extends TestCase
{
    public function testArrayConversion()
    {

        $obj1 = ["g" => "h", "i" => ["j", "k"]];
        $obj2 = Obj::fromArray(["a" => "b", "c" => ["d", "e"], "f" => Obj::fromArray($obj1)]);

        $expected = ["a" => "b", "c" => ["d", "e"], "f" => ["g" => "h", "i" => ["j", "k"]]];
        $actual = Obj::toArray($obj2);

        $this->assertEquals($expected, $actual);

    }
}