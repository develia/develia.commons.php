<?php


use Develia\From;
use PHPUnit\Framework\TestCase;
use function Develia\autoload;

require_once "../src/autoload.php";
autoload("Develia", __DIR__ . "/../src");

class FromTest extends TestCase
{


    public function testGroupBy()
    {
        $array = [[
            "grupo" => [1],
            "valor" => "11"
        ],
            [
                "grupo" => [1],
                "valor" => "12"
            ],
            ["grupo" => [2],
                "valor" => "21"
            ],
            ["grupo" => [2],
                "valor" => "22",
            ]];

        $grouping = (new From($array))->groupBy(function ($x) {
            return ["grupo" => $x["grupo"]];
        }, function (From $x) {
            return $x->toVector();
        });

        $keys = $grouping->keys()->toVector();
        $values = $grouping->values()->toVector();

        $this->assertEquals([["grupo" => [1]], ["grupo" => [2]]], $keys);
        $this->assertEquals([[[
            "grupo" => [1],
            "valor" => "11"
        ],
            [
                "grupo" => [1],
                "valor" => "12"
            ]],
            [["grupo" => [2],
                "valor" => "21"
            ],
                ["grupo" => [2],
                    "valor" => "22",
                ]]], $values);


        $this->assertEquals((new From($array))->groupBy(function ($x) {
            return $x["grupo"];
        }, function ($x) {
            return $x->sum(function ($y) {
                return $y["valor"];
            });
        })->toVector(),
            [23, 43]);


    }

    public function testTranspose()
    {
        $array = new From([
            [1, 2, 3],
            [4, 5, 6]
        ]);;
        $this->assertEquals([
            [1, 4],
            [2, 5],
            [3, 6]
        ], $array->transpose()->toArray());

        $array2 = new From([
            "pepe" => [
                "libros" => [1, 2, 3],
                "perros" => [6, 7, 8],
            ],
            "luis" => [
                "libros" => [3, 4, 5],
                "perros" => [9, 10, 11],
            ]
        ]);;

        $toArray = $array2->transpose()->toArray();
        $this->assertEquals([
            "libros" => [
                "pepe" => [1, 2, 3],
                "luis" => [3, 4, 5]
            ],
            "perros" => [
                "pepe" => [6, 7, 8],
                "luis" => [9, 10, 11]
            ],
        ], $toArray);
    }

    public function testMap()
    {
        $array = [1, 2, 3, 4];
        $from = new From($array);
        $from1 = $from->map(function ($x) {
            return $x * 2;
        });
        $this->assertEquals([2, 4, 6, 8], $from1->toArray());

    }

    public function testUnion()
    {
        $array = [1, 2, 3, 4];
        $from = new From($array);
        $from1 = $from->union([1, 2, 3, 4]);
        $this->assertEquals([1, 2, 3, 4, 1, 2, 3, 4], $from1->toArray());
    }

    public function testAverage()
    {
        $array = [1, 2];
        $from = new From($array);
        $this->assertEquals(1.5, $from->average());
    }


    public function testSkip()
    {
        $array = [1, 2, 3, 4];
        $from = new From($array);
        $from1 = $from->skip(2);
        $this->assertEquals([3, 4], $from1->toVector());
    }


    public function testIntersect()
    {
        $array = [1, 2, 3, 4];
        $from = new From($array);
        $from1 = $from->intersect([4, 3]);
        $this->assertEquals([3, 4], $from1->toArray());
    }


    public function testFilter()
    {
        $array = [1, 2, 3, 4];
        $from = new From($array);
        $from2 = $from->filter(function ($k, $x) {
            return $x > 2;
        });
        $this->assertEquals([3, 4], $from2->toVector());

        $from1 = $from->filter(function ($k, $x, $i) {
            return $x > 2;
        });
        $this->assertEquals([3, 4], $from1->toVector());

    }


    public function testOrderBy()
    {
        $array = [1, 3, 2, 4];
        $from = new From($array);
        $from2 = $from->orderBy(function ($x) {
            return $x;
        }, "desc");
        $this->assertEquals([4, 3, 2, 1], $from2->toArray());

        $from1 = $from->orderBy(function ($x) {
            return $x;
        }, "asc");
        $this->assertEquals([1, 2, 3, 4], $from1->toArray());


        $array = ["x" => 0, "b" => 7, "c" => 3, "a" => 9];

        $from = new From($array);
        $from2 = $from->orderKeysBy(function ($k) {
            return $k;
        }, "desc");

        $this->assertEquals(["x" => 0, "c" => 3, "b" => 7, "a" => 9], $from2->toArray());

        $from1 = $from->orderKeysBy(function ($k) {
            return $k;
        }, "asc");

        $this->assertEquals(["a" => 9, "b" => 7, "c" => 3, "x" => 0], $from1->toArray());
    }

    public function testAny()
    {
        $array = [1, 2, 3, 4];
        $from = new From($array);

        $this->assertEquals(true, $from->any());
        $this->assertEquals(false,
            $from->any(function ($x) {
                return $x > 4;
            }));
    }

    public function testContainsAny()
    {
        $array = [1, 2, 3, 4];
        $from = new From($array);
        $this->assertEquals(true, $from->containsAny([3, 5]));
        $this->assertEquals(false, $from->containsAny([-3, 5]));

    }


    public function testMapMany()
    {
        $vector = from([[1, 2, 3], [2, 3, 4]]);

        $this->assertEquals([
            0 => 1,
            1 => 2,
            2 => 3,
            3 => 2,
            4 => 3,
            5 => 4
        ], $vector->mapMany(function ($x) {
            return $x;
        })->toArray());

        $associative = from([
            ["alumnos" => [
                [
                    "nombre" => "pepe",
                    "edad" => 18
                ],
                [
                    "nombre" => "juan",
                    "edad" => 21
                ]
            ]],
            ["alumnos" => [
                [
                    "nombre" => "tony",
                    "edad" => 43
                ], [
                    "nombre" => "luis",
                    "edad" => 54
                ]
            ]]
        ]);

        $this->assertEquals([[
                "nombre" => "pepe",
                "edad" => 18
            ], [
                "nombre" => "juan",
                "edad" => 21
            ], [
                "nombre" => "tony",
                "edad" => 43
            ], [
                "nombre" => "luis",
                "edad" => 54
            ]]
            , $associative->mapMany(function ($x) {
                return $x["alumnos"];
            })->toArray());


    }

    public function testKeys()
    {
        $array = [1, 2, 3, 4];
        $from = new From($array);
        $from1 = $from->keys()->toVector();
        $this->assertEquals([0, 1, 2, 3], $from1);
    }

    public function testHigest()
    {
        $array = [1, 2, 3, 4];
        $from = new From($array);
        $this->assertEquals(4,
            $from->highest(function ($x) {
                return $x * 2;
            }));
    }

    public function testContains()
    {
        $array = [1, 2, 22, 4];
        $from = new From($array);
        $this->assertEquals(true, $from->contains(22));
        $this->assertEquals(false, $from->contains(33));
    }

    public function testTake()
    {
        $array = [1, 2, 3, 4];
        $from = new From($array);
        $from1 = $from->head(2);
        $this->assertEquals([1, 2], $from1->toArray());
    }

    public function testCount()
    {
        $array = [1, 2, 3, 4];
        $from = new From($array);
        $this->assertEquals(4, $from->count());
        $this->assertEquals(2,
            $from->count(function ($x) {
                return $x > 2;
            }));
    }

    public function testToArray()
    {
        $array = [1, 2, 3, 4];
        $from = new From($array);

        $this->assertEquals([1, 2, 3, 4], $from->toArray());
    }

    public function testPrepend()
    {
        $array = [1, 2, 3, 4];
        $from = new From($array);

        $from1 = $from->prepend(5);
        $this->assertEquals([5, 1, 2, 3, 4], $from1->toVector());


        $array = ["x" => 1, "y" => 2, "z" => 3];
        $from = new From($array);

        $from1 = $from->prepend(4, "a");
        $this->assertEquals(["a" => 4, "x" => 1, "y" => 2, "z" => 3], $from1->toArray());
    }

    public function testMax()
    {
        $array = [1, 2, 3, 4];
        $from = new From($array);
        $this->assertEquals(4, $from->max());
    }

    public function testAppend()
    {
        $array = [1, 2, 3, 4];
        $from = new From($array);

        $from1 = $from->append(5);
        $this->assertEquals([1, 2, 3, 4, 5], $from1->toArray());

        $array = ["x" => 1, "y" => 2, "z" => 3];
        $from = new From($array);

        $from1 = $from->append(4, "a");
        $this->assertEquals(["a" => 4, "x" => 1, "y" => 2, "z" => 3], $from1->toArray());
    }

    public function testMin()
    {
        $array = [1, 2, 3, 4];
        $from = new From($array);
        $this->assertEquals(1, $from->min());
    }

    public function testUnique()
    {
        $array = [1, 2, 2, 3, 4];
        $from = new From($array);

        $from1 = $from->unique();
        $this->assertEquals([1, 2, 3, 4], $from1->toArray());
    }

    public function testValues()
    {
        $array = [1, 2, 3, 4];
        $from = new From($array);

        $from1 = $from->values()->toVector();
        $this->assertEquals([1, 2, 3, 4], $from1);
    }

    public function testAll()
    {
        $array = [1, 2, 3, 4];
        $from = new From($array);

        $this->assertEquals(true,
            $from->all(function ($x) {
                return $x > 0;
            }));

        $this->assertEquals(false,
            $from->all(function ($x) {
                return $x < 0;
            }));
    }

    public function testFlatten()
    {
        $array = [[1, 2], [2, 3], [3, 4], [4, 5]];
        $from = new From($array);

        $from1 = $from->flatten();
        $this->assertEquals([1, 2, 2, 3, 3, 4, 4, 5], $from1->toArray());
    }

    public function testFirst()
    {
        $array = [1, 2, 3, 4];
        $from = new From($array);

        $this->assertEquals(1, $from->first());

    }

    public function testLowest()
    {
        $array = [1, 2, 3, 4];
        $from = new From($array);

        $this->assertEquals(1,
            $from->lowest(function ($x) {
                return $x + 1;
            }));
    }

    public function testElementAt()
    {
        $array = [1, 2, 3, 4];
        $from = new From($array);
        $this->assertEquals(3, $from->valueAt(2), 3);

    }
}
