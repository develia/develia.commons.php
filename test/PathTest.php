<?php

use Develia\OS;
use Develia\Path;
use PHPUnit\Framework\TestCase;
use function Develia\autoload;

require_once "../develia.php";
autoload("Develia", __DIR__ . "/../src");

class PathTest extends TestCase
{
    public function testAbsolute()
    {
        if (!OS::isWindows()) {
            self::assertEquals("/", Path::absolute("/hola/./.."));
            self::assertEquals("/", Path::absolute("/hola/../."));

            self::assertEquals("/juan", Path::absolute("/juan/hola/./.."));
            self::assertEquals("/juan", Path::absolute("/juan/hola/../."));

            self::assertEquals("/", Path::absolute("/hola/./../"));
            self::assertEquals("/", Path::absolute("/hola/.././"));

            self::assertEquals("/juan", Path::absolute("/juan/hola/./../"));
            self::assertEquals("/juan", Path::absolute("/juan/hola/.././"));

            self::assertEquals("/pepe", Path::absolute("/hola//./../pepe"));
            self::assertEquals("/pepe", Path::absolute("/hola/.././pepe"));

            self::assertEquals("/juan/pepe", Path::absolute("/juan/hola/./../pepe"));
            self::assertEquals("/juan/pepe", Path::absolute("/juan/hola/..//./pepe"));

            self::assertEquals("/juan/pepe", Path::absolute("/juan/hola/./../pepe/"));
            self::assertEquals("/juan/pepe", Path::absolute("/juan/hola/.././pepe/"));
        } else {
            self::assertEquals("E:\\", Path::absolute("E:\\hola\\.\\.."));
            self::assertEquals("E:\\", Path::absolute("E:\\hola\\..\\."));

            self::assertEquals("E:\\juan", Path::absolute("E:\\juan\\hola\\.\\.."));
            self::assertEquals("E:\\juan", Path::absolute("E:\\juan\\hola\\..\\."));

            self::assertEquals("E:\\", Path::absolute("E:\\hola\\.\\..\\"));
            self::assertEquals("E:\\", Path::absolute("E:\\hola\\..\\.\\"));

            self::assertEquals("E:\\juan", Path::absolute("E:\\juan\\hola\\.\\..\\"));
            self::assertEquals("E:\\juan", Path::absolute("E:\\juan\\hola\\..\\.\\"));

            self::assertEquals("E:\\pepe", Path::absolute("E:\\hola\\\\.\\..\\pepe"));
            self::assertEquals("E:\\pepe", Path::absolute("E:\\hola\\..\\.\\pepe"));

            self::assertEquals("E:\\juan\\pepe", Path::absolute("E:\\juan\\hola\\.\\..\\pepe"));
            self::assertEquals("E:\\juan\\pepe", Path::absolute("E:\\juan\\hola\\..\\\\.\\pepe"));

            self::assertEquals("E:\\juan\\pepe", Path::absolute("E:\\juan\\hola\\.\\..\\pepe\\"));
            self::assertEquals("E:\\juan\\pepe", Path::absolute("E:\\juan\\hola\\..\\.\\pepe\\"));
        }
    }
}