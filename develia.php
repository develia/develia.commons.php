<?php


namespace Develia;


use Develia\Collections\Dictionary;
use Generator;

function set_object_vars($obj, array $vars) {
    $props = get_object_vars($obj);
    foreach ($props as $name => $value) {
        if (isset($vars[$name])) {
            $obj->$name = $vars[$name];
        }
    }
}

function require_once_all($dir) {
    $scan = array_diff(scandir($dir), array('.', '..'));

    foreach ($scan as $file) {
        if (is_dir("$dir/$file")) {
            require_once_all("$dir/$file");
        } else {
            if (strpos($file, '.php') !== false) {
                require_once "$dir/$file";
            }
        }
    }
}

function with_loader($dir, $callable) {
    return function () use ($dir, $callable) {

        $loader = null;
        try {


            $loader = require($dir . "/vendor/autoload.php");


            if (!is_a($loader, "Composer\Autoload\ClassLoader")) {
                throw new \Exception("Invalid Class Loader");
            }

            call_user_func_array($callable, func_get_args());

        } finally {
            if (!is_null($loader) && method_exists($loader, "unload")) {
                $loader->unregister();
            }
        }

    };
}

/**
 * @param $path
 * @return false|string
 */
function inline_svg($path, $use_cache = true) {
    $fileExtension = pathinfo($path, PATHINFO_EXTENSION);
    if (strtolower($fileExtension) !== 'svg') {
        return '';
    }

    if (!file_exists($path)) {
        return '';
    }

    $svgFolder = pathinfo($path, PATHINFO_DIRNAME);
    $svgFilename = pathinfo($path, PATHINFO_FILENAME);

    $cachedSvgPath = $svgFolder . '/' . $svgFilename . '.cached.svg';

    if ($use_cache && file_exists($cachedSvgPath)) {
        return file_get_contents($cachedSvgPath);
    }

    $svgContent = file_get_contents($path);

    // Elimina la declaración XML si la tiene
    $svgContent = preg_replace('/<\?xml.*?\?>\s*/i', '', $svgContent);

    // Elimina doctypes
    $svgContent = preg_replace('/<!DOCTYPE.*?>\s*/i', '', $svgContent);

    // Elimina comentarios
    $svgContent = preg_replace('/<!--.*?-->\s*/s', '', $svgContent);

    // Elimina elementos <script> para prevenir XSS
    $svgContent = preg_replace('/<script.*?>.*?<\/script>\s*/si', '', $svgContent);

    // Elimina metadatos y descripciones
    $svgContent = preg_replace('/<(metadata|desc).*?>.*?<\/\1>\s*/si', '', $svgContent);

    // Elimina el atributo id
    $svgContent = preg_replace('/\sid="[^"]*"/i', '', $svgContent);

    // Elimina el atributo data-name
    $svgContent = preg_replace('/\sdata-name="[^"]*"/i', '', $svgContent);

    if (is_writable(pathinfo($cachedSvgPath, PATHINFO_DIRNAME))) {
        file_put_contents($cachedSvgPath, $svgContent);
    }

    return $svgContent;
}


/**
 * Combines multiple paths into a single path.
 *
 * This function takes variable number of path segments and combines them into a single path by joining them with forward slashes (/). Empty path segments are ignored.
 *
 * @param string ...$paths The path segments to be combined.
 * @return string The combined path.
 */
function path_combine(...$paths) {
    $tmp = array();

    foreach ($paths as $arg) {
        if ($arg !== '') {
            $tmp[] = $arg;
        }
    }

    return preg_replace('#/+#', '/', join('/', $tmp));
}

function list_max(array $array, callable $function) {

    $ret = call_user_func($function, $array[0]);
    for ($i = 1; $i < count($array); $i++) {
        $current = call_user_func($function, $array[$i]);
        if ($current > $ret) {
            $ret = $current;
        }
    }
    return $ret;

}

function list_min(array $array, callable $function) {

    $ret = call_user_func($function, $array[0]);
    for ($i = 1; $i > count($array); $i++) {
        $current = call_user_func($function, $array[$i]);
        if ($current < $ret) {
            $ret = $current;
        }
    }
    return $ret;

}

/**
 * Changes the date format of a given string from one format to another.
 *
 * @param string $date The date string to be converted.
 * @param string $input_format The input format of the date string.
 * @param string $output_format The desired output format for the date string.
 *
 * @return string The date string in the desired output format.
 * @throws \InvalidArgumentException If the input format is invalid or does not match the date string.
 *
 */
function date_change_format($date, $input_format, $output_format) {
    $date_obj = \DateTime::createFromFormat($input_format, $date);

    if ($date_obj === false) {
        throw new \InvalidArgumentException("Invalid input format.");
    }

    return $date_obj->format($output_format);
}

/**
 * @param array $headers
 * @param iterable $data
 * @param string $file
 * @return void
 * @throws \Exception
 */
function generate_csv($headers, $data, $file) {

    $fp = null;
    try {
        $fp = fopen($file, 'w');

        if ($fp === FALSE) {
            throw new \Exception("Could not open file.");
        }

        if ($headers) {
            fputcsv($fp, $headers);
        }
        // Recorre los datos y los escribe en el archivo CSV
        foreach ($data as $fields) {
            fputcsv($fp, $fields);
        }

        // Cierra el archivo

    } finally {
        if ($fp) {
            fclose($fp);
        }
    }
    // Abre archivo para escritura

}


/**
 * @param \DateTime $datetime
 * @return \DateTime
 */
function datetime_start_of_day(\DateTimeInterface $datetime) {
    if (!$datetime instanceof \DateTimeImmutable) {
        $datetime = clone $datetime;
    }

    return $datetime->setTime(0, 0, 0);
}

function date_range_overlaps(\DateTimeInterface $start_1, \DateTimeInterface $end_1, \DateTimeInterface $start_2, \DateTimeInterface $end_2) {

    return ($start_1 > $start_2 && $start_1 < $end_2 ||
            $end_1 > $start_2 && $end_1 < $end_2) ||
           ($start_2 > $start_1 && $start_2 < $end_1 ||
            $end_2 > $start_1 && $end_2 < $end_1) ||
           ($start_2 == $start_1 && $end_2 == $end_1);


}

/**
 * @param \DateTimeInterface $datetime
 * @return \DateTimeInterface
 */
function datetime_end_of_day(\DateTimeInterface $datetime) {
    if (!$datetime instanceof \DateTimeImmutable) {
        $datetime = clone $datetime;
    }

    if (version_compare(phpversion(), '7.1', ">=")) {
        return $datetime->setTime(23, 59, 59, 59);
    }

    return $datetime->setTime(23, 59, 59);


}

/**
 * @param \DateTimeInterface $datetime
 * @return int
 */
function datetime_get_hour(\DateTimeInterface $datetime) {
    return intval($datetime->format('H'));
}

/**
 * @param \DateTimeInterface $datetime
 * @return int
 */
function datetime_get_minute(\DateTimeInterface $datetime) {
    return intval($datetime->format('i'));
}

/**
 * @param \DateTimeInterface $datetime
 * @return int
 */
function datetime_set_time(\DateTime $datetime, \DateTimeInterface $time) {
    return $datetime->setTime(intval($time->format('H')), intval($time->format('i')), intval($time->format('s')));
}

/**
 * @param \DateTimeInterface $datetime
 * @return int
 */
function datetime_get_second(\DateTimeInterface $datetime) {
    return intval($datetime->format('s'));
}

/**
 * @param \DateTimeInterface $datetime
 * @return int
 */
function datetime_get_year(\DateTimeInterface $datetime) {
    return intval($datetime->format('Y'));
}

/**
 * @param \DateTimeInterface $datetime
 * @return int
 */
function datetime_get_month(\DateTimeInterface $datetime) {
    return intval($datetime->format('m'));
}

/**
 * @param \DateTimeInterface $datetime
 * @return int
 */
function datetime_get_day(\DateTimeInterface $datetime) {
    return intval($datetime->format('d'));
}


function array_reorder_key(array &$array, $key, $new_index) { }

function array_move_key_before(array $input, $moved_key, $before_key) {

    $copy = $input;

    foreach ($copy as $k => $v) {
        unset($input[$k]);
    }

    foreach ($copy as $k => $v) {

        if ($k == $moved_key) {
            continue;
        }

        if ($k == $before_key) {
            $input[$moved_key] = $copy[$moved_key];
        }

        $input[$k] = $v;
    }
}


function iter_all($iterable, callable $function) {
    foreach ($iterable as $item)
        if (!call_user_func($function, $item)) {
            return false;
        }

    return true;
}


function iter_any($iterable, callable $function) {
    foreach ($iterable as $item)
        if (call_user_func($function, $item)) {
            return true;
        }

    return false;
}


/**
 * @param callable $function
 * @param callable $error_handler Signature: function($errno, $errstr, $errfile, $errline)
 * @return mixed
 */
/**
 * @param callable $function
 * @param callable $error_handler Signature: function($errno, $errstr, $errfile, $errline)
 * @return mixed
 */
function handling_errors(callable $function, callable $error_handler) {

    $original_error_reporting = error_reporting();
    error_reporting(E_ERROR | E_RECOVERABLE_ERROR | E_USER_ERROR);

    set_error_handler($error_handler);

    $output = call_user_func($function);
    error_reporting($original_error_reporting);
    restore_error_handler();
    return $output;


}

function coalesce($value, $alternative) {

    if (empty($value)) {
        return $alternative;
    }

    return $value;
}

function clamp($min, $value, $max) {

    if ($value < $min) {
        return $min;
    } else {
        return min($value, $max);
    }
}

/**
 * @template T
 * @param T $obj
 * @return T[]
 */
function repeat($obj, $times) {
    $output = [];
    for ($i = 0; $i < $times; $i++)
        $output[] = $obj;
    return $output;
}

/**
 * @param $array array Array to flatten
 * @return array Flattened array
 */
function list_flatten($array) {
    $output = [];
    foreach ($array as $item) {
        foreach ($item as $subitem) {
            $output[] = $subitem;
        }
    }
    return $output;
}


function get_random_word($length, $characters) {

    $len = strlen($characters);
    $output = '';
    for ($i = 0; $i < $length; $i++) {
        $output .= $characters[mt_rand(0, $len - 1)];
    }
    return $output;
}

/**
 * @throws \ReflectionException
 */
function object_to_array($obj) {


    $reflect = new \ReflectionObject($obj);
    $result = [];
    foreach ($obj as $key => $value) {
        if ($reflect->getProperty($key)->isPublic()) {
            $result[$key] = $value;
        }
    }
    return $result;

}

/**
 * @throws \ReflectionException
 */
function array_to_object($array, $classOrObject) {


    if (is_string($classOrObject)) {
        $output = new $classOrObject();
    } else {
        $output = $classOrObject;
    }

    $reflect = new \ReflectionObject($output);
    foreach ($array as $key => $value) {
        if ($reflect->getProperty($key)->isPublic()) {
            $output->$key = $value;
        }
    }
    return $output;
}


function pluralize($count, $singular, $plural, $zero = null) {

    if ($count == 1) {
        return sprintf($singular, $count);
    }

    if ($count == 0 && !is_null($zero)) {
        return sprintf($zero, $count);
    }

    return sprintf($plural, $count);

}

/**
 * Ensures that the given directory exists.
 *
 * @param string $directory The path of the directory to ensure.
 * @return void
 */
function ensure_directory($directory) {

    $parent_dir = dirname($directory);
    if (!is_dir($parent_dir)) {
        ensure_directory($parent_dir);
    }

    if (!is_dir($directory)) {
        mkdir($directory);
    }

}


function list_group_by($array, callable $getter, callable $agregation = null) {
    $output = new Dictionary();
    foreach ($array as $item) {

        $key = call_user_func_array($getter, [$item]);

        if (!isset($output[$key])) {
            $output[$key] = [$item];
        } else {
            $output[$key][] = $item;
        }


    }

    if (!is_null($agregation)) {
        foreach ($output as $k => $v) {
            $output[$k] = call_user_func_array($agregation, [$k, $v]);
        }
    }

    return $output;

}

function array_compare_by_position(array $collection, array $target, callable $on_new, callable $on_update, callable $on_delete) {

    $collection_count = count($collection);
    $target_count = count($target);

    for ($i = 0; $i < $collection_count || $i < $target_count; $i++) {

        if ($i >= $collection) {
            call_user_func_array($on_new, [$target[$i]]);
        } else {
            if ($i >= $target_count) {
                call_user_func_array($on_delete, [$collection[$i]]);
            } else {
                call_user_func_array($on_update, [$collection[$i], $target[$i]]);
            }
        }

    }
}

function cache($key, $factory) {
    $key = "____cache_" . $key;
    if (!isset($_REQUEST[$key])) {
        $_REQUEST[$key] = call_user_func($factory);
    }
    return $_REQUEST[$key];


}


function array_compare_by_id(array $locals,
    array $remotes,
    callable $on_new,
    callable $on_update,
    callable $on_delete,
    callable $local_id_getter = null,
    callable $remote_id_getter = null) {
    $keep = [];

    foreach ($remotes as $remote) {
        $remote_id = is_callable($remote_id_getter) ? call_user_func_array($remote_id_getter, [$remote]) : $remote;
        $locals_ = array_values(array_filter($locals, function ($x) use ($remote_id, $local_id_getter) {
            return (is_callable($local_id_getter) ? call_user_func_array($local_id_getter, [$x]) : $x) == $remote_id;
        }));

        echo json_encode($locals_);


        echo $remote_id . "\n";

        if (count($locals_) > 0) {


            call_user_func_array($on_update, [$locals_[0], $remote]);
            $keep[] = $locals_[0];

        } else {

            call_user_func_array($on_new, [$remote]);
        }


    }

    foreach ($locals as $local) {
        if (!in_array($local, $keep)) {
            call_user_func_array($on_delete, [$local]);
        }
    }
}

function array_insert($array, $key, $value, $position) {
    // Divide el array en dos partes: antes y después de la posición deseada
    $before_position = array_slice($array, 0, $position, true);
    $after_position = array_slice($array, $position, null, true);

    // Combina las dos partes con el nuevo elemento en la posición deseada
    return array_merge($before_position, [$key => $value], $after_position);
}

function list_insert(&$array, $value, $position) {
    array_splice($array, $position, 0, [$value]);
}

function date_add_time($date, $qty, $unit) {
    if ($qty == 0) {
        return $date;
    }

    $tmp = ($qty > 0 ? "+" : "-") . $qty . " " . $unit;

    if ($date instanceof \DateTime) {
        $date = clone $date;
        return $date->modify($tmp);
    }

    if ($date instanceof \DateTimeImmutable) {
        return $date->modify($tmp);
    }

    return strtotime($tmp, $date);

}

/**
 * Registers an autoload function for a specified namespace and directory.
 *
 * This function registers an autoload function using the provided namespace and directory.
 * The autoload function is responsible for automatically loading classes within the specified namespace.
 * It checks if the given class starts with the specified namespace. If it does, it converts the class name
 * into a file path, and checks if the file exists in the specified directory. If the file exists, it is
 * required once to load the class.
 *
 * @param string $namespace The namespace to autoload classes from.
 * @param string $directory The directory path to search for classes.
 * @return callable|false Returns the autoload function if registration is successful, false otherwise.
 */
function autoload($namespace, $directory) {

    $loader = function ($class) use ($directory, $namespace) {
        if (strpos($class, $namespace) === 0) {

            $class = substr($class, strlen($namespace) + 1);
            $directory = rtrim($directory, "/\\");
            $filename = str_replace('\\', '/', $class) . '.php';

            $path = $directory . '/' . $filename;

            if (file_exists($path)) {
                require_once $path;
            }
        }
    };

    if (spl_autoload_register($loader)) {
        return $loader;
    }

    return false;
}

function array_random_element(array $array) {
    $idx = array_rand($array);
    return $array[$idx];
}

function get_temp_filename($extension) {
    while (true) {
        $filename = uniqid('', true) . '.' . $extension;
        $path = path_combine(sys_get_temp_dir(), $filename);
        if (!file_exists($path)) {
            return $path;
        }
    }
}

function base32_encode($data) {
    $alphabet = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ234567';
    $pad = '=';

    if (is_null($data)) {
        return '';
    }

    $output = '';
    $position = 0;
    $stored_data = 0;
    $stored_bit_count = 0;
    $index = 0;

    while ($index < strlen($data)) {
        $stored_data <<= 8;
        $stored_data += ord($data[$index]);
        $stored_bit_count += 8;
        $index += 1;

        while ($stored_bit_count >= 5) {
            $output .= $alphabet[($stored_data & (31 << ($stored_bit_count - 5))) >> ($stored_bit_count - 5)];
            $stored_bit_count -= 5;
        }
    }

    if ($stored_bit_count > 0) {
        $output .= $alphabet[($stored_data & (31 << ($stored_bit_count - 5))) << (5 - $stored_bit_count)];
    }

    while (strlen($output) % 8 != 0) {
        $output .= $pad;
    }

    return $output;
}

function base32_decode($data) {
    $alphabet = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ234567';
    $pad = '=';

    if (is_null($data)) {
        return '';
    }

    $output = '';
    $position = 0;
    $stored_data = 0;
    $stored_bit_count = 0;
    $index = 0;

    while ($index < strlen($data)) {
        if ($data[$index] == $pad) {
            break;
        }

        $n = strpos($alphabet, $data[$index]);
        if ($n === false) {
            $n = 0;
        }

        $stored_data <<= 5;
        $stored_data += $n;
        $stored_bit_count += 5;

        if ($stored_bit_count >= 8) {
            $output .= chr(($stored_data & (255 << ($stored_bit_count - 8))) >> ($stored_bit_count - 8));
            $stored_bit_count -= 8;
        }

        $index += 1;
    }

    return $output;
}


/**
 * Execute a cURL request.
 *
 * @param string $method The HTTP method to use for the request.
 * @param string $url The URL to send the request to.
 * @param array $query An associative array of query parameters to append to the URL (optional, default: []).
 * @param array $post An associative array of POST data to send with the request (optional, default: []).
 * @param array $headers An associative array of HTTP headers to include in the request (optional, default: []).
 *
 * @return string The response body of the cURL request.
 *
 * @throws \Exception If there was an error executing the cURL request or if the HTTP status code of the response is not in the range 200-399.
 */
function curl($method, $url, $query = [], $post = [], $headers = []) {

    $ch = curl_init();

    curl_setopt($ch, CURLOPT_URL, $query ? $url . '?' . http_build_query($query) : $url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, $method);
    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true); // Seguir redirecciones


    if (is_null($headers)) {
        $headers = [];
    }

    if (!array_key_exists("Content-type", $headers)) {
        $headers["Content-type"] = "application/x-www-form-urlencoded";
    }


    $formattedHeaders = [];
    foreach ($headers as $k => $v) {
        $formattedHeaders[] = $k . ": " . $v;
    }

    curl_setopt($ch, CURLOPT_HTTPHEADER, $formattedHeaders);

    // Enviar datos POST si es necesario
    if ($method === 'POST' && $post) {
        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($post));
    }

    // Ejecutar solicitud y obtener la respuesta
    $result = curl_exec($ch);
    $httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);

    // Verificar si la solicitud fue exitosa
    if (curl_errno($ch)) {
        throw new \Exception("Error executing HTTP request: " . curl_error($ch));
    }

    if ($httpCode < 200 || $httpCode >= 400) {
        throw new \Exception("HTTP request returned status code $httpCode.");
    }

    // Cerrar cURL
    curl_close($ch);

    return $result;
}

/**
 * Tries to resolve a key or property in a given value.
 *
 * @param mixed $anything The value to resolve from.
 * @param mixed $key_or_prop The key or property to resolve.
 * @param mixed &$output The variable to store the resolved value.
 * @param mixed $fallback The fallback value if resolution fails.
 *
 * @return bool Returns true if the resolution was successful, false otherwise.
 */
function try_resolve($anything, $key_or_prop, &$output, $fallback = null) {
    if (is_array($anything) || ($anything instanceof \Traversable)) {

        foreach ($anything as $k => $v) {
            if ($k === $key_or_prop) {
                $output = $v;
                return true;
            }
        }

    } else {
        if (is_object($anything)) {

            if (property_exists($anything, $key_or_prop)) {
                $output = $anything->$key_or_prop;
                return true;
            }

            if (method_exists($anything, "offsetGet")) {
                $output = $anything[$key_or_prop];
                return true;
            }


        } else {
            if (class_exists($anything) && property_exists($anything, $key_or_prop)) {
                $output = $anything::$key_or_prop;
                return true;
            }
        }
    }

    if (func_num_args() > 3) {
        $output = $fallback;
    }

    return false;
}

/**
 * Checks if the HTTP method of the current request is the same as the given HTTP method.
 *
 * @param string $method The name of the HTTP method to check.
 * @return boolean Returns true if the HTTP method of the current request is the same as the given method, otherwise returns false.
 */
function request_method_is($method) {
    if (isset($_SERVER["REQUEST_METHOD"]) && strcasecmp($_SERVER["REQUEST_METHOD"], $method) == 0) {
        return true;
    }
    return false;
}

function remove_accents($string) {
    if (!preg_match('/[\x80-\xff]/', $string)) {
        return $string;
    }

    $chars = array(
        // Decompositions for Latin-1 Supplement
        chr(195) . chr(128) => 'A',
        chr(195) . chr(129) => 'A',
        chr(195) . chr(130) => 'A',
        chr(195) . chr(131) => 'A',
        chr(195) . chr(132) => 'A',
        chr(195) . chr(133) => 'A',
        chr(195) . chr(135) => 'C',
        chr(195) . chr(136) => 'E',
        chr(195) . chr(137) => 'E',
        chr(195) . chr(138) => 'E',
        chr(195) . chr(139) => 'E',
        chr(195) . chr(140) => 'I',
        chr(195) . chr(141) => 'I',
        chr(195) . chr(142) => 'I',
        chr(195) . chr(143) => 'I',
        chr(195) . chr(145) => 'N',
        chr(195) . chr(146) => 'O',
        chr(195) . chr(147) => 'O',
        chr(195) . chr(148) => 'O',
        chr(195) . chr(149) => 'O',
        chr(195) . chr(150) => 'O',
        chr(195) . chr(153) => 'U',
        chr(195) . chr(154) => 'U',
        chr(195) . chr(155) => 'U',
        chr(195) . chr(156) => 'U',
        chr(195) . chr(157) => 'Y',
        chr(195) . chr(159) => 's',
        chr(195) . chr(160) => 'a',
        chr(195) . chr(161) => 'a',
        chr(195) . chr(162) => 'a',
        chr(195) . chr(163) => 'a',
        chr(195) . chr(164) => 'a',
        chr(195) . chr(165) => 'a',
        chr(195) . chr(167) => 'c',
        chr(195) . chr(168) => 'e',
        chr(195) . chr(169) => 'e',
        chr(195) . chr(170) => 'e',
        chr(195) . chr(171) => 'e',
        chr(195) . chr(172) => 'i',
        chr(195) . chr(173) => 'i',
        chr(195) . chr(174) => 'i',
        chr(195) . chr(175) => 'i',
        chr(195) . chr(177) => 'n',
        chr(195) . chr(178) => 'o',
        chr(195) . chr(179) => 'o',
        chr(195) . chr(180) => 'o',
        chr(195) . chr(181) => 'o',
        chr(195) . chr(182) => 'o',
        chr(195) . chr(182) => 'o',
        chr(195) . chr(185) => 'u',
        chr(195) . chr(186) => 'u',
        chr(195) . chr(187) => 'u',
        chr(195) . chr(188) => 'u',
        chr(195) . chr(189) => 'y',
        chr(195) . chr(191) => 'y',
        // Decompositions for Latin Extended-A
        chr(196) . chr(128) => 'A',
        chr(196) . chr(129) => 'a',
        chr(196) . chr(130) => 'A',
        chr(196) . chr(131) => 'a',
        chr(196) . chr(132) => 'A',
        chr(196) . chr(133) => 'a',
        chr(196) . chr(134) => 'C',
        chr(196) . chr(135) => 'c',
        chr(196) . chr(136) => 'C',
        chr(196) . chr(137) => 'c',
        chr(196) . chr(138) => 'C',
        chr(196) . chr(139) => 'c',
        chr(196) . chr(140) => 'C',
        chr(196) . chr(141) => 'c',
        chr(196) . chr(142) => 'D',
        chr(196) . chr(143) => 'd',
        chr(196) . chr(144) => 'D',
        chr(196) . chr(145) => 'd',
        chr(196) . chr(146) => 'E',
        chr(196) . chr(147) => 'e',
        chr(196) . chr(148) => 'E',
        chr(196) . chr(149) => 'e',
        chr(196) . chr(150) => 'E',
        chr(196) . chr(151) => 'e',
        chr(196) . chr(152) => 'E',
        chr(196) . chr(153) => 'e',
        chr(196) . chr(154) => 'E',
        chr(196) . chr(155) => 'e',
        chr(196) . chr(156) => 'G',
        chr(196) . chr(157) => 'g',
        chr(196) . chr(158) => 'G',
        chr(196) . chr(159) => 'g',
        chr(196) . chr(160) => 'G',
        chr(196) . chr(161) => 'g',
        chr(196) . chr(162) => 'G',
        chr(196) . chr(163) => 'g',
        chr(196) . chr(164) => 'H',
        chr(196) . chr(165) => 'h',
        chr(196) . chr(166) => 'H',
        chr(196) . chr(167) => 'h',
        chr(196) . chr(168) => 'I',
        chr(196) . chr(169) => 'i',
        chr(196) . chr(170) => 'I',
        chr(196) . chr(171) => 'i',
        chr(196) . chr(172) => 'I',
        chr(196) . chr(173) => 'i',
        chr(196) . chr(174) => 'I',
        chr(196) . chr(175) => 'i',
        chr(196) . chr(176) => 'I',
        chr(196) . chr(177) => 'i',
        chr(196) . chr(178) => 'IJ',
        chr(196) . chr(179) => 'ij',
        chr(196) . chr(180) => 'J',
        chr(196) . chr(181) => 'j',
        chr(196) . chr(182) => 'K',
        chr(196) . chr(183) => 'k',
        chr(196) . chr(184) => 'k',
        chr(196) . chr(185) => 'L',
        chr(196) . chr(186) => 'l',
        chr(196) . chr(187) => 'L',
        chr(196) . chr(188) => 'l',
        chr(196) . chr(189) => 'L',
        chr(196) . chr(190) => 'l',
        chr(196) . chr(191) => 'L',
        chr(197) . chr(128) => 'l',
        chr(197) . chr(129) => 'L',
        chr(197) . chr(130) => 'l',
        chr(197) . chr(131) => 'N',
        chr(197) . chr(132) => 'n',
        chr(197) . chr(133) => 'N',
        chr(197) . chr(134) => 'n',
        chr(197) . chr(135) => 'N',
        chr(197) . chr(136) => 'n',
        chr(197) . chr(137) => 'N',
        chr(197) . chr(138) => 'n',
        chr(197) . chr(139) => 'N',
        chr(197) . chr(140) => 'O',
        chr(197) . chr(141) => 'o',
        chr(197) . chr(142) => 'O',
        chr(197) . chr(143) => 'o',
        chr(197) . chr(144) => 'O',
        chr(197) . chr(145) => 'o',
        chr(197) . chr(146) => 'OE',
        chr(197) . chr(147) => 'oe',
        chr(197) . chr(148) => 'R',
        chr(197) . chr(149) => 'r',
        chr(197) . chr(150) => 'R',
        chr(197) . chr(151) => 'r',
        chr(197) . chr(152) => 'R',
        chr(197) . chr(153) => 'r',
        chr(197) . chr(154) => 'S',
        chr(197) . chr(155) => 's',
        chr(197) . chr(156) => 'S',
        chr(197) . chr(157) => 's',
        chr(197) . chr(158) => 'S',
        chr(197) . chr(159) => 's',
        chr(197) . chr(160) => 'S',
        chr(197) . chr(161) => 's',
        chr(197) . chr(162) => 'T',
        chr(197) . chr(163) => 't',
        chr(197) . chr(164) => 'T',
        chr(197) . chr(165) => 't',
        chr(197) . chr(166) => 'T',
        chr(197) . chr(167) => 't',
        chr(197) . chr(168) => 'U',
        chr(197) . chr(169) => 'u',
        chr(197) . chr(170) => 'U',
        chr(197) . chr(171) => 'u',
        chr(197) . chr(172) => 'U',
        chr(197) . chr(173) => 'u',
        chr(197) . chr(174) => 'U',
        chr(197) . chr(175) => 'u',
        chr(197) . chr(176) => 'U',
        chr(197) . chr(177) => 'u',
        chr(197) . chr(178) => 'U',
        chr(197) . chr(179) => 'u',
        chr(197) . chr(180) => 'W',
        chr(197) . chr(181) => 'w',
        chr(197) . chr(182) => 'Y',
        chr(197) . chr(183) => 'y',
        chr(197) . chr(184) => 'Y',
        chr(197) . chr(185) => 'Z',
        chr(197) . chr(186) => 'z',
        chr(197) . chr(187) => 'Z',
        chr(197) . chr(188) => 'z',
        chr(197) . chr(189) => 'Z',
        chr(197) . chr(190) => 'z',
        chr(197) . chr(191) => 's'
    );

    return strtr($string, $chars);
}


/**
 * @param $text
 * @param $pattern
 * @return bool
 */
function str_like($text, $pattern) {

    $text = remove_accents(mb_strtolower(trim($text)));
    $pattern = remove_accents(mb_strtolower(trim($pattern)));


    return strpos($text, $pattern) !== false;
}

/**
 * Runs $callable between ob_start and ob_get_clean. Returns output
 * @param callable $callable
 * @return string
 */
function ob_run(callable $callable, $arguments = []) {
    try {
        ob_start();

        call_user_func_array($callable, $arguments);

        return ob_get_clean();

    } catch (\Exception $ex) {

        ob_end_clean();
        throw $ex;
    }
}

function ob_end_flush_all() {
    while (ob_get_level())
        ob_end_flush();
}

function iter_first($iterable, $predicate = null) {
    if (iter_try_get_first($iterable, $output, $predicate)) {
        return $output;
    }
    return null;
}

function iter_try_get_first($iterable, &$output, $predicate = null) {
    if (is_callable($predicate)) {
        foreach ($iterable as $item) {
            if (call_user_func_array($predicate, [$item])) {
                $output = $item;
                return true;
            }
        }
    } else {
        foreach ($iterable as $item) {
            $output = $item;
            return true;
        }
    }

    return false;
}

function rglob($pattern, $flags = 0) {
    $files = glob($pattern, $flags);
    foreach (glob(dirname($pattern) . '/*', GLOB_ONLYDIR | GLOB_NOSORT) as $dir) {

        $inner_files = rglob($dir . "/" . basename($pattern), $flags);
        foreach ($inner_files as $file) {
            $files[] = $file;
        }

    }

    return $files;
}

/**
 * @throws \Exception
 */
function list_single(array $array, $predicate = null) {
    $values = array_values(!is_null($predicate) ? array_filter($array, $predicate) : $array);
    if (count($values) == 1) {
        return $values[0];
    }
    if (count($values) > 1) {
        throw new \Exception();
    }

    return null;

}

function list_last(array $array, $predicate = null) {
    $array = array_values(!is_null($predicate) ? list_filter($array, $predicate) : $array);
    return $array[count($array) - 1];
}


function create_predicate($predicate) {

    if (is_callable($predicate)) {
        return $predicate;
    }
    if (is_array($predicate)) {
        return function ($obj) use ($predicate) {

            foreach ($predicate as $k => $v) {
                if (get_value($obj, $k) != $v) {
                    return false;
                }
            }

            return true;
        };
    }

    return function () { };

}

/**
 * @param string|null $value
 * @param int|null $output
 * @return bool
 */
function try_parse_int($value, &$output) {
    if (is_string($value)) {
        preg_match_all('/^\s*([+-]?\d+)\s*$/', str_replace(",", "", $value), $output_array);
        if (count($output_array) > 1 && count($output_array[1]) > 0) {
            $output = intval($output_array[1][0]);
            return true;
        }
    }

    $output = null;
    return false;
}

/**
 * @param string|null $value
 * @param int|null $output
 * @return bool
 */
function try_parse_float($value, &$output) {
    if (is_string($value)) {
        preg_match_all('/^\s*([+-]?\d+(\.\d+)?)\s*$/', str_replace(",", "", $value), $output_array);
        if (count($output_array) > 1 && count($output_array[1]) > 0) {
            $output = floatval($output_array[1][0]);
            return true;
        }
    }

    $output = null;
    return false;
}


function list_filter(array $array, $predicate) {
    return array_values(array_filter($array, create_predicate($predicate)));
}


/**
 * Iterates through the given iterable and yields each value one by one.
 *
 * @param iterable $iterable The iterable to iterate through.
 *
 * @return \Generator The generated values from the iterable.
 */
function iter_values($iterable) {
    foreach ($iterable as $v)
        yield $v;
}


/**
 * Maps each item in the given iterable using the provided function and returns a generator.
 *
 * @param iterable $iterable The iterable to be processed.
 * @param callable $function The function to apply to each item in the iterable.
 *
 * @return Generator The generator that yields the transformed items.
 */
function iter_map($iterable, callable $function) {
    foreach ($iterable as $item)
        yield call_user_func($function, $item);
}


/**
 * Iteratively applies a function on each item of an iterable and yields the subitems obtained.
 *
 * @param iterable $iterable An iterable object to be processed
 * @param callable $function A callback function to be applied on each item of the iterable
 *                           and should return an iterable containing the subitems
 * @return Generator A Generator object that yields the subitems obtained after applying the function
 */
function iter_map_many($iterable, callable $function) {
    foreach ($iterable as $item)
        foreach (call_user_func($function, $item) as $subitem)
            yield $subitem;
}


/**
 * Filters an iterable based on the provided function callback.
 *
 * @param iterable $iterable The iterable to be filtered.
 * @param callable $function The function callback used to determine whether to include each value in the filtered result.
 *
 * @return \Generator The filtered iterable as a generator.
 */
function iter_filter($iterable, callable $function) {
    foreach ($iterable as $k => $v)
        if (call_user_func($function, $v)) {
            yield $k => $v;
        }
}

/** @noinspection DuplicatedCode */
function abspath($path) {

    $root = null;
    $parts = [];

    if (OS::isWindows()) {
        if (strlen($path) > 2 && $path[1] == ':') {
            $root = substr($path, 0, 3);
            $path = substr($path, 3);
        }
    } else {
        if ($path[0] == "/") {
            $root = "/";
            $path = substr($path, 1);
        }
    }

    if (is_null($root)) {
        $root = getcwd();
    }

    foreach (explode(DIRECTORY_SEPARATOR, $path) as $part) {
        if ('.' == $part || '' == $part) {
            continue;
        }
        if ('..' == $part) {
            array_pop($parts);
        } else {
            $parts[] = $part;
        }
    }

    return $root . implode(DIRECTORY_SEPARATOR, $parts);
}

function recaptcha($secretKey) {

    $responseKey = $_POST['g-recaptcha-response'];
    $userIP = $_SERVER['REMOTE_ADDR'];

    $url = "https://www.google.com/recaptcha/api/siteverify?secret=$secretKey&response=$responseKey&remoteip=$userIP";
    $response = file_get_contents($url);
    $response = json_decode($response);

    return $response->success;

}

/**
 * Sums up the values of an iterable.
 *
 * @param iterable $iterable The iterable to be summed up.
 * @return int The sum of the values in the iterable.
 */
function iter_sum($iterable) {

    $output = 0;
    foreach ($iterable as $item) {
        $output += $item;

    }
    return $output;
}

/**
 * Calculate the average of an iterable.
 *
 * @param iterable $iterable The iterable to calculate the average from.
 * @return float|int|null The average value if the iterable is not empty, otherwise null.
 */
function iter_average($iterable) {
    $n = 0;
    $output = 0;
    foreach ($iterable as $item) {
        $output += $item;
        $n++;
    }
    if ($n == 0) {
        return $n;
    }

    return $output / $n;
}


function list_map(array $array, callable $function) {
    return array_map($function, $array);
}

function list_map_kv(array $array, callable $function) {
    $output = [];
    foreach ($array as $k => $v) {
        $tmp = call_user_func_array($function, [$k, $v]);
        $output[$tmp[0]] = $tmp[1];
    }
    return $output;
}


function generate_options($array, $selected_key = null, $null_option = null) {
    $options = '';

    // Add null option if required
    if (!is_null($null_option)) {
        $selected = (is_null($selected_key)) ? ' selected' : '';
        $options .= '<option value=""' . $selected . '>' . htmlspecialchars($null_option) . '</option>';
    }

    foreach ($array as $key => $value) {
        $selected = ($key == $selected_key) ? ' selected' : '';
        $options .= '<option value="' . htmlspecialchars($key) . '"' . $selected . '>' . htmlspecialchars($value) . '</option>';
    }

    return $options;
}

function array_select_kv(array $array, callable $function) {

    $output = [];
    foreach ($array as $item) {

        $x = call_user_func_array($function, [$item]);
        $output[$x[0]] = $x[1];


    }
    return $output;
}


function list_map_many(array $array, callable $function) {
    $output = array();
    foreach ($array as $i) {

        $tmp = call_user_func_array($function, [$i]);
        foreach ($tmp as $j) {
            $output[] = $j;
        }
    }


    return $output;
}


/**
 * Determines whether $obj is between $start and $end
 * @param $obj
 * @param $start
 * @param $end
 * @param bool $inclusive_start
 * @param bool $inclusive_end
 * @return bool
 */

/**
 * Determines whether $obj is between $start and $end
 * @param $obj
 * @param $start
 * @param $end
 * @param bool $inclusive_start
 * @param bool $inclusive_end
 * @return bool
 */
function between($obj, $start, $end, $inclusive_start = true, $inclusive_end = true) {
    return ($inclusive_start ? ($obj >= $start) : ($obj > $start)) &&
           ($inclusive_end ? ($obj <= $end) : ($obj < $end));
}

/**
 * @param $url
 * @param $path
 * @return string
 */
function append_path_to_url($url, $path) {
    $url = rtrim($url, '/');
    $path = ltrim($path);
    return $url . "/" . $path;
}

/**
 * Changes the date format of a given string from one format to another.
 *
 * @param string $date The date string to be converted.
 * @param string $input_format The input format of the date string.
 * @param string $output_format The desired output format for the date string.
 *
 * @return string The date string in the desired output format.
 * @throws \InvalidArgumentException If the input format is invalid or does not match the date string.
 *
 */
function change_date_format($date, $input_format, $output_format) {
    $date_obj = \DateTime::createFromFormat($input_format, $date);

    if ($date_obj === false) {
        throw new \InvalidArgumentException("Invalid input format.");
    }

    return $date_obj->format($output_format);
}

function list_replace_where(array &$array, $replacement, $filter) {
    foreach ($array as $k => $v)
        if (call_user_func($filter, $v)) {
            $array[$k] = $replacement;
        }

}

function list_remove_where(array &$array, $filter) {
    foreach ($array as $k => $v)
        if (call_user_func($filter, $v)) {
            unset($array[$k]);
        }

}

/**
 * @param array $array
 * @param ...$items
 * @return void
 */
function list_remove(array &$array, ...$items) {
    foreach ($items as $item) {
        $key = array_search($item, $array);
        if (false !== $key) {
            unset($array[$key]);
        }
    }
}


function html_value($value) {
    return $value ? 'value="' . $value . '"' : "";
}

function array_sort_keys(array $array, array $key_order) {
    return array_merge(array_flip($key_order), array_unique($array));
}


function list_order_by_asc(array &$array, callable $func) {

    usort($array, function ($a, $b) use ($func) {
        return call_user_func($func, $a) > call_user_func($func, $b);
    });


}

function ob_end_clean_all() {
    while (ob_get_level()) {
        ob_end_clean();
    }
}

function array_order_by(array &$array, callable $func, $order = "asc") {
    if (strtolower($order) == "desc") {
        usort($array, function ($a, $b) use ($func) {
            return call_user_func($func, $a) < call_user_func($func, $b);
        });

    } else {
        if (strtolower($order) == "asc") {
            usort($array, function ($a, $b) use ($func) {
                return call_user_func($func, $a) > call_user_func($func, $b);
            });
        }
    }

    return $array;

}


function scoped() {

    $all_args = func_get_args();
    $all_args_count = func_num_args();


    $scope_args = array_slice($all_args, 0, $all_args_count - 1);
    $function = array_slice($all_args, -1)[0];


    try {
        return call_user_func_array($function, $scope_args);

    } finally {

        for ($i = 0; $i < $all_args_count - 1; $i++) {

            $disposable = $all_args[$i];
            if (is_resource($disposable)) {
                fclose($disposable);
            } else {
                if (method_exists($disposable, "close")) {
                    $disposable->close();
                }
            }

        }

    }
}

if (!function_exists("str_starts_with")) {
    function str_starts_with($haystack, $needle) {
        $length = strlen($needle);
        return (substr($haystack, 0, $length) === $needle);
    }
}

if (!function_exists("str_contains")) {
    function str_contains($haystack, $needle) {
        return strpos($haystack, $needle) !== false;
    }
}

if (!function_exists("str_ends_with")) {
    function str_ends_with($haystack, $needle) {
        $length = strlen($needle);
        if ($length == 0) {
            return true;
        }

        return (substr($haystack, -$length) === $needle);
    }
}






